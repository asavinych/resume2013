public with sharing class PatientListCode {

	public Boolean showAddButtonSection {get; set;}
	public Boolean showAddVisitSection {get; set;}
	public Boolean isDoctor {get; set;}
	public Visit__c visit { get;set;}
	private String userId {get; set;}
	public List<Visit__c> visits {get; set;}

	
	public PatientListCode() {
		userId = UserInfo.getUserId();
		Id profileId = UserInfo.getProfileId();
		List<Profile> listResult=[select Name from Profile where Id=:profileId];

		if (listResult.size()>0){
			isDoctor = listResult[0].Name=='Doctor';
		}

		updateVisits();
		hideAddVisitSection();
	}

	public void updateVisits() {
		visits = [select DateVisit__c,
		          Patient__r.Full_Name__C,
		          Comments__c
		          from Visit__c
		          where Doctor__c = :userId
		                            order by DateVisit__c asc];
	}

	public void clickAddVisitButton() {
		visit = new Visit__c();

		if (isDoctor){
			visit.Doctor__c = userId;
		}
		showAddButtonSection = false;
		showAddVisitSection = true;
	}

	public void hideAddVisitSection() {
		showAddButtonSection = true;
		showAddVisitSection = false;
	}

	public PageReference save() {
		try {
			insert visit;
			updateVisits();
			hideAddVisitSection();
		} catch (DMLException e) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ''+e.getDmlMessage(0)));
			return null;
		}

		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'User has been added.'));
		return null;
	}
}