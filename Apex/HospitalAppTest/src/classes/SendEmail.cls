public with sharing class SendEmail {

	public static void sendEmail(String email){
		Messaging.reserveSingleEmailCapacity(1);
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[] {email};
		mail.setToAddresses(toAddresses);
		mail.setCcAddresses(new String[0]);
		mail.setSenderDisplayName('Salesforce Support');
		mail.setSubject('Visit patient');
		mail.setBccSender(false);
		mail.setUseSignature(false);
		mail.setPlainTextBody('Visit patient');
		mail.setHtmlBody('<b> Visit patient</b>');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}

}