trigger AfterSavePatient on Patient__c (before insert, before update) {
	
	List<String> newPassorts = new List<String>();

	for(Patient__c record : Trigger.new){
		newPassorts.add(record.Full_Passport__c);
	}

	List<Patient__c> listPatients = [select Full_Passport__c from Patient__c where Full_Passport__c in :newPassorts];
	Set<String> setPatientPassports = new Set<String>();

	for(Patient__c item:listPatients){
		setPatientPassports.add(item.Full_Passport__c);	
	}
	
	for(Patient__c record : Trigger.new){
		if (setPatientPassports.contains(record.Full_Passport__c) ){
			record.addError('User exists in Database');
		}
	}
}