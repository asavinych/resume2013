trigger AfterSaveVisit on Visit__c (before insert, before update) {
	Set<DateTime> newDates = new Set<Datetime>();
	String userId = UserInfo.getUserId();

	for(Visit__c newVisit:Trigger.new){
		Integer hour = newVisit.DateVisit__c.hour();
		
		if (hour<8 || hour>20){
			newVisit.addError('Wrong hour! Value must be between 8 and 20!');
			continue;
		}

		Integer minutes = newVisit.DateVisit__c.minute();
		if (minutes != 30 && minutes != 0){
			newVisit.addError('Wrong minutes! Value must be 0 or 30!'+minutes);
			continue;	
		}

		if (!newDates.contains(newVisit.DateVisit__c)){
			newDates.add(newVisit.DateVisit__c);
		}		
	}

	List<Visit__c> busyDates = new List<Visit__c>([select DateVisit__c from Visit__c 
									  		where 
									  		DateVisit__c in :newDates
									  		and Doctor__c=:userId]);

	Set<Datetime> setDates = new Set<Datetime>();

	for(Visit__c item : busyDates){
		setDates.add(item.DateVisit__c);	
	}

	
	Set<Id> idDoctors=new Set<Id>();
	
	for(Visit__c record : Trigger.new){
		if (setDates.contains(record.DateVisit__c)){
			record.addError('Wrong date!');
		}else{
			Date today = Date.today();
			Date visitDate = record.DateVisit__c.date();

			if (visitDate.daysBetween(today)<3){		
				if (!idDoctors.contains(record.Doctor__c)){
					idDoctors.add(record.Doctor__c);
				}
			}
		}
	}


	if (idDoctors.size()>0){
		for(User item : [select Username from User where Id in :idDoctors]){
			SendEmail.sendEmail(item.Username);
		}
	}
}