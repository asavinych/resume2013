int zd2int(zip_dig z){
00BB13D0  push        ebp  
00BB13D1  mov         ebp,esp  
00BB13D3  sub         esp,0D8h  
00BB13D9  push        ebx  
00BB13DA  push        esi  
00BB13DB  push        edi  
00BB13DC  lea         edi,[ebp-0D8h]  
00BB13E2  mov         ecx,36h  
00BB13E7  mov         eax,0CCCCCCCCh  
00BB13EC  rep stos    dword ptr es:[edi]  
	int i;
	int zi=0;
00BB13EE  mov         dword ptr [zi],0  
	for(i=0; i<5; i++)
00BB13F5  mov         dword ptr [i],0  
00BB13FC  jmp         zd2int+37h (0BB1407h)  
00BB13FE  mov         eax,dword ptr [i]  
00BB1401  add         eax,1  
00BB1404  mov         dword ptr [i],eax  
00BB1407  cmp         dword ptr [i],5  
00BB140B  jge         zd2int+51h (0BB1421h)  
	{
		zi=10*zi+z[i];
00BB140D  mov         eax,dword ptr [zi]  
00BB1410  imul        eax,eax,0Ah  
00BB1413  mov         ecx,dword ptr [i]  
00BB1416  mov         edx,dword ptr [z]  
00BB1419  add         eax,dword ptr [edx+ecx*4]  
00BB141C  mov         dword ptr [zi],eax  
	}
00BB141F  jmp         zd2int+2Eh (0BB13FEh)  
	return zi;
00BB1421  mov         eax,dword ptr [zi]  
}
00BB1424  pop         edi  
00BB1425  pop         esi  
00BB1426  pop         ebx  
00BB1427  mov         esp,ebp  
00BB1429  pop         ebp  
00BB142A  ret  