#include "StdAfx.h"
#include <cstdlib>
#include <GL/glut.h>
#include <math.h>
#include <iostream>
#include <stdio.h>
#ifdef WIN32
	#define WIN32_LEAN_AND_MEAN
	#include <Windows.h>
#endif

Fractall::IFractall* fd;
Fractall::FractallFactory fracFactory;

bool flag=true;

void Draw() {
	glClear(GL_COLOR_BUFFER_BIT);
	
	if (flag)
	{
		glBegin(GL_POINTS);
			(*fd).Draw();
		glEnd();
	}
	glFlush();
}

void Timer(int iUnused)
{
    glutPostRedisplay();
    glutTimerFunc(100, Timer, 0);
}

void Initialize() {
	glClearColor(0.0, 0.0, 1.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
}

void MouseButton(int button,int state,int x, int y)
{
	//	has the button been pressed or released?
	if (state == GLUT_DOWN) 
	{
		//	Which button was pressed?
		switch(button) 
		{
		case GLUT_LEFT_BUTTON:
			if (flag)
			{
				flag=false;
				fd=fracFactory.NextFractal();
			}
			else
			{
				flag=true;
			}
			break;
		default:
			break;
		}
		
	}
	glutPostRedisplay();
}

int main(int iArgc, char** cppArgv) {
	glutInit(&iArgc, cppArgv);
	fracFactory=Fractall::FractallFactory();
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(900, 750);
	glutInitWindowPosition(200, 150);
	glutCreateWindow("First fractal");

	Initialize();

	fd= fracFactory.NextFractal();

	glutDisplayFunc(Draw);
	
	glutMouseFunc(MouseButton);

	Timer(2);
    glutMainLoop();
	
	return 0;
}