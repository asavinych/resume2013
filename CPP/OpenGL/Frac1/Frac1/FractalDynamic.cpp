#include "stdafx.h"
#include "FractalDynamic.h"
#include <math.h>
#include <windows.h>
#include <process.h>
#include <cstdlib>
#include <iostream>

namespace Fractall
{

	void FractalDynamic::Draw()
	{
		float x=0;
		float y=0;
		for(int i=0; i<65;i++)
		{
			
		for(int j=0;j<50;j++)
		{
			x=float(i);
			y=float(j);

			for(int k=1;k<100;k++)
			{
				float t=x;

				y=y+sin(x+a*sin(b*x))*dt;
				x=t - sin(y+a*sin(b*y))*dt;
			
				float outputPixels[3] ={x, y, 0};
				
				drawFunction(outputPixels);
			}
		}
		}

		if (b<2*3.141592653)
		{
			b+=0.01f;
		}
		else
		{
			b=0;
		}


		bool modA=false;

		if (rand()%100 > 50)
		{
			modA=true;
		}

		if (modA&&a<20)
		{
			a+=0.1f;
		}
		else
		{
			a=1;
		}
	}
	
	void FractalDynamic::CalculateNext()
	{
		;
	}

	void FractalDynamic::CalcDrawCalc()
	{
		;
	}

	FractalDynamic::FractalDynamic()
	{
		a=1.0f;
		b=3.0f;
		dt=0.1f;
	}

}