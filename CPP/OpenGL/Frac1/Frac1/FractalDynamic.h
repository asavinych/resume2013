#ifndef FRACTAL_DYNAMIC_H
#define FRACTAL_DYNAMIC_H

#include "IFractal.h"

namespace Fractall
{
	class FractalDynamic:public IFractall
	{
		public:
			struct InArgs
			{
				float x;
				float y;
				float i;
			};

			FractalDynamic();
			void Draw();
			void CalculateNext();
			void CalcDrawCalc();
			float a;
			float b;
			float dt;
	};
}
#endif 