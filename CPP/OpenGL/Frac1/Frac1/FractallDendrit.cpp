#include "stdafx.h"
#include "FractallDendrit.h"
#include <math.h>
#include <cstdlib>

namespace Fractall
{
	void FractallDendrit::Draw()
	{
		float x=0.0f;
		float y=0.0f;
		for(int k=1;k<100000;k++)
		{
			float t=x;
			if (rand()%2==0)
			{
				x=a*x-b*y;
				y=b*t+a*y;
			}
			else
			{
				x=x*c-d*y+1-c;
				y=d*t+c*y-d;
			}
			
			float outputPixels[3] ={x, y, 0};
			drawFunction(outputPixels);
		}

		if (limit)
		{
			if (d<0.25f)
			{
				d+=0.001f;
			}
			else
			{
				limit=false;
			}
		}
		else
		{
			if (d>=0.0)
			{
				d-=0.001f;
			}
			else
			{
				limit=true;
			}

		}
	
	}
	
	void FractallDendrit::CalculateNext()
	{
		;
	}

	void FractallDendrit::CalcDrawCalc()
	{
		;
	}

	FractallDendrit::FractallDendrit()
	{
		a=0.0f;
		b=0.7f;
		c=0.7f;
		d=0.0f;
	}

}