#ifndef FRACTAL_DENDRIT_H
#define FRACTAL_DENDRIT_H

#include "IFractal.h"

namespace Fractall
{
	class FractallDendrit:public IFractall
	{
		public:
			FractallDendrit();
			void Draw();
			void CalculateNext();
			void CalcDrawCalc();
			float a;
			float b;
			float c;
			float d;
			bool limit;
	};
}
#endif 