#include "stdafx.h"
#include "IFractal.h"
#include <math.h>
#include <cstdlib>
#include <GL/glut.h>
#include <iostream>

namespace Fractall
{
	void drawF(float a[3])
	{
		glVertex3f(a[0]/80+0.1, a[1]/80+0.2,a[2]);
	}

	void drawF1(float a[3])
	{
		glVertex3f(a[0]*0.45+0.5, a[1]*0.45+0.5, 0.0);
	}

	void drawF2(float a[3])
	{
		glVertex3f(a[0]*0.2f+0.4f, a[1]*0.2f+0.6f, 0.0);
	}

	void drawF3(float a[3])
	{
		glVertex3f(a[0]*0.2f+0.5f, a[1]*0.2f+0.5f, 0.0);
	}

	void drawF4(float a[3])
	{
		glVertex3f(a[0]*0.5f+0.5f, a[1]*0.5f+0.5f, 0.0);
	}

	void drawF5(float a[3])
	{
		glVertex3f(a[0]*0.6f+0.4f, a[1]*0.6f+0.5f, 0.0);
	}

	void drawF6(float a[3])
	{
		glVertex3f(a[0]*0.1+0.6f, a[1]*0.1+0.5f, 0.0);
	}

	void drawF7(float a[3])
	{
		glColor3f(1.0, a[2]+0.5, 0.0);
		glVertex3f(a[0]*0.0012+0.5, a[1]*0.0012+0.5, 0.0);
	}

	IFractall* FractallFactory::NextFractal()
	{
		IFractall* fd;

		switch(stateFractal)
				{
				case 0:
					glColor3f(1.0, 1.0, 0.0);
					fd= new Fractall::FractallLatoocarfian();
					(*fd).drawFunction = drawF2;
					stateFractal=(stateFractal+1)%baseFrac;
					break;
				case 1:
					glColor3f(1.0, 1.0, 0.0);
					fd= new Fractall::FractalDynamic();
					(*fd).drawFunction = drawF;
					stateFractal=(stateFractal+1)%baseFrac;
					break;
				case 2:
					glColor3f(1.0, 1.0, 0.0);
					fd= new Fractall::FractallHenon();
					(*fd).drawFunction = drawF4;
					stateFractal=(stateFractal+1)%baseFrac;
					break;
				case 3:
					glColor3f(1.0, 1.0, 0.0);
					fd= new Fractall::FractallLace();
					(*fd).drawFunction = drawF3;
					stateFractal=(stateFractal+1)%baseFrac;
					break;
				case 9:
					glColor3f(1.0, 1.0, 0.0);
					fd= new Fractall::FractallForest();
					(*fd).drawFunction = drawF1;
					stateFractal=(stateFractal+1)%baseFrac;
					break;
				case 4:
					glColor3f(1.0, 1.0, 0.0);
					fd=new Fractall::FractallDendrit();
					(*fd).drawFunction=drawF5;
					stateFractal=(stateFractal+1)%baseFrac;
					break;
				case 5:
					glColor3f(1.0, 1.0, 0.0);
					fd=new Fractall::FractallLambda();
					(*fd).drawFunction=drawF6;
					stateFractal=(stateFractal+1)%baseFrac;
					break;
				case 6:
					glColor3f(1.0, 1.0, 0.0);
					fd=new Fractall::FractallJuliaStar(1);
					(*fd).drawFunction=drawF6;
					stateFractal=(stateFractal+1)%baseFrac;
					break;
				case 7:
				case 8:
					glColor3f(1.0, 1.0, 0.0);
					fd=new Fractall::FractallJuliaStar(2);
					(*fd).drawFunction=drawF6;
					stateFractal=(stateFractal+1)%baseFrac;
					break;
				default:
					break;
				}

		return fd;
	}
	
	FractallFactory::FractallFactory()
	{
		stateFractal=0;
		baseFrac=10;
	}

}