#ifndef FRACTAL_FACTORY_H
#define FRACTAL_FACTORY_H

#include "IFractal.h"

namespace Fractall
{
	class FractallFactory
	{
		public:
			FractallFactory();
			IFractall* NextFractal();
		private:
			int baseFrac;
			int stateFractal;

	};
}
#endif 