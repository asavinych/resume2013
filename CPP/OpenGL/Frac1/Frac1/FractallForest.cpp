#include "stdafx.h"
#include "FractallForest.h"
#include <math.h>
#include <cstdlib>

namespace Fractall
{
	void FractallForest::Draw()
	{
		float x=0.0f;
		float y=0.0f;

		for(int k=0;k<500000;k++)
		{
			float t=x;

			if (rand()%2==0)
			{
				x=a*x-b*y;
				y=b*t+a*y;
			}
			else
			{
				x=c*x-d*y+1-c;
				y=d*t+c*y-d;
			}

			float outputPixels[3] ={x, y, 0};
			drawFunction(outputPixels);
		}

		/*switch(rand()%4)
		{
		case 0:*/
		if (limit)
		{
			if (d<0.25f)
			{
				d+=0.001f;
			}
			else
			{
				limit=false;
			}
		}
		else
		{
			if (d>=0.0)
			{
				d-=0.001f;
			}
			else
			{
				limit=true;
			}

		}

		/*
			break;
		case 1:
			if (b<2*3.141592653)
			{
				b+=0.01f;
			}
			else
			{
				b=0.6f;
			}
			break;
		case 2:
			if (c<2*3.141592653)
			{
				c+=0.01f;
			}
			else
			{
				c=0.53f;
			}
			break;
		case 3:
			if (d<2*3.141592653)
			{
				d+=0.01f;
			}
			else
			{
				d=0.0f;
			}
			break;
		default:
			break;
		}*/
	}

	void FractallForest::CalculateNext()
	{
		;
	}

	void FractallForest::CalcDrawCalc()
	{
		;
	}

	FractallForest::FractallForest()
	{
		a=0.6f;
		b=0.6f;
		c=0.53f;
		d=0.0f;
		limit=true;
	}

}