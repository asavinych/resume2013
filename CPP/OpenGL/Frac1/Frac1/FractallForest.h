#ifndef FRACTAL_FOREST_H
#define FRACTAL_FOREST_H

#include "IFractal.h"

namespace Fractall
{
	class FractallForest:public IFractall
	{
		public:
			FractallForest();
			void Draw();
			void CalculateNext();
			void CalcDrawCalc();
			float a;
			float b;
			float c;
			float d;
			bool limit;
	};
}
#endif 