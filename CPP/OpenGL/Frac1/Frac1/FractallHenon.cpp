#include "stdafx.h"
#include "FractallHenon.h"
#include <math.h>
#include <cstdlib>

namespace Fractall
{
	void FractallHenon::Draw()
	{
		float x=0;
		float y=0;
		float x0=0;

		float t=0;
		float coA=cos(a);
		float sinA=sin(a);
		
		do
		{
			y=0;
			x=x0;
			for(int i=0; i<1000;i++)
			{

				t=x;
				float tsq=y-x*x;
				
				x=x*coA-tsq*sinA;
				y=t*sinA+tsq*coA;

				float outputPixels[3] ={x, y, 0};
				drawFunction(outputPixels);
				if (x>10||y>10) 
					break;

			
			}
			x0+=0.01f;
		}
		while(x0<1);

		if (a<2*3.141592653)
		{
			a+=0.01f;
		}
		else
		{
			a=0.0f;
		}
	}
	
	void FractallHenon::CalculateNext()
	{
		;
	}

	void FractallHenon::CalcDrawCalc()
	{
		;
	}

	FractallHenon::FractallHenon()
	{
		a=0.0f;
	}

}