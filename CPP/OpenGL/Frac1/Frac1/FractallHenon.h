#ifndef FRACTAL_Henon_H
#define FRACTAL_Henon_H

#include "IFractal.h"

namespace Fractall
{
	class FractallHenon:public IFractall
	{
		public:
			FractallHenon();
			void Draw();
			void CalculateNext();
			void CalcDrawCalc();
			float a;
			float b;
			float dt;
	};
}
#endif 