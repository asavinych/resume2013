#include "stdafx.h"
#include "FractallJuliaStar.h"
#include <math.h>
#include <cstdlib>

namespace Fractall
{
	void FractallJuliaStar::Draw()
	{
		float x=0.5f;
		float y=0.00001f;
		float xa, x1;
		float ya, y1;
		float r;

		float w=0;

		for(int k=0;k<200000;k++)
		{
			switch(typeFractal)
			{
			case 0:
				xa=-4.0f*x+cos(4.0f*w);
				ya=-4.0f*y+sin(4.0f*w);
				break;
			case 1:
				xa=-4.0f*x+1;
				ya=-4.0f*y;
				break;
			default:
				xa=-4.0f*x+cos(w);
				ya=-4.0f*y+sin(w);
				break;
			}

			r=sqrt(xa*xa+ya*ya);
			w=atan(abs(ya/xa));
			if (ya>0.0f&&xa<0.0f)
			{
				w=3.141592653f-w;
			}
			else
			{
				if (ya<0.0f&&xa<0.0f)
				{
					w+=3.141592653f;
				}
				else
				{
					if (ya<0.0f&&xa>0.0f)
					{
						w=-w;
					}
				}
			}
			float s;
			switch(typeFractal)
			{
			case 0:
				if (rand()%2==0)
				{
					x1=-cos(w)- sqrt(r) * cos(w/2.0f) / 2.0f;
					y1=-sin(w)+ sqrt(r) * sin(w/2.0f) / 2.0f;
				}
				else
				{
					x1=- cos(w) + sqrt(r) * cos(w/2.0f) / 2.0f;
					y1=- sin(w) - sqrt(r) * sin(w/2.0f) / 2.0f;
				}
				break;
			case 1:
				s=4.0f*(float(rand())/float(RAND_MAX))-1.0f;
				if (rand()%2==0)
				{
					x1=- s * cos(w) - sqrt(r) * cos(w/2.0f) / 2.0f;
					y1=s*sin(w) - sqrt(r) * sin(w/2.0f) / 2.0f;

				}
				else
				{
					x1=-s*cos(w) + sqrt(r) * cos(w/2.0f) / 2.0f;
					y1=s*sin(w) + sqrt(r) * sin(w/2.0f) / 2.0f;
				}
				break;
			default:
				if (rand()%2==0)
				{
					x1=- cos(2.0f*w) - sqrt(r) * cos(w/2.0f) / 2.0f;
					y1=- sin(2.0f*w) + sqrt(r) * sin(w/2.0f) / 2.0f;
				}
				else
				{
					x1=- cos(2.0f*w) + sqrt(r) * cos(w/2.0f) / 2.0f;
					y1=- sin(2.0f*w) - sqrt(r) * sin(w/2.0f) / 2.0f;
				}
				break;
				
			}
			x=x1;
			y=y1;
			float outputPixels[3] ={x, y, 0};
			drawFunction(outputPixels);
		}
		
					
	}
	
	void FractallJuliaStar::CalculateNext()
	{
		;
	}

	void FractallJuliaStar::CalcDrawCalc()
	{
		;
	}

	FractallJuliaStar::FractallJuliaStar(int type)
	{
		typeFractal=type;
	}

}