#ifndef FRACTAL_JULIASTAR_H
#define FRACTAL_JULIASTAR_H

#include "IFractal.h"

namespace Fractall
{
	class FractallJuliaStar:public IFractall
	{
		public:
			FractallJuliaStar(int type);
			void Draw();
			void CalculateNext();
			void CalcDrawCalc();
			int typeFractal;
	};
}
#endif 