#include "stdafx.h"
#include "FractallLace.h"
#include <math.h>
#include <cstdlib>

namespace Fractall
{

	float arctgLocal(float x, float y)
	{
		float w=atan(abs(x/y));

		if (y>0&&x<0) 
		{
			w=3.141592653f-w;
		}
		else
		{
			if (y<0&&x<0)
			{
				w+=3.141592653f;
			}
			else
			{
				if (y<0&&x>0)
				{
					w=(-w);
				}
			}
		}

		return w;
	}

	void FractallLace::Draw()
	{
		float x=0.1f;
		float y=0.1f;
		srand(23);
		float con=sqrt(divided)/divisor;
		for(int i=0;i<100000;i++)
		{
			float r0=sqrt(x*x+y*y);
			float w=arctgLocal(x-1.0f, y);
			float x1, y1;

			switch(rand()%4)
			{
			case 0:
				w=arctgLocal(x-1.0f, y);
				y1=-r0*cos(w)/2.0f+1.0f;
				x1=-r0*sin(w)/2.0f;
				break;
			case 1:
				w=arctgLocal(x+0.5f, y-con);
				y1=-r0*cos(w)/2.0f-0.5f;
				x1=-r0*sin(w)/2.0f+con;
				break;
			case 2:
				w= arctgLocal(x+0.5f, y+con);
				y1=-r0*cos(w)/2.0f-0.5f;
				x1=-r0*sin(w)/2.0f-con;
				break;
			default:
				w=arctgLocal(x, y);
				y1=-r0*cos(w)/2.0f;
				x1=-r0*sin(w)/2.0f;
				break;
			}

			x=x1;
			y=y1;
			float outputPixels[3] ={x, y, 0};
			drawFunction(outputPixels);
		}

		//��� ��������
		if (rand()%2==0)
		{
			divided+=0.01f;
		}
		else
		{
			divisor+=0.01f;
		}
	}
	
	void FractallLace::CalculateNext()
	{
		;
	}

	void FractallLace::CalcDrawCalc()
	{
		;
	}

	FractallLace::FractallLace()
	{
		divided=3.0f;
		divisor=2.0f;
	}

}