#ifndef FRACTAL_LACE_H
#define FRACTAL_LACE_H

#include "IFractal.h"

namespace Fractall
{
	class FractallLace:public IFractall
	{
		public:
			FractallLace();
			void Draw();
			void CalculateNext();
			void CalcDrawCalc();
			float divided;
			float divisor;
	};
}
#endif 