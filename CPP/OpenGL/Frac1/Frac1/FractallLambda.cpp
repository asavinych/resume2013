#include "stdafx.h"
#include "FractallLambda.h"
#include <math.h>
#include <cstdlib>
#include <complex>



namespace Fractall
{
	void FractallLambda::Draw()
	{
		int mx, my;
		std::complex<float> l, z;
		int max=100;
		float k;
		float it=0.35f;
		mx = 375;
		my = 375;
		for (int x = -mx; x <= mx; x++) {
			for (int y = -my; y <= my; y++) {
				l = std::complex<float>(x*0.01f+1.0f, y*0.01f);
				z = 0.5f;
				k = 0.0f;
				while ((k < it)&&(abs(z) < max)) {
					z = l*z*(1.0f-z);
					k+=0.01f;
				}
				if (k<it) 
				{
					float outputPixels[3] ={float(x), float(y), k};
					drawFunction(outputPixels);
				}	
			}
		}
		
	}
	
	void FractallLambda::CalculateNext()
	{
		;
	}

	void FractallLambda::CalcDrawCalc()
	{
		;
	}

	FractallLambda::FractallLambda()
	{
		;
	}

}