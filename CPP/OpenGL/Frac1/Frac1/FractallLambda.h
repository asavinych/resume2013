#ifndef FRACTAL_LAMBDA_H
#define FRACTAL_LAMBDA_H

#include "IFractal.h"

namespace Fractall
{
	struct Complex
	{
		float x;
		float y;
	};

	class FractallLambda:public IFractall
	{
		public:
			FractallLambda();
			void Draw();
			void CalculateNext();
			void CalcDrawCalc();

	};
}
#endif 