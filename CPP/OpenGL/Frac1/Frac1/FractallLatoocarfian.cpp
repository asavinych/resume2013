#include "stdafx.h"
#include "FractallLatoocarfian.h"
#include <math.h>
#include <cstdlib>

namespace Fractall
{
	void FractallLatoocarfian::Draw()
	{
		float x=1;
		float y=1;
		float t=0;

		for(int i=0;i<100000;i++)
		{
			t=x;
			x=sin(y*b)+c*sin(x*b)+cos(c*x);
			y=sin(t*a)+d*sin(y*a)+sin(d*y);

			float outputPixels[3] ={x, y, 0};
			drawFunction(outputPixels);
		}

		/*if (c<2*3.141592653)
		{
			c+=0.001f;
		}
		else
		{
			c=0;
		}*/

		if (b<0.95f)
		{
			b+=0.001f;
		}
		else
		{
			b=0.72f;
		}

		if (a<4.17f){
			a+=0.001f;
		}else{
			a=3.50f;
		}
		
	}
	
	void FractallLatoocarfian::CalculateNext()
	{
		;
	}

	void FractallLatoocarfian::CalcDrawCalc()
	{
		;
	}

	FractallLatoocarfian::FractallLatoocarfian()
	{
		a=3.50f;
		b=0.72f;
		c=1.76f;
		d=0.54f;
	}
}