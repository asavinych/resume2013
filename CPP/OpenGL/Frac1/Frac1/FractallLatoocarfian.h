#ifndef FRACTAL_Latoocarfian_H
#define FRACTAL_Latoocarfian_H

#include "IFractal.h"

namespace Fractall
{
	

	class FractallLatoocarfian:public IFractall
	{
		public:
			FractallLatoocarfian();
			void Draw();
			void CalculateNext();
			void CalcDrawCalc();
			float a;
			float b;
			float c;
			float d;
			bool isA;
			bool isB;
			bool isC;
			bool isD;

	};
}
#endif 