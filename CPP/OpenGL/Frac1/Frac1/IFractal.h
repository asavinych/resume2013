#ifndef	IFRACTAL_H
#define	IFRACTAL_H

namespace Fractall
{
	class IFractall
	{
		public:
			//������� ���������
			virtual void Draw()=0;
			//������� ������������ ���������� ����������� ��������
			virtual void CalculateNext()=0;
			//�����������, ������ � ������ ������, � ����� ����������� � ������ ������
			virtual void CalcDrawCalc()=0;
			void (*drawFunction)(float[3]);
	};
}

#endif