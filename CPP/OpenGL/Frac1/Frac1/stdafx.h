// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include "IFractal.h"
#include "FractalDynamic.h"
#include "FractallHenon.h"
#include "FractallLatoocarfian.h"
#include "FractallLace.h"
#include "FractallForest.h"
#include "FractallDendrit.h"
#include "FractallJuliaStar.h"
#include "FractallFactory.h"
#include "FractallLambda.h"

// TODO: reference additional headers your program requires here
