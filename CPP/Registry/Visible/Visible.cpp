// Visible.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Visible.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
HWND hwndButton;								//button
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
WNDPROC OldButtonProc;

bool visible=false;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_VISIBLE, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_VISIBLE));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_VISIBLE));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_VISIBLE);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 500, 200, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}


void SetValue(HKEY hKey, LPCWSTR lpValueName, DWORD val)
{
	LONG res;
	res= RegSetValueEx(hKey,lpValueName, 0, REG_DWORD, (LPBYTE)&val , sizeof(DWORD));    
	if(res!=ERROR_SUCCESS)       
		MessageBox(0,__T("Error using RegSetValueEx() function"),__T("Error"),0); 
}

BOOL SetValueInRegister(BOOL modify)
{

	HKEY hKey;

	LONG res;
	
	res=RegOpenKeyEx(HKEY_CURRENT_USER, __T("Software\\\\Microsoft\\\\Windows\\\\CurrentVersion\\\\Explorer\\\\Advanced"),0,KEY_ALL_ACCESS, &hKey);   

	if(res!=ERROR_SUCCESS)
			MessageBox(0,__T("Error Opening Registry Key"),__T("Error"),0);
	
	if (modify)
	{
		SetValue(hKey,  __T("Hidden"), 1);
		SetValue(hKey,  __T("HideFileExt"), 0);
		SetValue(hKey,  __T("SuperHidden"), 1);
		SetValue(hKey,  __T("ShowSuperHidden"), 1);
	}
	else
	{
		SetValue(hKey,  __T("Hidden"), 2);
		SetValue(hKey,  __T("HideFileExt"), 1);
		SetValue(hKey,  __T("SuperHidden"), 0);
		SetValue(hKey,  __T("ShowSuperHidden"), 0);
	}
	
	res=RegCloseKey(hKey);

	if(res!=ERROR_SUCCESS)
			MessageBox(0,__T("Error Closing Registry Key"),__T("Error"),0);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	HWND hButton, hTextBoxAddress, hTextBoxValue, hwndButton;

	switch (message)
	{
	case WM_CREATE:

		hButton=CreateWindowEx(NULL, L"BUTTON", L"Visible", WS_VISIBLE | WS_CHILD,
		180, 40, 120, 50, hWnd, (HMENU)BTN_BUTTON_CLICK_ID, NULL, NULL);

		break;
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case BTN_BUTTON_CLICK_ID:
			//MessageBox(hWnd, L"I was clicked!", L"Info", MB_OK);

			if (visible)
			{
				SetValueInRegister(visible);
				SetDlgItemText(hWnd, BTN_BUTTON_CLICK_ID, L"Visible!");
				visible=false;
			}
			else
			{
				SetValueInRegister(visible);
				SetDlgItemText(hWnd, BTN_BUTTON_CLICK_ID, L"Unvisible!");
				visible=true;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}



