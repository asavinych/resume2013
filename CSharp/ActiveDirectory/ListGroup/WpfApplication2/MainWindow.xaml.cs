﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.DirectoryServices;
using System.Threading;
using System.Windows.Threading;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Thread threadAddItems;

        public MainWindow()
        {
            InitializeComponent();
            threadAddItems = new Thread(new ThreadStart(getNameFromAD));
            
        }

        private void getNameFromAD()
        {
            DirectoryEntry dir = new DirectoryEntry("");
            
            foreach (DirectoryEntry a in dir.Children)
            {
                if (a.Name.IndexOf("") > 0)
                    foreach (DirectoryEntry b in a.Children)
                    {
                        if (b.Name.IndexOf("") > 0)
                            foreach (DirectoryEntry c in b.Children)
                            {

                                if (c.Name.IndexOf("") > 0)
                                    foreach (DirectoryEntry d in c.Children)
                                    {
                                        this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate() { listBox1.Items.Add(d.Name+d.Path); });
                                        Thread.Sleep(1000);
                                    }
                            }
                    }
            }
        }


        private void button1_Click(object sender, RoutedEventArgs e)
        {
            threadAddItems.IsBackground = true;
            threadAddItems.Start();
        }
    }
}
