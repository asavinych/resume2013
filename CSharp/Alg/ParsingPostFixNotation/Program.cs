﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        //sample the input data = 5 6 7 * + 1 -
        //sample the output 46, if input data 5 6 7 * + 1 -   
        static void Main(string[] args)
        {
            Stack<int> values = new Stack<int>();

            foreach (string token in args)
            {
                int value;
                if (int.TryParse(token, out value))
                {
                    values.Push(value);
                }
                else
                {
                    int rhs = values.Pop();
                    int lhs = values.Pop();

                    switch (token)
                    {
                        case "+":
                            values.Push(lhs + rhs);
                            break;
                        case "-":
                            values.Push(lhs - rhs);
                            break;
                        case "/":
                            values.Push(lhs / rhs);
                            break;
                        case "*":
                            values.Push(lhs * rhs);
                            break;
                        default:
                            break;
                    }
                }

            }

            Console.WriteLine(values.Pop());
            Console.ReadLine();
        }
    }
}
