﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication2
{
    class Program
    {
        static void swap(ref int first, ref int second)
        {
            first ^= second;
            second ^= first;
            first ^= second;
        }

        static void qs(int[] s_arr, int first, int last)
        {
            int i = first, j = last, x = s_arr[(first + last) / 2];

            do
            {
                while (s_arr[i] < x) i++;
                while (s_arr[j] > x) j--;

                if (i <= j)
                {
                    if (i < j) swap(ref s_arr[i],ref s_arr[j]);
                    i++;
                    j--;
                }
            } while (i <= j);

            if (i < last)
                qs(s_arr, i, last);
            if (first < j)
                qs(s_arr, first, j);
        }

        static int partition(int[] m, int a, int b) 
        {
            int i = a;
            for (int j = a; j <= b; j++)         // просматриваем с a по b
            {
                if (m[j].CompareTo(m[b]) <= 0)  // если элемент m[j] не превосходит m[b],
                {
                    int t = m[i];                  // меняем местами m[j] и m[a], m[a+1], m[a+2] и так далее...
                    m[i] = m[j];                 // то есть переносим элементы меньшие m[b] в начало,
                    m[j] = t;                    // а затем и сам m[b] «сверху»
                    i++;                         // таким образом последний обмен: m[b] и m[i], после чего i++
                }
            }
            return i - 1;                        // в индексе i хранится <новая позиция элемента m[b]> + 1
        }

        static void quicksort(int[] m, int a, int b) // a - начало подмножества, b - конец
        {                                        // для первого вызова: a = 0, b = <элементов в массиве> - 1
            if (a >= b) return;
            int c = partition(m, a, b);
            quicksort(m, a, c - 1);
            quicksort(m, c + 1, b);
        }

        static void Main(string[] args)
        {
            double d = 0;
            Random rnd = new Random();
            int max = 1000000;
            int[] a = (new int[max]).Select(x => rnd.Next(max)).ToArray();
            DateTime dt = DateTime.Now;

            qs(a, 0, a.Length - 1);

            DateTime dt1 = DateTime.Now;

            TimeSpan ts = dt1 - dt;
            Console.WriteLine("{0} : {1} : {2} : {3}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);


            a = (new int[max]).Select(x => rnd.Next(max)).ToArray();

            dt = DateTime.Now;

            quicksort(a, 0, a.Length - 1);

            dt1 = DateTime.Now;

            ts = dt1 - dt;

            Console.WriteLine("{0} : {1} : {2} : {3}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
            Console.ReadLine();
        }
    }
}
