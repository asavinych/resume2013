﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomizingDisplayObjectInDebugger
{
    class Program
    {
        static void Main(string[] args)
        {
            var p = new Person("Sarah");

            p.Age = 33;
            p.FavouriteColors.Add(2, "Red");
            p.FavouriteColors.Add(1, "Orange");
            p.FavouriteColors.Add(3, "Pink");
        }
    }
}
