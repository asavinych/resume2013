﻿using System;
using System.Collections.Generic;

using System.Text;
using System.IO;

namespace OemToOem
{
    public static class OemToOem
    {
		//the one time pad
        public static string convertOemToOem(string fileName)
        {
            StreamReader sr = new StreamReader(fileName, Encoding.GetEncoding("CP866"));

            byte tw = 12;
            FileInfo fi = new FileInfo(fileName);
            int len = (int)fi.Length;
            byte[] str = new byte[len];
            sr.BaseStream.Read(str, 0, len);

            for (int i = 0; i < str.Length; i++)
            {
                str[i] ^= tw;
            }
            sr.Close();
            return Encoding.GetEncoding("CP866").GetString(str);
        }
    }
}
