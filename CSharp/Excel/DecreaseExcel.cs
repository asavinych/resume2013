﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using System.IO;

namespace SAS.BigFilesSber
{
    public class DecreaseExcel
    {
        //Кусок кода убран ;-)
		
        static string XmlToCSVByExcel(string filepath)
        {
            string resultPath = "";

            Application excel = new Application();
            excel.Visible = false;
            object missing = Type.Missing;

            Workbooks books = excel.Workbooks;
            Workbook book = books.Open(filepath);
            Worksheet sheet = book.ActiveSheet;

            sheet.Cells.Replace(",", ";", XlLookAt.xlPart, XlSearchOrder.xlByRows, false, false, false);
            resultPath = Path.GetDirectoryName(filepath) + "\\" + Path.GetFileNameWithoutExtension(filepath)
                                     + ".csv";

            book.SaveAs(resultPath, XlFileFormat.xlCSVMSDOS);
            book.Close(false, Type.Missing, Type.Missing);


            if (book != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(book);
            }

            books.Close();

            if (books != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(books);
            }

            if (sheet != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(sheet);
            }


            excel.Quit();

            if (excel != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(excel);
            }


            book = null;
            books = null;
            excel = null;

            GC.Collect();
            
            return resultPath;
        }

        //Кусок кода убран ;-)
    }
}
