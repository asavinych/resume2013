﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Microsoft.Office.Interop.Excel;
using System.IO;

namespace ModifyBigFileXML
{
    public class ForModifyBigFileXML
    {
        enum StateFileWrite
        {
            Run,
            PreEnd,
            End
        }

        static int countStringInString(string strIn, string strPattern)
        {
            int res = 0;
            while (strIn.IndexOf(strPattern) > -1)
            {
                res++;
                strIn = strIn.Replace(strPattern, "");
            }

            return res;
        }

        public static string[] XmlModify(string filepath, List<Tuple<int, int>> li, bool delSpace)
        {
            List<string> resultList = new List<string>();

            XDocument xdoc = XDocument.Load(filepath);

            XElement xel = xdoc.Root;

            XElement[] xeles = xdoc.Root.Elements().ToArray();
            XElement[] worksheetels = xeles[3].Elements().ToArray();
            XElement[] columns = worksheetels[0].Elements().Where(x => x.Name.LocalName.IndexOf("Column") >= 0).ToArray();
            XElement[] rows = worksheetels[0].Elements().Where(x => x.Name.LocalName.IndexOf("Row") >= 0).ToArray();

            if (li == null)
            {
                li=new List<Tuple<int, int>>() { new Tuple<int, int>(1, rows.Length) };
            }

            int countLi = 0;
            StateFileWrite sfw = StateFileWrite.Run;

            do
            {
                XDocument newDoc = new XDocument();
                newDoc.Declaration = new XDeclaration(xdoc.Declaration);
                newDoc.AddFirst(xdoc.FirstNode);

                XElement table = new XElement(worksheetels[0].Name, worksheetels[0].Attributes());

                table.Add(columns);

                for (int j = li[countLi].Item1; j < li[countLi].Item2; j++)
                {
                    if (!delSpace || rows[j - 1].Elements().Count() > 0)
                    {
                        foreach (XElement elem in rows[j - 1].Elements())
                        {
                            foreach (XElement elemCell in elem.Elements())
                            {
                                if (elemCell.Value == null || elemCell.Value == "" || elemCell.Value == String.Empty)
                                {
                                    elemCell.Value = "'";
                                }
                            }
                        }
                        table.Add(rows[j - 1]);
                    }
                }

                XElement worksheet = new XElement(xeles[3].Name, xeles[3].Attributes());

                worksheet.Add(new XElement(table));
                worksheet.Add(new XElement(worksheetels[1]));

                newDoc.Add(new XElement(xel.Name, xel.Attributes(),
                                            xeles[1],
                                            xeles[2],
                                            worksheet));

                string pathNewFile = Path.GetDirectoryName(filepath) + "\\" + Path.GetFileNameWithoutExtension(filepath)
                                    + countLi.ToString() + Path.GetExtension(filepath);

                newDoc.Save(pathNewFile, SaveOptions.DisableFormatting);
                
                resultList.Add(pathNewFile);

                switch (sfw)
                {
                    case StateFileWrite.PreEnd:
                        sfw = StateFileWrite.End;
                        break;
                    case StateFileWrite.Run:
                        if (countLi == li.Count - 1)
                        {
                            if (li[countLi].Item2 < rows.Length - 1)
                            {
                                li.Add(new Tuple<int, int>(li[countLi].Item2, rows.Length + 1));
                                countLi++;
                                sfw = StateFileWrite.PreEnd;
                            }
                            else
                            {
                                sfw = StateFileWrite.End;
                            }
                        }
                        else
                        {
                            countLi++;
                        }
                        break;
                    case StateFileWrite.End:
                        break;
                    default:
                        break;
                }
            }
            while (sfw != StateFileWrite.End);

            return resultList.ToArray();
        }

        public static string XmlToCSVByExcel(string filepath)
        {
            string resultPath = "";

            Application excel = new Application();
            excel.Visible = false;
            object missing = Type.Missing;

            Workbooks books = excel.Workbooks;

            Workbook book = books.Open(filepath);

            Worksheet sheet = book.ActiveSheet;

            sheet.Cells.Replace(",", ";", XlLookAt.xlPart, XlSearchOrder.xlByRows, false, false, false);

            resultPath = Path.GetDirectoryName(filepath) + "\\" + Path.GetFileNameWithoutExtension(filepath)
                                     + ".csv";

            book.SaveAs(resultPath, XlFileFormat.xlCSVMSDOS);

            book.Close(false, Type.Missing, Type.Missing);

            if (book != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(book);
            }

            books.Close();

            if (books != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(books);
            }

            if (sheet != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(sheet);
            }


            excel.Quit();

            if (excel != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(excel);
            }


            book = null;
            books = null;
            excel = null;

            GC.Collect();
            
            return resultPath;
        }

        public static string fromCSVToExcel(string csvpath, int indexTableData, int countCol, bool delSpace)
        {
            string resultPath = "";

            StreamReader sr = new StreamReader(csvpath, Encoding.GetEncoding("CP866"));
            var aIn = sr.ReadToEnd().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            List<string[]> liStr = new List<string[]>();

            for (int i = indexTableData; i < aIn.Length; i++)
            {
                liStr.Add(aIn[i].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToArray());
            }

            Application excel1 = new Application();
            
            excel1.Visible = true;
            
            object missing1 = Type.Missing;

            Workbooks books1 = excel1.Workbooks;
            Workbook book1 = excel1.Workbooks.Add();
            Worksheet sheet1 = book1.ActiveSheet;

            int row = 0;

            Func<string, string> ff = x => (x.Length > 8) ? "'" + x : x;

            for (int i = 0; i < liStr.Count; i++)
            {
                if (liStr[i].Length == countCol)
                {
                    for (int j = 0; j < countCol; j++)
                    {
                        sheet1.Cells[row + 1, j + 1] = ff(liStr[i][j]);
                    }
                    row++;
                }

                 //?
                if (liStr[i].Length == 17 && ((delSpace) ? (i >= 2 && liStr[i - 1].Length == 7 && liStr[i - 2].Length == 1) : (i >= 3 && liStr[i - 1].Length == 0 && liStr[i - 2].Length == 7 && liStr[i - 3].Length == 1)))
                {
                    for (int j = 1; j < liStr[i].Length - 1; j++)
                    {
                        sheet1.Cells[row + 1, j].FormulaR1C1 = ff(liStr[i][j]);
                    }
                    row++;
                }
            }

            sheet1.Cells.Font.Name = "Times New Roman";
            sheet1.Cells.Font.Size = 9;
            sheet1.Cells.Font.Strikethrough = false;
            sheet1.Cells.Font.Superscript = false;
            sheet1.Cells.Font.Subscript = false;
            
            for (int i = 1; i < countCol + 1; i++)
            {
                List<int> liForAvg=new List<int>();

                for (int i1 = 0; i1 < 3; i1++)
                {
                    if (sheet1.Cells[2+ i1, i ].ToString().Length > 0)
                    {
                        liForAvg.Add(sheet1.Cells[2+ i1, i ].FormulaR1C1.ToString().Length);
                    }
                }

                sheet1.Columns[i].ColumnWidth = (int)(Math.Log((liForAvg.Average()+2), 2) * 4);
                sheet1.Columns[i].Borders.LineStyle = XlLineStyle.xlContinuous;
                sheet1.Columns[i].Borders.Weight = XlBorderWeight.xlThin;
            }

            sheet1.Cells.WrapText = true;
            sheet1.Cells.HorizontalAlignment = XlHAlign.xlHAlignGeneral;
            sheet1.Cells.VerticalAlignment = XlVAlign.xlVAlignCenter;
            sheet1.Rows[1].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                ////формируем шапку
            ////вставка двух строк
            //sheet1.Rows[1].Insert(XlInsertShiftDirection.xlShiftDown, XlInsertFormatOrigin.xlFormatFromLeftOrAbove);
            //sheet1.Rows[1].Insert(XlInsertShiftDirection.xlShiftDown, XlInsertFormatOrigin.xlFormatFromLeftOrAbove);

            //sheet1.Range["A1:A2"].Merge();



            resultPath = Path.GetDirectoryName(csvpath) + "\\" + Path.GetFileNameWithoutExtension(csvpath)
                                     + "_таблица.xls";

            book1.SaveAs(resultPath, XlFileFormat.xlTemplate);

            book1.Close(false, Type.Missing, Type.Missing);

            if (book1 != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(book1);
            }

            books1.Close();

            if (books1 != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(books1);
            }

            if (sheet1 != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(sheet1);
            }


            excel1.Quit();

            if (excel1 != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(excel1);
            }


            book1 = null;
            books1 = null;
            excel1 = null;

            GC.Collect();

            return resultPath;
        }

        static void Main(string[] args)
        {
            //string[] path = XmlModify(@"C:\temp\1\011485537.xml", null, true);

            //Console.WriteLine("Сформированны XML файлы сжатые (почти)", path.Length);

            //string csvpath = XmlToCSVByExcel(path[0]);

            //Console.WriteLine("Сформированны CSV Файлы", csvpath);

            string path1 = fromCSVToExcel(/*@"C:\temp\СБЕРБАНК 30348 2012 1 полугод0.csv"*/@"C:\temp\1\0114855370.csv", 28, 15, true);//csvpath);

            Console.WriteLine("Сформированн Excel" + path1);

            Console.ReadLine();
        }
    }
}
