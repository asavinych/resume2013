--EXEC sp_configure 'show advanced options', 1
--GO
---- To update the currently configured value for advanced options.
--RECONFIGURE
--GO
---- To enable the feature.
--EXEC sp_configure 'xp_cmdshell', 1
--GO
---- To update the currently configured value for this feature.
--RECONFIGURE
--GO

--example
EXEC xp_cmdshell "C:\temp\UploadOnFTP.exe 'C:\temp\New' ftp://216.67.243.83/samp/ user password"
