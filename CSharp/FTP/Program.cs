﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace UploadOnFTP
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream fs = null;
            Stream rs = null;
            if (args.Length > 3)
            {
                string[] files = Directory.GetFiles(args[0].Trim(new char[] { '\'', '\"' }), "*.*", SearchOption.AllDirectories);
                foreach (string file in files)
                {

                    try
                    {
                        string uploadFileName = new FileInfo(file).Name;
                        string uploadUrl = args[1].Trim(new char[] { '\'', '\"' });
                        fs = new FileStream(file, FileMode.Open, FileAccess.Read);

                        string ftpUrl = string.Format("{0}/{1}", uploadUrl, uploadFileName);
                        FtpWebRequest requestObj = FtpWebRequest.Create(ftpUrl) as FtpWebRequest;
                        requestObj.Method = WebRequestMethods.Ftp.UploadFile;
                        requestObj.Credentials = new NetworkCredential(args[2], args[3]);
                        rs = requestObj.GetRequestStream();

                        byte[] buffer = new byte[8092];
                        int read = 0;
                        while ((read = fs.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            rs.Write(buffer, 0, read);
                        }
                        rs.Flush();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("File upload/transfer Failed.\r\nError Message:\r\n" + ex.Message);
                    }
                    finally
                    {
                        if (fs != null)
                        {
                            fs.Close();
                            fs.Dispose();
                        }

                        if (rs != null)
                        {
                            rs.Close();
                            rs.Dispose();
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Error! Input data must contains:\r\n the directory with files for upload (first parameter),\r\n the url for FTP server (second parameter),\r\n the login (third parameter) and the password (fourth parameter)");
                Console.ReadLine();
            }
        }
    }
}
