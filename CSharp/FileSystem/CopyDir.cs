﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MyWorkPlace
{
    public static class DirectoryExtended
    {
        public static bool CopyDir(DirectoryInfo dia, DirectoryInfo dib)
        {
            if (dia.FullName.ToLower() == dib.FullName.ToLower())
            {
                return false;
            }
                //Проверка, не существует ли такой директории
                if (Directory.Exists(dib.FullName) == false)
                {
                    Directory.CreateDirectory(dib.FullName);
                }

                //Копируем каждый файл
                foreach (FileInfo fi in dia.GetFiles("*.*"))
                {
                    string path = System.IO.Path.Combine(dib.ToString(), fi.Name);
                    if (!File.Exists(path))
                    {
                        fi.CopyTo(path, true);
                    }
                }

                foreach (DirectoryInfo di in dia.GetDirectories())
                {
                    DirectoryInfo nextTargetSubDir = dib.CreateSubdirectory(di.Name);
                    CopyDir(di, nextTargetSubDir);
                }
                return true;
        }

        public static void ClearDir(string dirPath)
        {
                foreach (var subDir in Directory.GetDirectories(dirPath))
                {
                    Directory.Delete(subDir, true);
                }

                foreach (var currFile in Directory.GetFiles(dirPath))
                {
                    File.Delete(currFile);
                }
        }
    }
}
