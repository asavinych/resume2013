﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Threading;

namespace CopYFolders
{
    class Program
    {
        private static bool CopyDir(DirectoryInfo dia, DirectoryInfo dib)
        {
            try
            {
                if (dia.FullName.ToLower() == dib.FullName.ToLower())
                {
                    return false;
                }
                //Проверка, не существует ли такой директории
                if (Directory.Exists(dib.FullName) == false)
                {
                    Directory.CreateDirectory(dib.FullName);
                }

                //Копируем каждый файл
                foreach (FileInfo fi in dia.GetFiles("*.*"))
                {
                    string path = System.IO.Path.Combine(dib.ToString(), fi.Name);
                    if (!File.Exists(path))
                    {
                        fi.CopyTo(path, true);
                    }
                }

                foreach (DirectoryInfo di in dia.GetDirectories())
                {
                    DirectoryInfo nextTargetSubDir = dib.CreateSubdirectory(di.Name);
                    CopyDir(di, nextTargetSubDir);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("__________________");
                Console.WriteLine(e.Message);
                Console.WriteLine(dia);
                 return false;
            }

        }

        private static void ClearDir(string dirPath)
        {
            try
            {
                foreach (var subDir in Directory.GetDirectories(dirPath))
                {
                    Directory.Delete(subDir, true);
                }

                foreach (var currFile in Directory.GetFiles(dirPath))
                {
                    File.Delete(currFile);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("__________________");
                Console.WriteLine(e.Message);
                Console.WriteLine(dirPath);
            }
        }

        static void Main(string[] args)
        {
            DateTime start = DateTime.Now;
            string[] str = Directory.GetDirectories(@"", "", SearchOption.TopDirectoryOnly);
            Console.WriteLine(str.Length);
            Console.WriteLine(str[1]);
            Console.ReadLine();
            //CopyDir(new DirectoryInfo(str[1]), new DirectoryInfo(@"" + (new DirectoryInfo(str[1])).Name));
            //Console.WriteLine("Yes");
            //Console.ReadLine();
            Parallel.ForEach(str, x =>
                {
                    CopyDir(new DirectoryInfo(x), new DirectoryInfo(@"" + (new DirectoryInfo(x)).Name));
                    Console.WriteLine(x);
                });

            Thread.Sleep(10000);
            Console.WriteLine("очистка_________________________________");
            Parallel.ForEach(str, x => ClearDir(x));

            Parallel.ForEach(str, x =>
                {
                    Directory.Delete(x);
                    Console.WriteLine(x);
                });
            Console.WriteLine(start.ToLongDateString() + "    " + start.ToLongTimeString());
            Console.WriteLine(DateTime.Now.ToLongDateString() + "    " + DateTime.Now.ToLongTimeString());
            Console.WriteLine("All");
            Console.ReadLine();
        }
    }
}
