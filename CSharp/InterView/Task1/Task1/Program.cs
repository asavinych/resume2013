﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task1
{
    class Program
    {
        enum Brackets
        {
            None, 
            SimpleBrackets,
            SquareBrackets,
            CurveBrackets
        }


        static void CheckBrackets()
        {
            Console.WriteLine("Введит строку:");
            string inp = Console.ReadLine();

            int countSimpleBrackets=0;
            int countSquareBrackets = 0;
            int countCurveBrackets = 0;

            Brackets state = Brackets.None;
            Stack<Brackets> st = new Stack<Brackets>();
            bool res=true;

            for (int i = 0; i < inp.Length && res; i++)
            {
                switch (state)
                {
                    case Brackets.None:
                        #region None
                        switch (inp[i])
                        {
                            case '(':
                                st.Push(state);
                                state = Brackets.SimpleBrackets;
                                countSimpleBrackets++;
                                break;
                            case '[':
                                st.Push(state);
                                state = Brackets.SquareBrackets;
                                countSquareBrackets++;
                                break;
                            case '{':
                                st.Push(state);
                                state = Brackets.CurveBrackets;
                                countCurveBrackets++;
                                break;
                            case ')':
                            case ']':
                            case '}':
                                res = false;
                                break;
                            default:
                                break;
                        }
                        #endregion
                        break;
                    case Brackets.SimpleBrackets:
                        #region SimpleBrackets
                        switch (inp[i])
                        {
                            case '(':
                                st.Push(state);
                                state = Brackets.SimpleBrackets;
                                countSimpleBrackets++;
                                break;
                            case '[':
                                st.Push(state);
                                state = Brackets.SquareBrackets;
                                countSquareBrackets++;
                                break;
                            case '{':
                                st.Push(state);
                                state = Brackets.CurveBrackets;
                                countCurveBrackets++;
                                break;
                            case ')':
                                state = st.Pop();
                                countSimpleBrackets--;
                                break;
                            case ']':
                            case '}':
                                res = false;
                                break;
                            default:
                                break;
                        }
                        #endregion
                        break;
                    case Brackets.SquareBrackets:
                        #region SquareBrackets
                        switch (inp[i])
                        {
                            case '(':
                                st.Push(state);
                                state = Brackets.SimpleBrackets;
                                countSimpleBrackets++;
                                break;
                            case '[':
                                st.Push(state);
                                state = Brackets.SquareBrackets;
                                countSquareBrackets++;
                                break;
                            case '{':
                                st.Push(state);
                                state = Brackets.CurveBrackets;
                                countCurveBrackets++;
                                break;
                            case ']':
                                state = st.Pop();
                                countSquareBrackets--;
                                break;
                            case ')':
                            case '}':
                                res = false;
                                break;
                            default:
                                break;
                        }
                        #endregion
                        break;
                    case Brackets.CurveBrackets:
                        #region CurveBrackets
                        switch (inp[i])
                        {
                            case '(':
                                st.Push(state);
                                state = Brackets.SimpleBrackets;
                                countSimpleBrackets++;
                                break;
                            case '[':
                                st.Push(state);
                                state = Brackets.SquareBrackets;
                                countSquareBrackets++;
                                break;
                            case '{':
                                st.Push(state);
                                state = Brackets.CurveBrackets;
                                countCurveBrackets++;
                                break;
                            case '}':
                                state = st.Pop();
                                countCurveBrackets--;
                                break;
                            case ')':
                            case ']':
                                res = false;
                                break;
                            default:
                                break;
                        }
                        #endregion
                        break;
                }
            }

            if (countCurveBrackets == 0 && countSimpleBrackets == 0 && countSquareBrackets == 0 && res)
            {
                Console.WriteLine("Checkpassed");
            }
            else
            {
                Console.WriteLine("Checkfailed");
            }

        }

        static void Main(string[] args)
        {
            do
            {
                CheckBrackets();
                Console.WriteLine("Ещё?");
            } while (Console.ReadLine()[0] != 'n');
        }
    }
}
