﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace AutoMegre
{
    public class ControlVersion
    {
        private List<string> firstSource;
        private List<string> secondSource;
        private List<string> originSource;

        //O - origin source is not ended
        //F - first source is not ended
        //S - second source is not ended
        private enum StateParsing
        {
            OFS,
            FS,
            OS,
            OF,
            S,
            F,
            End
        }
        
        /// <summary>
        /// Automatic megre two files
        /// </summary>
        /// <param name="firstFile">The path of a first file</param>
        /// <param name="secondFile">The path of a second file</param>
        /// <param name="originFile">The path of a original file</param>
        /// <param name="outFile">The path of a result file</param>
        public void autoMegre(string firstFile, string secondFile, string originFile, string outFile)
        {
            firstSource = getSource(firstFile);
            secondSource = getSource(secondFile);
            originSource = getSource(originFile);
            
            StreamWriter outputStream = new StreamWriter(outFile);
            
            int maxLines = Math.Max(firstSource.Count, secondSource.Count);
            int iFirst = 0;
            int iSecond = 0;
            int iOrigin = 0;
            StateParsing state = StateParsing.OFS;

            while (true)
            {
                string res = String.Empty;

                switch (state)
                {
                    case StateParsing.OFS:

                        res = getNewStringIfOFS(firstSource[iFirst], secondSource[iSecond], originSource[iOrigin],
                                                ref iFirst, ref iSecond, ref iOrigin);

                        outputStream.WriteLine(res);
                        outputStream.Flush();

                        if (iFirst >= firstSource.Count && iSecond >= secondSource.Count)
                        {
                            state = StateParsing.End;
                            break;
                        }

                        if (iOrigin >= originSource.Count)
                        {

                            if (iFirst>=firstSource.Count)
                            {
                                state=StateParsing.S;
                                break;
                            }

                            if (iSecond>=secondSource.Count)
                            {
                                state=StateParsing.F;
                                break;
                            }
                            
                            state = StateParsing.FS;
                            break;
                        }

                        if (iFirst >= firstSource.Count)
                        {
                            state = StateParsing.OS;
                            break;
                        }

                        if (iSecond >= secondSource.Count)
                        {
                            state = StateParsing.OF;
                        }
                        break;
                    case StateParsing.FS:
                        
                        res = getNewStringIfFS(firstSource[iFirst], secondSource[iSecond],
                                                ref iFirst, ref iSecond);
            
                        outputStream.WriteLine(res);
                        outputStream.Flush();

                        if (iFirst >= firstSource.Count && iSecond >= secondSource.Count)
                        {
                            state = StateParsing.End;
                            break;
                        }

                        if (iSecond >= secondSource.Count)
                        {
                            state = StateParsing.F;
                            break;
                        }

                        if (iFirst >= firstSource.Count)
                        {
                            state = StateParsing.S;
                            break;
                        }
                        break;
                    case StateParsing.OS:

                        getNewStringIfO(outputStream, originSource[iOrigin], secondSource[iSecond], ref iSecond, ref iOrigin);

                        if (iSecond >= secondSource.Count)
                        {
                            state = StateParsing.End;
                            break;
                        }

                        if (iOrigin >= originSource.Count)
                        {
                            state = StateParsing.S;
                            break;  
                        }
                        break;
                    case StateParsing.OF:

                        getNewStringIfO(outputStream, originSource[iOrigin], firstSource[iFirst], ref iFirst, ref iOrigin);

                        if (iFirst >= firstSource.Count)
                        {
                            state = StateParsing.End;
                            break;
                        }

                        if (iOrigin >= originSource.Count)
                        {
                            state = StateParsing.F;
                            break;
                        }
                        break;
                    case StateParsing.S:
                        
                        outputStream.WriteLine(secondSource[iSecond]);
                        outputStream.Flush();
                        iSecond++;
                        
                        if (iSecond >= secondSource.Count)
                        {
                            state = StateParsing.End;
                            break;
                        }
                        break;
                    case StateParsing.F:

                        outputStream.WriteLine(firstSource[iFirst]);
                        outputStream.Flush();
                        iFirst++;

                        if (iFirst >= firstSource.Count)
                        {
                            state = StateParsing.End;
                            break;
                        }
                        break;
                    case StateParsing.End:
                    default:
                        outputStream.Close();
                        return;
                }
                
            }
        }

        private List<string> getSource(string file)
        {
            using (StreamReader stream = new StreamReader(file, true))
            {
                return stream.ReadToEnd().Split(new char[] { '\n' }).Select(x => x.TrimEnd('\r', '\n')).ToList();
            }
        }

        private string getNewStringIfOFS(string one, string two, string original, ref int iFirst, ref int iSecond, ref int iOrigin)
        {
            if (one.Equals(two) && original.Equals(one))
            {
                iFirst++;
                iSecond++;
                iOrigin++;
                return original;
            }

            //if one programmer added spaces (tabs) in the code
            if (one.EqualsWithoutSpace(two) && original.EqualsWithoutSpace(one))
            {
                iFirst++;
                iSecond++;
                iOrigin++;

                if (one.Length > two.Length)
                {
                    return two;
                }
                else
                {
                    return one;
                }
            }

            //checking adding and deleting string the first programmer
            if (!original.EqualsWithoutSpace(one) && original.EqualsWithoutSpace(two))
            {
               if (addingString(firstSource, secondSource, original, one, ref iFirst, ref iSecond, ref iOrigin))
               {
                   return one;
               }
            }

            //checking adding and deleting string the second programmer
            if (!original.EqualsWithoutSpace(two) && original.EqualsWithoutSpace(one))
            {
                if (addingString(secondSource, firstSource, original, two, ref iSecond, ref iFirst, ref iOrigin))
                {
                    return two;
                }
            }

            //checking deleting string the both programmers
            if (!original.EqualsWithoutSpace(one) && one.EqualsWithoutSpace(two))
            {
                iFirst++;
                iOrigin++;
                iSecond++;
                return one;
            }

            //if has been added the empty string
            if (!original.EqualsWithoutSpace(one) && !one.EqualsWithoutSpace(two) && !original.EqualsWithoutSpace(two) && (one.deleteSpace() == String.Empty))
            {
                iFirst++;
                return one;
            }

            //if has been added the empty string
            if (!original.EqualsWithoutSpace(one) && !one.EqualsWithoutSpace(two) && !original.EqualsWithoutSpace(two) && (two.deleteSpace() == String.Empty))
            {
                iSecond++;
                return two;
            }

            iFirst++;
            iOrigin++;
            iSecond++;
            return String.Format("Conflict  {0}\t|\t{1}\t|\t{2}", original, one, two);
        }

        private bool addingString(List<string> sourceFirst,List<string> sourceSecond, string original, string str, ref int iFirst1,ref int iSecond1, ref int iOrigin)
        {
            int index = sourceFirst.FindIndex(iFirst1 + 1, x => x.EqualsWithoutSpace(original));

            //if string added
            if (index == iFirst1 + 1)
            {
                iFirst1++;
                return true;
            }

            if (index > 0)
            {
                bool addedString = original.EqualsWithoutSpace(sourceFirst[index]) && original.EqualsWithoutSpace(sourceSecond[iSecond1]) && original.Length > 1;

                if (addedString)
                {
                    iFirst1++;
                    return true;
                }
            }
            else
            {
                //if string deleted
                int indexSecond = sourceSecond.FindIndex(iSecond1 + 1, x => x.EqualsWithoutSpace(str));
                int indexOrigin = originSource.FindIndex(iOrigin + 1, x => x.EqualsWithoutSpace(str));

                if (indexOrigin < 0)
                {
                    return false;
                }

                if (indexSecond == indexOrigin && indexOrigin >= 0)
                {
                    iOrigin++;
                    iFirst1++;
                    iSecond1++;
                    return true;
                }

                bool firstDeleteString = true;

                for (int i = 0; i < (originSource.Count-indexOrigin) && (iFirst1 + i) < firstSource.Count; i++)
                {
                    if (firstSource[iFirst1 + i] != originSource[indexOrigin + i])
                    {
                        firstDeleteString = false;
                        break;
                    }
                }

                if (firstDeleteString)
                {
                    iSecond1++;
                    iOrigin++;
                    iFirst1++;
                    return true;
                }
            }

            return false;
        }

        private string getNewStringIfFS(string one, string two, ref int iFirst, ref int iSecond)
        {
            //if one programmer added spaces (tabs) in the code
            if (one.EqualsWithoutSpace(two))
            {
                iFirst++;
                iSecond++;

                if (one.Length > two.Length)
                {
                    return two;
                }
                else
                {
                    return one;
                }
            }

            if (one.Equals(two))
            {
                iFirst++;
                iSecond++;
                return one;
            }

            iFirst++;
            iSecond++;
            return String.Format("Conflict    |\t{0}\t|\t{1}", one, two);
        }

        //if the original file and one of the two files are not ended 
        //sw - stream for output, origin - string from original file, two - string from not ended file, 
        //iSecond - index string for not ended file
        private void getNewStringIfO(StreamWriter sw, string origin, string two, ref int iSecond, ref int iOrigin)
        {
            if (!origin.Equals(two))
            {
                iSecond++;
                iOrigin++;

                sw.WriteLine(two);
                sw.Flush();
            }
            else
            {
                iSecond++;
                iOrigin++;
            }
        }
    }
}
