﻿using System;
using System.Text.RegularExpressions;

namespace AutoMegre
{
    public static class ExtendedString
    {
        /// <summary>
        /// Removes all spaces and tabs in string
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string deleteSpace(this string str)
        {
            string res = Regex.Replace(str, " +", String.Empty);
            return Regex.Replace(res, "(\t)+", String.Empty);
        }

        /// <summary>
        /// Determines whether this instance without spaces and tabs and another string without spaces and tabs have the same value.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public static bool EqualsWithoutSpace(this string str, string next)
        {
            return str.deleteSpace().Equals(next.deleteSpace());
        }
    }
}
