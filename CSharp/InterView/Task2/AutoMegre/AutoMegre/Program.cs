﻿using System;
using System.IO;

namespace AutoMegre
{
    class Program
    {
        static void Main(string[] args)
        {
            string end = String.Empty;

            do
            {
                Console.WriteLine("Input path original file (example C:\\temp\\1.cs):");
                string originFile = Console.ReadLine();

                Console.WriteLine("Input first programmer's file (example C:\\temp\\1.cs):");
                string firstFile = Console.ReadLine();

                Console.WriteLine("Input second programmer's file (example C:\\temp\\1.cs):");
                string secondFile = Console.ReadLine();

                if (File.Exists(originFile) && File.Exists(firstFile) && File.Exists(secondFile))
                {
                    try
                    {
                        Console.WriteLine("Path for output (example C:\\temp\\out.cs):");
                        string outputFile = Console.ReadLine();
                        ControlVersion cv = new ControlVersion();
                        cv.autoMegre(firstFile, secondFile, originFile, outputFile);
                        Console.WriteLine("All");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
                else
                {
                    Console.WriteLine("Файлы не найдены!");
                }

                Console.WriteLine("Replay? (y|n)");
                end = Console.ReadLine();
            }
            while (end.ToLower().Equals("y"));
        }
    }
}
