﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace LoadBook
{
    public class ParserPage
    {
        public WebProxy currentProxy;

        public string getHtml(string url)
        {
            try
            {
                HttpWebRequest authReq = (HttpWebRequest)WebRequest.Create(url);
                authReq.Method = "GET";
                authReq.Proxy = currentProxy;
                authReq.AllowAutoRedirect = true;
                HttpWebResponse regResponse = (HttpWebResponse)authReq.GetResponse();
                StreamReader myStreamReader = new StreamReader(regResponse.GetResponseStream(), Encoding.GetEncoding("windows-1251"));
                return myStreamReader.ReadToEnd().Replace("\r", "").Replace("\n", "").Replace("\t", "");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "";
            }
        }

        public bool loadImage(string url, string newUrl)
        {
            WebClient webClient = new WebClient();
            webClient.Proxy = currentProxy;
            webClient.DownloadFile(url, newUrl);

            return true;
        }

        public string loadData(string url, int i)
        {
            string nameBook = String.Empty;
            string urlImage = String.Empty;
            string author = String.Empty;
            string genre = String.Empty;
            string pages = String.Empty;
            string annotation = String.Empty;
            string isbn = String.Empty;

            StringBuilder sb = new StringBuilder(getHtml(url));

            if (sb.Length > 100)
            {
                try
                {
                    Regex rg = new Regex("<div id=\"product-title\" class=\"prodtitle\"><h1>([А-Яа-я:,.0-9A-Za-z ]*)</h1>");
                    Match m = rg.Match(sb.ToString());

                    if (m.Groups.Count > 1)
                    {
                        nameBook = m.Groups[1].Value;
                    }

                    rg = new Regex("<div id=\"product-image\"><img src=\"([A-Za-z0-9:/._]*)\" alt=");
                    m = rg.Match(sb.ToString());

                    if (m.Groups.Count > 1)
                    {
                        urlImage = m.Groups[1].Value;
                        loadImage(urlImage, String.Format("C:\\dd3\\{0}.jpg", i));
                    }

                    rg = new Regex("<div class=\"authors\">Автор: (<a href=\"/authors/[0-9]*/\">[-A-Za-zА-Яа-я0-9_., ]*</a>[, ]*)*</div>");

                    m = rg.Match(sb.ToString());

                    author = m.Groups[0].Value.Replace("<div class=\"authors\">Автор: ", "").Replace("</a>", "").Replace("</div>", "");
                    author = Regex.Replace(author, "<a href=\"/authors/[0-9]*/\">", "");

                    rg = new Regex("href=\"/genres/[0-9]*/\" title=\"[-A-Za-zА-Яа-я0-9_., ']*\"itemprop='url'><span itemprop='title'>[А-Яа-я ]*</span>");

                    MatchCollection resMatch = rg.Matches(sb.ToString());

                    foreach (Match match in resMatch)
                    {
                        string val = match.Groups[0].Value.Replace("</span>", "");
                        val = Regex.Replace(val, "href=\"/genres/[0-9]*/\" title=\"[-A-Za-zА-Яа-я0-9_., ']*\"itemprop='url'><span itemprop='title'>", "");
                        genre += ", " + val;
                    }
                    genre = genre.Remove(0, 2);

                    rg = new Regex("<div class=\"pages2\">Страниц: [0-9]*");

                    m = rg.Match(sb.ToString());
                    pages = m.Groups[0].Value.Replace("<div class=\"pages2\">Страниц: ", "");

                    rg = new Regex("<meta property=\"og:description\" content=\"[-A-Za-zА-Яа-я0-9_., '\":!?;$%@#№&]*\"");
                    m = rg.Match(sb.ToString());
                    annotation = m.Groups[0].Value.Replace("<meta property=\"og:description\" content=\"", "");

                    rg = new Regex("<meta name=\"description\" content=\"[-A-Za-zА-Яа-я0-9_., '\":!?;$%@#№&]* | ISBN ([-0-9]*)");
                    m = rg.Match(sb.ToString());
                    isbn = m.Groups[0].Value.Trim();

                    Console.WriteLine(i);


                    return String.Format("<index>{0}</index><auth>{1}</auth><nameBook>{2}</nameBook><genre>{3}</genre><pages>{4}</pages><ann>{5}</ann><isbn>{6}</isbn>",
                                                    i, author, nameBook, genre, pages, annotation, isbn);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return "";
                }
            }

            return "";
        }
    }
}
