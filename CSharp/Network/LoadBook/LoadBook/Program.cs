﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace LoadBook
{
    class Program
    {
        public static void saveValues(StreamWriter writer, string value)
        {
            lock (writer)
            {
                writer.WriteLine(value);
                writer.Flush();
            }
        }

        static void start(object data)
        {

            object[] objs = (object[])data;
            int index = (int)objs[0];
            int end = (int)objs[4];
            for (int j = index; j < end; j++)
            {
                StreamWriter sw = (StreamWriter)objs[1];

                Console.WriteLine("Start thread - {0} ", j);
                ParserPage pp = new ParserPage();

                Tuple<string, int> curProxy = new Tuple<string, int>((string)objs[2], (int)objs[3]);// ProxyCollection.getFreeCurrentProxyList;
                while (curProxy == null)
                {
                    curProxy = ProxyCollection.getFreeCurrentProxyList;
                    Console.WriteLine("NULLL PROXY!!! {0}", j);
                }

                Console.WriteLine("Get proxy for {0} - {1}:{2}", j, curProxy.Item1, curProxy.Item2);
                pp.currentProxy = new WebProxy(curProxy.Item1, curProxy.Item2);
                string res = pp.loadData(String.Format("http://www.labirint.ru/books/{0}/", j), j);
                if (res != String.Empty)
                    saveValues(sw, res);
                Console.WriteLine("End {0}", j);

                Thread.Sleep(1000);
            }
        }

        static void Main(string[] args)
        {
            StreamWriter sw = new StreamWriter(@"C:\dd3\23.txt", true);
            ParallelOptions po = new ParallelOptions();
            po.MaxDegreeOfParallelism = 5;
            //ProxyCollection.InitListProxy(@"C:\test\proxy.txt");

            Console.WriteLine("The first parameters are initialized");
            //int j = 4488;


            Thread thread1 = new Thread(new ParameterizedThreadStart(start));
            Thread thread2 = new Thread(new ParameterizedThreadStart(start));
            Thread thread3 = new Thread(new ParameterizedThreadStart(start));
            Thread thread4 = new Thread(new ParameterizedThreadStart(start));
            Thread thread5 = new Thread(new ParameterizedThreadStart(start));
            Thread thread6 = new Thread(new ParameterizedThreadStart(start));
            Thread thread7 = new Thread(new ParameterizedThreadStart(start));
            Thread thread8 = new Thread(new ParameterizedThreadStart(start));
            Thread thread9 = new Thread(new ParameterizedThreadStart(start));
            thread1.Start(new object[] { 18464, sw, "114.141.162.53", 8080, 46339 }); //
            thread2.Start(new object[] { 107325, sw, "113.106.73.209", 8082, 142812 }); //
            thread5.Start(new object[] { 159914, sw, "202.98.123.126", 8080, 175000 }); //
            thread3.Start(new object[] { 211099, sw, "220.132.19.136", 8080, 242115 }); //
            thread9.Start(new object[] { 262339, sw, "46.108.241.253", 8080, 281943 }); //
            thread4.Start(new object[] { 310877, sw, "192.95.14.4", 8089, 344472 }); //
            thread6.Start(new object[] { 180278, sw, "31.131.30.161", 3128, 190279 }); //
            thread7.Start(new object[] { 374571, sw, "63.141.249.37", 8089, 380829 }); //
            thread8.Start(new object[] { 362120, sw, "198.50.171.253", 7808, 370000 }); //
            

            thread1.Join();
            thread2.Join();
            thread3.Join();
            thread4.Join();
            thread5.Join();
            while (thread1.IsAlive || thread2.IsAlive || thread3.IsAlive || thread4.IsAlive || thread5.IsAlive)
                ;
            Console.ReadLine();
        }
    }
}
