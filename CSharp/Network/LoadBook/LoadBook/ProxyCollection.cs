﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;

namespace LoadBook
{
    public static class ProxyCollection
    {
        private static List<Tuple<string, int>> globalProxyList;
        private static Queue<Tuple<string, int>> currentProxyList;
        private static int countGettedProxy;

        public static Tuple<string, int> getFreeCurrentProxyList
        {
            get
            {

                while (countGettedProxy > 5)
                    ;

                lock (currentProxyList)
                {
                    if (currentProxyList.Count > 0)
                    {
                        Tuple<string, int> res = currentProxyList.Dequeue();
                        countGettedProxy++;
                        //
                        return res;
                    }
                    else
                    {
                        Tuple<string, int> res = null;
                        for (int i = 0; i < globalProxyList.Count; i++)
                        {
                            if (SoketConnect(globalProxyList[i].Item1, globalProxyList[i].Item2))
                            {
                                res = new Tuple<string, int>(globalProxyList[i].Item1, globalProxyList[i].Item2);
                                globalProxyList.Remove(res);
                                break;
                            }
                        }

                        
                        return res;
                    }
                }
            }
            set
            {
                lock (currentProxyList)
                {
                    currentProxyList.Enqueue(new Tuple<string, int>(value.Item1, value.Item2));
                    countGettedProxy--;
                }
            }
        }

        public static bool SoketConnect(string host, int port)
        {
            var is_success = false;
            try
            {
                var connsock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                connsock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 200);
                System.Threading.Thread.Sleep(500);
                var hip = IPAddress.Parse(host);
                var ipep = new IPEndPoint(hip, port);
                connsock.Connect(ipep);
                if (connsock.Connected)
                {
                    is_success = true;
                }
                connsock.Close();
            }
            catch (Exception)
            {
                is_success = false;
            }
            return is_success;
        }

        public static void InitListProxy(string fileName)
        {
            Console.WriteLine("Start initializing proxy list");

            StreamReader sr = new StreamReader(fileName);

            globalProxyList = sr.ReadToEnd().Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).Select(x =>
            {
                string[] res = x.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                return new Tuple<string, int>(res[0], Int32.Parse(res[1]));
            }
                ).ToList();

            Console.WriteLine("Proxy list is initialized - {0}", globalProxyList.Count);
        }

        static ProxyCollection()
        {
            currentProxyList = new Queue<Tuple<string, int>>();
            countGettedProxy = 0;
        }
    }
}
