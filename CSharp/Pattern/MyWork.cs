﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace MyWorkPlace
{
    public class MyWork
    {

        private DispatcherTimer loopWork;
        private object argsForThisWork;

        public bool IsEnabledLoop
        {
            get
            {
                return loopWork.IsEnabled;
            }
        }

        public MyWork()
        {
            ;
        }

        public MyWork(object args)
        {
            argsForThisWork = args;
        }

        public MyWork(Action<object, EventArgs> actionForLoop, Dispatcher forCreator, TimeSpan interval, object args)
        {
            loopWork = new System.Windows.Threading.DispatcherTimer(DispatcherPriority.Normal, forCreator);
            loopWork.Interval=interval;
            loopWork.Tick += new EventHandler(actionForLoop);
            argsForThisWork = args;
            loopWork.Start();
        }

        public void StartLoop()
        {
            loopWork.Start();
        }

        public void StopLoop()
        {
            loopWork.Stop();
        }

        public void StartOnes(object args, Action<object> work)
        {
            Task task = new Task(work, args);
            
            task.Start();
        }

    }
}
