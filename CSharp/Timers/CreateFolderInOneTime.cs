﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using System.Timers;

namespace ConsoleApplication1
{
    class Program
    {

        static void Main(string[] args)
        {

            Timer tt = new Timer(1000);

            tt.Elapsed += new ElapsedEventHandler(tt_Elapsed);

            tt.Start();

            Console.ReadLine();
        }

        static bool one = true;
        static bool two = true;

        static void tt_Elapsed(object sender, ElapsedEventArgs e)
        {

            if (one && DateTime.Now > (new DateTime(2013, 2, 15, 19, 55, 0)))
            {

                File.Delete(@"");
                one = false;
            }

            if (two && DateTime.Now > (new DateTime(2013, 2, 16, 8, 0, 0)))
            {
                File.Copy(@"", @"");
                two = false;
            }
        }
    }
}
