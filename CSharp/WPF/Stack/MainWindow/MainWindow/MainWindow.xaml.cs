﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MainWindow
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MWindow : Window
    {
        private Stack<UndoActionBtn> undoOps = new Stack<UndoActionBtn>();
        private Random rnd;


        public MWindow()
        {
            InitializeComponent();
            rnd = new Random();
        }

        private Brush GetRandomBush()
        {
            byte[] bytes = new byte[3];
            rnd.NextBytes(bytes);

            return new SolidColorBrush(Color.FromRgb(bytes[0], bytes[1], bytes[2]));
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            undoOps.Push(new UndoActionBtn((Button)sender));
            ((Button) sender).Background = GetRandomBush();
            UpdateList();
        }

        private void UpdateList()
        {
            listBox.Items.Clear();

            foreach (UndoActionBtn action in undoOps)
            {
                listBox.Items.Add(action.ToString());
            }
        }

        private void Undo_Click(object sender, RoutedEventArgs e)
        {
            if (undoOps.Count > 0)
            {
                undoOps.Pop().Execute();
                UpdateList();
            }
        }

    }
}
