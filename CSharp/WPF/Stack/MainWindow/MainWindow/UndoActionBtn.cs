﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;

namespace MainWindow
{
    public class UndoActionBtn
    {
        private Brush brush;
        private Button button;

        public UndoActionBtn(Button button)
        {
            this.button = button;
            this.brush = button.Background.CloneCurrentValue();
        }

        public void Execute()
        {
            button.Background = brush;
        }

        public override string ToString()
        {
            return String.Format("{0} : {1}", button.Content, brush.ToString());
        }
    }
}
