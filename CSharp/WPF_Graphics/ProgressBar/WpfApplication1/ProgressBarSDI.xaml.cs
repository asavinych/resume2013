﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Media.Animation;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for ProgressBarSDI.xaml
    /// </summary>
    public partial class ProgressBarSDI : UserControl
    {

        //Для прорисовки первого квадрата без анимации
        private bool firstValue;

        //отступ следующего квадрата
        private double valueLeftRect;

        //length of all progress bar
        private double _length;

        //сторона квадрата
        private int valueSide;

        [DefaultValue(40)]
        [Description("The length of the square")]
        public int Side
        {
            get
            {
                return valueSide;
            }
            set
            {
                this.Height = value;
                this.canvas1.Height = value;
                this.valueSide = value;
            }
        }


       
        [Description("The length of the progress bar")]
        public double LengthProgressBar
        {
            get
            {
                return _length;
            }
            set
            {
                _length = value;
            }
        }

        [Description("The first color for gradient")]
        public Color firstColor
        {
            get;
            set;
        }

        [Description("The first color for gradient")]
        public Color secondColor
        {
            get;
            set;
        }


        private int numRect=0;

        public void IncValueProgress()
        {
            double LengthRect = this.Width / _length;

            if ((valueLeftRect + LengthRect) <= this.Width)
            {

                Rectangle rect = new Rectangle();
                rect.Height = valueSide;
                rect.Width = LengthRect;
                rect.Margin = new Thickness(valueLeftRect, 0, 0, 0);

                LinearGradientBrush lg = new LinearGradientBrush(firstColor, secondColor, 90);
                lg.SpreadMethod = GradientSpreadMethod.Reflect;
                
                lg.StartPoint = new Point(0, 0.5);
                lg.EndPoint = new Point(0, 1);
                rect.Fill = lg;
                rect.Stroke = Brushes.White;
                if (firstValue)
                {
                    canvas1.Children.Add(rect);
                    valueLeftRect += LengthRect / 2;
                    firstValue = false;
                }
                else
                {

                    rect.Margin = new Thickness(rect.Margin.Left - LengthRect / 2, 0, 0, 0);
                    ThicknessAnimation da = new ThicknessAnimation();
                    da.From = new Thickness(rect.Margin.Left, 0, 0, 0);
                    da.To = new Thickness(rect.Margin.Left + LengthRect, 0, 0, 0);
                    da.Duration = TimeSpan.FromSeconds(1);


                    this.RegisterName("myRect"+numRect.ToString(), rect);
                    rect.Name = "myRect" + numRect.ToString();
                    Storyboard.SetTargetName(da, "myRect"+numRect.ToString());
                    Storyboard.SetTargetProperty(da, new PropertyPath(Rectangle.MarginProperty));


                    canvas1.Children.Add(rect);
                    Storyboard sb = new Storyboard();
                    sb.Children.Add(da);
                    sb.Begin(rect);
                    valueLeftRect += LengthRect;
                    
                }
                numRect++;
            }
        }



        public ProgressBarSDI()
        {
            InitializeComponent();

            firstColor = Color.FromArgb(200, 0, 0, 255);
            secondColor = Color.FromArgb(230, 0, 0, 150);
            firstValue = true;
            valueLeftRect=0;
            valueSide=40;
        }
    }
}
