﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using System.Drawing;

namespace ProgressBarGradient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //для анимации
        DispatcherTimer dt = new DispatcherTimer();

        //для снимков экрана
        DispatcherTimer dt1 = new DispatcherTimer();

        //программируем градиентную заливку и анимацию
        private void dt_Tick(object sender, EventArgs e)
        {
            LinearGradientBrush lgb = new LinearGradientBrush();

            GradientStop stop0 = new GradientStop(Colors.Blue, 0.0);

            GradientStop stop1 = new GradientStop(Colors.White, 0.0);

            GradientStop stop2 = new GradientStop(Colors.Blue, 1.0);


            //this.RegisterName("gradStop1", stop1);

            lgb.GradientStops.Add(stop0);
            lgb.GradientStops.Add(stop1);
            lgb.GradientStops.Add(stop2);

            rectangle1.Fill = lgb;

            DoubleAnimation offsetAnimation = new DoubleAnimation();
            offsetAnimation.From = 0.0;
            offsetAnimation.To = 1.0;
            offsetAnimation.Duration = TimeSpan.FromSeconds(2);
            
            stop1.BeginAnimation(GradientStop.OffsetProperty, offsetAnimation);
        }

        public MainWindow()
        {
            InitializeComponent();


            dt = new System.Windows.Threading.DispatcherTimer(DispatcherPriority.Normal, this.Dispatcher);
            dt.Interval = TimeSpan.FromSeconds(2.3);
            dt.Tick += new EventHandler(dt_Tick);

            dt1 = new DispatcherTimer(DispatcherPriority.Normal, this.Dispatcher);

            dt1.Interval = TimeSpan.FromSeconds(0.01);

            dt1.Tick += new EventHandler(dt1_Tick);

        }




        private int countBmp;

        //делаем снимки экрана
        void dt1_Tick(object sender, EventArgs e)
        {
            System.Windows.Forms.Screen sc = System.Windows.Forms.Screen.PrimaryScreen;

            Bitmap bt = new Bitmap(sc.WorkingArea.Width, sc.WorkingArea.Height);
            using (Graphics gr = Graphics.FromImage(bt))
            {
                gr.CopyFromScreen(sc.WorkingArea.X, sc.WorkingArea.Y, 0, 0, new System.Drawing.Size(bt.Width, bt.Height));
            }

            bt.Save(countBmp.ToString()+".png", System.Drawing.Imaging.ImageFormat.Png);
            countBmp++;
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            //dt1.IsEnabled = true;
            //dt1.Start();
            dt_Tick(null, null);
            dt.IsEnabled = true;

            dt.Start();
        }



    }
}
