﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Routings.Model;

namespace Routings
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<CustomerPoints> _cp;
        int counter = 0;
        bool reverse = false;

        public MainWindow()
        {
            InitializeComponent();
            _Init();
        }

        private void _Init()
        {
            _NavigatePage();
            _cp = CustomerPoints.GetCustomerPoints(@"C:\Temp\dispatch_app\dispatch_app\EKPC_7-15-2013_Appointments.txt");
            dGInst.ItemsSource = _cp;
            dGInst.Items.Refresh();
        }

        private void _NavigatePage()
        {
            Uri uri = new Uri(@"pack://application:,,,/example.html");
            Stream source = Application.GetContentStream(uri).Stream;
            webBrowser.NavigateToStream(source);

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
                if (reverse)
                {
                    counter--;
                    if (_cp[counter].points.Count>0)
                    {
                        this.webBrowser.InvokeScript("setCenterMap", _cp[counter].points[0].latitude, _cp[counter].points[0].longitude);

                        for (int i = 0; i < _cp[counter].points.Count; i++)
                        {
                            this.webBrowser.InvokeScript("removeMarker");
                        }
                    
                        if (counter == 0) reverse = false;
                    }
                }
                else
                {
                    this.webBrowser.InvokeScript("setCenterMap", _cp[counter].points[0].latitude, _cp[counter].points[0].longitude);

                    for (int i = 0; i < _cp[counter].points.Count; i++)
                    {
                        string colorRes = string.Format("{0:X2}{1:X2}{2:X2}", _cp[counter].color.R, _cp[counter].color.G, _cp[counter].color.B);
                        this.webBrowser.InvokeScript("writeMarkers",
                                                        _cp[counter].points[i].latitude,
                                                        _cp[counter].points[i].longitude,
                                                        i,
                                                        _cp[counter].points[i].text,
                                                        colorRes);
                    }

                    counter++;
                    if (counter >= _cp.Count) reverse = true;
                }

        }
    }
}
