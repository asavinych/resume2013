﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Reflection;

namespace Routings.Model
{
    public class CustomerPoints
    {
        public string customerId
        {
            get;
            set;
        }

        public List<Point> points;

        public Color color
        {
            get;
            set;
        }

        public CustomerPoints(string customer)
        {
            customerId = customer;
            points = new List<Point>();

            Type color1 = typeof(Colors);
            PropertyInfo[] arrPropertyInfo = color1.GetProperties();
            List<PropertyInfo> propInfo = arrPropertyInfo.OrderBy(x => x.Name).ToList();
            color = (Color)(propInfo[CustomerPoints.countColor].GetValue(null));
            CustomerPoints.countColor++;
            switch (CustomerPoints.countColor)
            {
                case 2: case 4: 
                    CustomerPoints.countColor++;
                    break;
                case 7:
                    CustomerPoints.countColor+=4;
                    break;
            }
            
        }

        public CustomerPoints()
        {
            points = new List<Point>();
            customerId = String.Empty;
        }

        public static int countColor = 0;

        public static List<CustomerPoints> GetCustomerPoints(string file)
        {
            List<CustomerPoints> res = new List<CustomerPoints>();

            StreamReader sr = new StreamReader(file);
            List<Point> liRes = new List<Point>();

            string[] midStrs = sr.ReadToEnd().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 1; i < midStrs.Length; i++)
            {
                Point pp = new Point();
                string[] points = midStrs[i].Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);
                getCustomerPointsOne(points, res);
            }

            return res;
        }

        public static void getCustomerPointsOne(string[] points, List<CustomerPoints> liCustomerPoints)
        {
            if (points.Length > 11)
            {
                var a = (liCustomerPoints.Count > 0) ? liCustomerPoints.SingleOrDefault(x => x.customerId == points[3]) : null;
                if (a != null)
                {
                    a.points.Add(new Point() { latitude = decimal.Parse(points[7]), longitude = decimal.Parse(points[8]), text = points[11] + points[1] });
                }
                else
                {
                    CustomerPoints cp = new CustomerPoints(points[3]);
                    cp.points.Add(new Point() { latitude = decimal.Parse(points[7]), longitude = decimal.Parse(points[8]), text = points[11] + points[1] });
                    liCustomerPoints.Add(cp);
                }
            }
        }
    }
}
