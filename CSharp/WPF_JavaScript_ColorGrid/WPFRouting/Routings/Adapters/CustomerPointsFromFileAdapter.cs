﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Routing.Model;


namespace Routing.Adapters
{
    public class CustomerPointsFromFileAdapter:IAdapter<CustomerPoints>
    {
        private string fileName
        {
            get
            {
                return Routing.Properties.Settings.Default["CustomerPointFile"].ToString();
            }
        }

        public int countColor = 12;

        /// <summary>
        /// Get list customer points from file
        /// </summary>
        /// <returns></returns>
        public List<CustomerPoints> getListData()
        {
            List<CustomerPoints> res = new List<CustomerPoints>();

            Stream source = Application.GetContentStream(new Uri(@"pack://application:,,,/"+fileName)).Stream;

            StreamReader sr = new StreamReader(source);
            List<Routing.Model.Point> liRes = new List<Routing.Model.Point>();

            string[] midStrs = sr.ReadToEnd().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 1; i < midStrs.Length; i++)
            {
                Routing.Model.Point pp = new Routing.Model.Point();
                string[] points = midStrs[i].Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);
                getCustomerPointsOne(points, res);
            }

            return res;
        }

        public void getCustomerPointsOne(string[] points, List<CustomerPoints> liCustomerPoints)
        {
            if (points.Length > 11)
            {
                var a = (liCustomerPoints.Count > 0) ? liCustomerPoints.SingleOrDefault(x => x.customerId == points[3]) : null;
                if (a != null)
                {
                    a.points.Add(new Routing.Model.Point() { latitude = decimal.Parse(points[7]), longitude = decimal.Parse(points[8]), text = points[1] + " " + points[11] });
                }
                else
                {
                    CustomerPoints cp = new CustomerPoints(points[3]);
                    cp.color = getColorForPoints();
                    cp.startPoint = new Routing.Model.Point() { latitude = decimal.Parse(points[7]), longitude = decimal.Parse(points[8]), text = points[1] + " " + points[11] };
                    liCustomerPoints.Add(cp);
                }
            }
        }

        public Color getColorForPoints()
        {
            List<int> badColors = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,13  };

            
            Type color1 = typeof(Colors);
            PropertyInfo[] arrPropertyInfo = color1.GetProperties();
            List<PropertyInfo> propInfo = arrPropertyInfo.OrderBy(x => x.Name).ToList();

            Color color = (Color)(propInfo[countColor].GetValue(null));

            do
            {
                countColor++;
                countColor %= 141;
            }
            while (badColors.Contains(countColor));

            return color;
        }

    }
}
