﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Routing.Model;

namespace Routing.Adapters
{
    public interface IAdapter<T> where T:BaseModel
    {
        List<T> getListData();
    }
}
