﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Routing.Model;

namespace Routing.Adapters
{
    public class OrganizationAdapter : IAdapter<Organization>
    {
        public List<Organization> getListData()
        {
            return new List<Organization>() { 
            new Organization() { index=0, name="EKPC"},
            new Organization() {index=1, name="Org 1"},
            new Organization() {index=2, name="Org 2"},
            };
        }
    }
}
