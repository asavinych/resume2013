﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Routing.ConvectersTypes
{
    public static class ExtensionMethods
    {
        public static int GetIntValue(this object value)
        {
            try
            {
                return Convert.ToInt32(value);
            }
            catch
            {
                return 0;
            }
        }
        public static bool GetBoolean(this object value)
        {
            try
            {
                return Convert.ToBoolean(value);
            }
            catch
            {
                return false;
            }
        }

        public static float GetFloatValue(this object value)
        {
            if (value.ToString() == "")
                return 0;
            try
            {
                return (float)Convert.ToDouble(value);
            }
            catch
            {
                return 0;
            }
        }

        public static long GetLongValue(this object value)
        {
            if (value.ToString() == "")
                return 0;
            try
            {
                return (long)value;
            }
            catch
            {
                return 0;
            }
        }

        public static string GetStringValue(this object value)
        {
            if (value == DBNull.Value)
                return string.Empty;
            return value.ToString();
        }

        public static DateTime GetDateTimeValue(this object value)
        {
            return (DateTime)value;
        }



        public static string ToSql(this object value)
        {
            return value.ToString().Replace("'", "''");
        }
        public static string StripDashes(this object value)
        {
            return (value.ToString().Contains("-"))
                    ? value.ToString().Replace("-", "")
                    : value.ToString();
        }


    }
}
