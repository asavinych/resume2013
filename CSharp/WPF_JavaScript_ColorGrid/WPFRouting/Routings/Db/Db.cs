﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Configuration;



namespace Routing
{
	public class Db : IDisposable
	{
		// Internal members
		protected string ConnString;
		protected SqlConnection Conn;
		protected SqlTransaction Trans;
		protected bool Disposed;

		
		public SqlTransaction Transaction { get { return Trans; } }
		public Db()
		{
            ConnString = Routing.Properties.Settings.Default["ConnectionString"].ToString();
			Connect();
		}

		public Db(string connString)
		{
			ConnString = connString;
			Connect();
		}

		protected void Connect()
		{
			Conn = new SqlConnection(ConnString);
			Conn.Open();
		}

		public SqlCommand CreateCommand(string qry, CommandType type, params object[] args)
		{
			var cmd = new SqlCommand(qry, Conn);

			// Associate with current transaction, if any
			if (Trans != null)
				cmd.Transaction = Trans;

			// Set command type
			cmd.CommandType = type;

			// Construct SQL parameters
			if (args != null)
			{
				for (int i = 0; i < args.Length; i++)
				{
					if (args[i] is string && i < (args.Length - 1))
					{
						var parm = new SqlParameter { ParameterName = (string)args[i], Value = args[++i] };
						cmd.Parameters.Add(parm);
					}
					else if (args[i] is SqlParameter)
					{
						cmd.Parameters.Add((SqlParameter)args[i]);
					}
					else throw new ArgumentException("Invalid number or type of arguments supplied");
				}
			}
			return cmd;
		}

		#region Exec Members

		public int ExecNonQuery(string qry, params object[] args)
		{
			using (SqlCommand cmd = CreateCommand(qry, CommandType.Text, args))
			{
				return cmd.ExecuteNonQuery();
			}
		}

		public int ExecNonQueryProc(string proc, params object[] args)
		{
			using (SqlCommand cmd = CreateCommand(proc, CommandType.StoredProcedure, args))
			{
				return cmd.ExecuteNonQuery();
			}
		}

		public object ExecScalar(string qry, params object[] args)
		{
			using (SqlCommand cmd = CreateCommand(qry, CommandType.Text, args))
			{
				return cmd.ExecuteScalar();
			}
		}

		public object ExecScalarProc(string qry, params object[] args)
		{
			using (SqlCommand cmd = CreateCommand(qry, CommandType.StoredProcedure, args))
			{
				return cmd.ExecuteScalar();
			}
		}

		public SqlDataReader ExecDataReader(string qry, params object[] args)
		{
			using (SqlCommand cmd = CreateCommand(qry, CommandType.Text, args))
			{
				return cmd.ExecuteReader();
			}
		}

		public SqlDataReader ExecDataReaderProc(string qry, params object[] args)
		{
			using (SqlCommand cmd = CreateCommand(qry, CommandType.StoredProcedure, args))
			{
				return cmd.ExecuteReader();
			}
		}

		public DataSet ExecDataSet(string qry, params object[] args)
		{
			using (SqlCommand cmd = CreateCommand(qry, CommandType.Text, args))
			{
				SqlDataAdapter adapt = new SqlDataAdapter(cmd);
				DataSet ds = new DataSet();
				adapt.Fill(ds);
				return ds;
			}
		}

		public DataSet ExecDataSetProc(string qry, params object[] args)
		{
			using (SqlCommand cmd = CreateCommand(qry, CommandType.StoredProcedure, args))
			{
				SqlDataAdapter adapt = new SqlDataAdapter(cmd);
				DataSet ds = new DataSet();
				adapt.Fill(ds);
				return ds;
			}
		}

		#endregion

		#region Transaction Members

		public SqlTransaction BeginTransaction()
		{
			Rollback();
			Trans = Conn.BeginTransaction();
			return Transaction;
		}

		public void Commit()
		{
			if (Trans != null)
			{
				Trans.Commit();
				Trans = null;
			}
		}

		public void Rollback()
		{
			if (Trans != null)
			{
				Trans.Rollback();
				Trans = null;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!Disposed)
			{
				if (disposing)
				{
					if (Conn != null)
					{
						Rollback();
						Conn.Dispose();
						Conn = null;
					}
				}
				Disposed = true;
			}
		}

		#endregion
	}
}