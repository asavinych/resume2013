﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Reflection;
using System.ComponentModel;
using System.Windows;

namespace Routing.Model
{
    //for event when the data has changed in the DataGrid
    public delegate void ChangedDisplayEventHandler(object sender, EventArgs e);

    public class CustomerPoints:BaseModel
    {
        
        [DisplayName("")]
        public bool display
        {
            get ;
            set ;
        }

        [DisplayName("Color")] 
        public Color color
        {
            get;
            set;
        }

        public string customerId
        {
            get;
            set;
        }

        public int countApp
        {
            get
            {
                return points.Count;
            }
        }

        public TimeSpan timeDetour
        {
            get;
            set;
        }

        public decimal distance
        {
            get;
            set;
        }

        public Point startPoint;

        public List<Point> points;

     
        public event ChangedDisplayEventHandler displayChanged;

        public CustomerPoints(string customer)
        {
            points = new List<Point>();
            customerId = customer;
            startPoint = new Point();
            timeDetour = new TimeSpan(0, 0, 0);
            distance = 0;
            display = false;
        }

        public CustomerPoints()
        {
            points = new List<Point>();
            customerId = String.Empty;
        }
       
    }
}
