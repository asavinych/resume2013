﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Routing.Model
{

    public class CustomerCoordinates
    {
        public Int64 CustomerId { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public string ApiName { get; set; }
        public String FromTime { get; set; }
        public String ToTime { get; set; }
        public string location { get; set; }
        public double LongitudeService { get; set; }
        public double LatitudeService { get; set; }
        public string ZipCode { get; set; }
        public int ApptGroup { get; set; }
        public bool IsTaken { get; set; }
    }

    public class ZipCoordinates
    {
        public int Id { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string ZipCode { get; set; }
    }

    public class UserCoordinates
    {
        public int UserId { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }

    public class RoutePair
    {
        public Int64 CustomerIdStart { get; set; }
        public Int64 CustomerIdEnd { get; set; }
        public Int32 Distance { get; set; }
        public int ApptGroupStart { get; set; }
        public int ApptGroupEnd { get; set; } 
        public bool IsTaken { get; set; }
        public int RoutePoint { get; set; }   // 0, 1 start,2 end
       
    }

    public class GroupByAppTime
    {
        public int GroupId { get; set; }
        public Int64 CustomerId { get; set; }
        public int ApptGroup { get; set; }
        public int SeqNum { get; set; }
    }

    public class GroupOrderAppt
    {
        public int GroupId { get; set; }
        public int Version { get; set; }
        public Int64 Cost { get; set; }
        public List<Int64> Order { get; set; }
    }
}
