﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Routing.Model
{
    public struct Marker
    {
        public decimal lat;

        public decimal lng;

        public int i;
        
        public string text;

        public string color;
    }
}
