﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Routing.Model
{
    public class Organization:BaseModel
    {
        public int index
        {
            get;
            set;
        }

        public string name
        {
            get;
            set;
        }
    }
}
