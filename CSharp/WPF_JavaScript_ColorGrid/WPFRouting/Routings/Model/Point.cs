﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Routing.Model
{
    public class Point
    {
        public decimal latitude
        {
            get;
            set;
        }

        public decimal longitude
        {
            get;
            set;
        }

        public string text
        {
            get;
            set;
        }
    }
}
