﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Routing.View
{
    /// <summary>
    /// Interaction logic for ChangeOrderAppoitments.xaml
    /// </summary>
    public partial class ChangeOrderAppoitments : Window
    {
        private List<Model.Point> liData;

        public List<Model.Point> data
        {
            get
            {
                return liData;
            }
            set
            {
                liData = value;
                dGPoint.Items.Refresh();
            }
        }

        public ChangeOrderAppoitments(List<Model.Point> _data)
        {
            InitializeComponent();
            liData = new List<Model.Point>(_data);
            dGPoint.ItemsSource = liData;
            dGPoint.Items.Refresh();
        }

        private void toTop_Click(object sender, RoutedEventArgs e)
        {
            int i = dGPoint.SelectedIndex;
            if (i > 0) 
            {
                Model.Point p = liData[i];
                liData.RemoveAt(i);
                liData.Insert(0, p);

                refreshAndFocusDataGrid(0);
            }
        }

        private void toUp_Click(object sender, RoutedEventArgs e)
        {
            int i = dGPoint.SelectedIndex;
            if (i > 0) 
            {
                Model.Point p = liData[i];
                liData.RemoveAt(i);
                liData.Insert(i-1, p);

                refreshAndFocusDataGrid(i - 1);
            }
        }

        private void toDown_Click(object sender, RoutedEventArgs e)
        {
            int i = dGPoint.SelectedIndex;
            if (i > -1 && i<liData.Count-1) 
            {
                Model.Point p = liData[i];
                liData.RemoveAt(i);
                liData.Insert(i +1, p);

                refreshAndFocusDataGrid(i+1);
            }
        }

        private void toEnd_Click(object sender, RoutedEventArgs e)
        {
            int i = dGPoint.SelectedIndex;
            if (i > -1 && i < liData.Count - 1)
            {
                Model.Point p = liData[i];
                liData.RemoveAt(i);
                liData.Add(p);

                refreshAndFocusDataGrid(liData.Count - 1);
            }
        }

        private void refreshAndFocusDataGrid(int selectedItemIndex)
        {
            dGPoint.Items.Refresh();
            dGPoint.SelectedIndex = selectedItemIndex;
            dGPoint.Focus();
        }


    }
}
