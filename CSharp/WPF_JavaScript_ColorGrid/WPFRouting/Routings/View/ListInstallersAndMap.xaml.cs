﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Routing.Model;
using Routing.UserInterfaceLogic;
using Routing.Adapters;
using System.Windows.Navigation;
using mshtml;
using System.Threading;

namespace Routing.View
{
    /// <summary>
    /// Interaction logic for ListInstallersAndMap.xaml
    /// </summary>
    public partial class ListInstallersAndMap : Window
    {

        /// <summary>
        /// Data about installers
        /// </summary>
        private List<CustomerPoints> _cp;

        /// <summary>
        /// For create list customer points
        /// </summary>
        private IAdapter<CustomerPoints> customerPoint;

        private Organization selectOrganization;

        public ListInstallersAndMap()
        {
            InitializeComponent();

            SelectOrganization selectOrg = new SelectOrganization();
            selectOrg.ShowDialog();
            if (!selectOrg.dialogResult)
            {
                this.Close();
                return;
            }

            selectOrganization = selectOrg.selectedOrganization;

            customerPoint = new CustomerPointsFromFileAdapter();

            init();
        }

        private void init()
        {

            _NavigatePage();

            _cp = customerPoint.getListData();

            dGInst.ItemsSource = _cp;
            dGInst.Items.Refresh();

           
        }

        private void _NavigatePage()
        {
            Uri uri = new Uri(@"pack://application:,,,/HTML/example.html");

            Stream source = Application.GetContentStream(uri).Stream;
            webBrowser.LoadCompleted += BrowserOnLoadComleted;
            webBrowser.NavigateToStream(source);
        }

        /// <summary>
        /// Dynamic loading JS library working with JSON in HTML page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void BrowserOnLoadComleted(object sender, NavigationEventArgs args)
        {
            var doc = (HTMLDocument)webBrowser.Document;
            var head = doc.getElementsByTagName("head").Cast<HTMLHeadElement>().First();
            var script = (IHTMLScriptElement)doc.createElement("script");
            var dir = AppDomain.CurrentDomain.BaseDirectory + Routing.Properties.Settings.Default["JSONLib"].ToString();
            script.src = dir;
            script.type = "text/javascript";
            head.appendChild((IHTMLDOMNode)script);
        }

        private void SelectAll_Click(object sender, RoutedEventArgs e)
        {
            ActionForAllMarkers(true);
        }

        private void ClearAll_Click(object sender, RoutedEventArgs e)
        {
            ActionForAllMarkers(false);
        }

        private void ActionForAllMarkers(bool display)
        {
            for (int i = 0; i < _cp.Count; i++)
            {
                _cp[i].display = display;
            }

            Refresh_Click(null, null);
            dGInst.Items.Refresh();
        }

        //Changing map.
        private void x_displayChanged(object sender, EventArgs e)
        {
            CustomerPoints cpInner = (CustomerPoints)sender;

          
            if (cpInner.points.Count > 0 && cpInner.display)
            {
                this.webBrowser.InvokeScript("setCenterMap", cpInner.points[0].latitude, cpInner.points[0].longitude);
                string colorRes = string.Format("#{0:X2}{1:X2}{2:X2}", cpInner.color.R, cpInner.color.G, cpInner.color.B);

                List<Marker> liMarkers = new List<Marker>();
                int startNumber=0;
                if (cpInner.startPoint != null)
                {
                    liMarkers.Add(new Marker() { lat = cpInner.startPoint.latitude, lng = cpInner.startPoint.longitude, i = startNumber, text = cpInner.startPoint.text, color = colorRes });
                    startNumber++;
                }

                for (int j = 0; j < cpInner.points.Count; j++)
                {
                    liMarkers.Add(new Marker() { lat = cpInner.points[j].latitude, lng = cpInner.points[j].longitude, i = j + startNumber, color = colorRes, text = cpInner.points[j].text });
                }

                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string resStr = serializer.Serialize(liMarkers);
                this.webBrowser.InvokeScript("writeMarkers", resStr);
            }
        }

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            removeAllMarkers();

            _cp.ForEach(x => x_displayChanged(x, null));
        }

        private void removeAllMarkers()
        {
            this.webBrowser.InvokeScript("removeMarkers");
        }

        private void ChangeOrder_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < _cp.Count; i++)
            {
                if (_cp[i].display)
                {
                    ChangeOrderAppoitments ch = new ChangeOrderAppoitments(_cp[i].points);

                    ch.ShowDialog();

                    _cp[i].points = ch.data;
                }
            }
        }

        private void Transfer_Click(object sender, RoutedEventArgs e)
        {
            CustomerPoints first = null;
            CustomerPoints second = null;
            
            for (int i = 0; i < _cp.Count; i++)
            {
                if (_cp[i].display)
                {
                    if (first == null)
                    {
                        first = _cp[i];
                    }
                    else
                    {
                        if (second == null)
                        {
                            second = _cp[i];
                            break;
                        }
                    }
                }
            }

            if (first != null && second != null)
            {
                TransferAppoitments ta = new TransferAppoitments(first, second);
                ta.ShowDialog();

                first = ta.getFirst;
                second = ta.getSecond;
            }
        }

        private void getDistance()
        {
           // Thread.Sleep(3000);
            foreach (var cpInner in _cp)
            {
                if (cpInner.points.Count > 0)
                {
                    List<Marker> liMarkers = new List<Marker>();
                    int startNumber = 0;
                    if (cpInner.startPoint != null)
                    {
                        liMarkers.Add(new Marker() { lat = cpInner.startPoint.latitude, lng = cpInner.startPoint.longitude, i = startNumber, text = cpInner.startPoint.text, color = "000000" });
                        startNumber++;
                    }

                    for (int j = 0; j < cpInner.points.Count; j++)
                    {
                        liMarkers.Add(new Marker() { lat = cpInner.points[j].latitude, lng = cpInner.points[j].longitude, i = j + startNumber, color = "000000", text = cpInner.points[j].text });
                    }

                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    string resStr = serializer.Serialize(liMarkers);

                    this.Dispatcher.Invoke((Action)(() =>
                                                {
                                                    this.webBrowser.InvokeScript("calcDistance", resStr);
                                                }));
                    Thread.Sleep(500);
                    this.Dispatcher.Invoke((Action)(() =>
                    {
                        string[] distance_time=this.webBrowser.InvokeScript("getDistance_Time").ToString().Split(new char[] {'_'}, StringSplitOptions.RemoveEmptyEntries);
                        decimal distance=0;
                        int time=0;
                        if (distance_time.Length>0 && decimal.TryParse(distance_time[0], out distance) && int.TryParse(distance_time[1], out time))
                        {
                            cpInner.distance=Math.Round(distance/1609, 0);
                            cpInner.timeDetour = new TimeSpan(0, 0, time);
                        }
                    }));

                    this.Dispatcher.Invoke((Action)(() =>
                    {
                        dGInst.Items.Refresh();
                    }));
                    Thread.Sleep(500);
                }
            }
           
        }

        private void CalcTimeDist_Click(object sender, RoutedEventArgs e)
        {
            Task tt = new Task(() =>
            {
                getDistance();
            });

            tt.Start();
        }
    }
}
    