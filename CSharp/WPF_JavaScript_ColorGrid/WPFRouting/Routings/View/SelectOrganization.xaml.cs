﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Routing.Adapters;
using Routing.Model;

namespace Routing.View
{
    /// <summary>
    /// Interaction logic for SelectOrganization.xaml
    /// </summary>
    public partial class SelectOrganization : Window
    {

        private IAdapter<Organization> organizationAdapter;
        private List<Organization> liOrganization;

        public bool dialogResult
        {
            get;
            set;
        }

        public Organization selectedOrganization
        {
            get
            {
                int index = organizationsComboBox.SelectedIndex;

                if (index>-1)
                    return liOrganization[index];

                return null;
            }
        }

        public DateTime selectedDate
        {
            get;
            set;
        }

        public SelectOrganization()
        {
            InitializeComponent();
            organizationAdapter = new OrganizationAdapter();
            liOrganization = organizationAdapter.getListData();

            organizationsComboBox.ItemsSource = liOrganization.Select(x => x.name);
            organizationsComboBox.SelectedIndex = 0;

            datePicker.SelectedDate = DateTime.Now;
            
            selectedDate = DateTime.Now;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            dialogResult = false;
            this.Close();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            dialogResult = true;
            this.Close();
        }

        private void MonthlyCalendar_SelectedDatesChanged(object sender,EventArgs e)
        {
            selectedDate = datePicker.SelectedDate.Value;
        }

        private void datePicker_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            MonthlyCalendar_SelectedDatesChanged(sender, null);
        }

        private void datePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            MonthlyCalendar_SelectedDatesChanged(sender, null);
        }

    }
}
