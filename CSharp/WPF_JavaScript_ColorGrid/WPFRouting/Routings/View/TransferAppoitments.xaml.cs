﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Routing.Model;

namespace Routing.View
{
    /// <summary>
    /// Interaction logic for TransferAppoitments.xaml
    /// </summary>
    public partial class TransferAppoitments : Window
    {
        private CustomerPoints _first;
        private CustomerPoints _second;

        public CustomerPoints getFirst
        {
            get
            {
                return _first;
            }
        }

        public CustomerPoints getSecond
        {
            get
            {
                return _second;
            }
        }

        public TransferAppoitments(CustomerPoints first, CustomerPoints second)
        {
            InitializeComponent();

            _first = first;
            _second = second;

            dGOne.ItemsSource = _first.points;
            dGTwo.ItemsSource = _second.points;

            dGOne.Items.Refresh();
            dGTwo.Items.Refresh();
        }

        private void AllInSecond_Click(object sender, RoutedEventArgs e)
        {
            _second.points.AddRange(_first.points);
            _first.points.Clear();

            refreshAllGrid();
        }

        private void AllInFirst_Click(object sender, RoutedEventArgs e)
        {
            _first.points.AddRange(_second.points);
            _second.points.Clear();

            refreshAllGrid();
        }

        private void OneInSecond_Click(object sender, RoutedEventArgs e)
        {
            int i = dGOne.SelectedIndex;

            if (i > -1)
            {
                Model.Point p = _first.points[i];
                _first.points.RemoveAt(i);
                _second.points.Add(p);
            }

            refreshAllGrid();
        }

        private void OneInFirst_Click(object sender, RoutedEventArgs e)
        {
            int i = dGOne.SelectedIndex;

            if (i > -1)
            {
                Model.Point p = _second.points[i];
                _second.points.RemoveAt(i);
                _first.points.Add(p);
            }

            refreshAllGrid();
        }

        private void refreshAllGrid()
        {
            dGOne.Items.Refresh();
            dGTwo.Items.Refresh();
        }
    }
}
