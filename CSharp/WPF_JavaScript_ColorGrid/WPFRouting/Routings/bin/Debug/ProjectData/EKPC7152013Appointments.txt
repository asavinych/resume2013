Job Type	Address	Account Multiplier	Installer	SEQ	Date	Time	Latitude	Longitude	NumOfDevices	Account	City	Comments	CoopName	AC_Count	WH_Count	Meter_Num	Customer	FullAddress	HomePhone	MobilePhone	CustomerID
T1	1427 CORAL HILL LECTA RD	64737-2	Anthony Fulkerson	1	7/15/2013	8:00 AM	37.028569	-85.855318	2	0040840001-34	GLASGOW	7/8 apt set for 7/15 8-12 1 wh please call on the way 270-678-40769 TG	EKPC - Farmers	0	0	64737	CLAYTON PEDEN	"1427 CORAL HILL LECTA RD, GLASGOW, KY. "	270-678-4076		2048036
T1	314B SUSANNAH AV	99999-2	Anthony Fulkerson	2	7/15/2013	12:00 PM	37.830871	-85.440656	2	203403001	BARDSTOWN	2 of 2 appts/meet her at 110 treetop first fb 7/8 apt set for 7/15 12-3 2 a/c 2 wh please call befor	EKPC - Salt River	0	0		SARAH SPAINHOWARD	"314B SUSANNAH AV, BARDSTOWN, KY. "	502-348-6069		2275099
T1	1520 HURRICANE HILLS	143853-2	Anthony Fulkerson	3	7/15/2013	12:00 PM	37.829412	-85.638818	2	0026542001-21	BOSTON	Priority appt --- appt set for 07/15 between 12pm-3pm--call before arrival 502-833-2886--bg	EKPC - Salt River	0	0	143853	JIM GRAF	"1520 HURRICANE HILLS, BOSTON, KY. "	502-833-2886		2232496
T1	110 TREE TOP DR	131127-2	Anthony Fulkerson	4	7/15/2013	12:00 PM	37.850978	-85.460933	2	0075891021-21	BARDSTWON	Appt 1 of 2..Appt set for 7/15 between 12p-3p--Call before arrival 502-348-6069--CKH	EKPC - Salt River	1	1	131127	JOHN FRAZIER	"110 TREE TOP DR, BARDSTWON, KY. "	502-348-6069	502-827-0310	2247233
T1	155 BLAKENROD BLVD	123237-2	Anthony Fulkerson	5	7/15/2013	12:00 PM	37.897459	-85.473044	2	0185779001-21	COXS CREEK	CUSTOMER RESCHEDULING APPT B/C TECH WAS ONLY ABLE TO INSTALL WH NOT AC DUE TO RAIN-502-349-0357//ONL	EKPC - Salt River	2	1	123237	ANDREW LEWANDOWSKI	"155 BLAKENROD BLVD, COXS CREEK, KY. "	502-349-0357	NONE	2261623
T1	1137 GRAYS RUN RD	99999-2	Anthony Fulkerson	6	7/15/2013	3:00 PM	37.971026	-85.338341	2	202948001	TAYLORSVILLE	apt set 7/15 3-6pm//contact 502-724-7454//jdb	EKPC - Salt River	1	1		AMANDA E STEWART	"1137 GRAYS RUN RD, TAYLORSVILLE, KY."	502-724-7454		2274350
T1	275 SALEM SCH RD	66391-2	Anthony Fulkerson	7	7/15/2013	3:00 PM	37.759255	-86.041867	2	0815002600-51	RINEYVILLE	"Called Customer to reschedule their appointment. Customer rescheduled to 7/15 from 3PM to 6PM. 1AC, "	EKPC - Nolin	1	1	66391	LESLIE ROGERS	"275 SALEM SCH RD, RINEYVILLE, KY. "	270-877-5000		2167947
T1	27 CHESTNUT DR	22815-2	Casey Fike	1	7/15/2013	8:00 AM	37.570836	-83.414093	2	2704606900-56	JACKSON	1AC BEST CONTACT 606-233-6323 POS	EKPC - LickingValley	0	0	22815	ADAM FELTNER	"27 CHESTNUT DR, JACKSON, KY. "	606-693-1092	606-233-6323	2123244
T1	240A CREECH RD	20166-2	Casey Fike	2	7/15/2013	8:00 AM	37.751515	-83.619898	2	1409409900-56	PINE RIDGE	1WH//606-668-9305//JM	EKPC - LickingValley	0	1	20166	JESSIE CREECH	"240A CREECH RD, PINE RIDGE, KY. "	606-668-9305		2117417
T1	78 COLLINSWOOD TRL	31588-2	Casey Fike	3	7/15/2013	8:00 AM	37.789416	-83.646869	2	1406200700-56	PINE RIDGE	1WH BEST CONTACT 606-668-6887 POS	EKPC - LickingValley	0	0	31588	OTTIS TRENT	"78 COLLINSWOOD TRL, PINE RIDGE, KY."	606-668-6887		2127411
T1	3004 HIGHWAY 3193 LOT #A	26879-2	Casey Fike	4	7/15/2013	12:00 PM	37.595641	-83.424465	2	2702607200-56	JACKSON	6-19//1AC 1WH//hasnt heard of program//please call before arrival//JJB	EKPC - LickingValley	1	1	26879	CHARLES HOLLAN	"3004 HIGHWAY 3193 LOT #A, JACKSON, KY. "	606-666-7387		2134364
T1	412 GEVEDON RD	10954-2	Casey Fike	5	7/15/2013	3:00 PM	37.765151	-83.610119	2	1407201300-56	PINE RIDGE	"1AC,1WH BEST CONTACT 606-668-7754 POS"	EKPC - LickingValley	0	0	10954	STEVE PARKS	"412 GEVEDON RD, PINE RIDGE, KY. "	606-668-7754		2127454
T1	202 ROBINWOOD DR	53670-2	Chad Snapp	1	7/15/2013	8:00 AM	38.177934	-85.205004	2	77783583001	SHELBYVILLE	call when on the way 502-633-5520 1wh/ac appt 7/15 8-12 jrc	EKPC - Shelby	1	1	53670	JOANN J GIBBS	"202 ROBINWOOD DR, SHELBYVILLE, KY. "	502-633-5520		2288064
T1	1370 GLENCARIN RD	32251-2	Chad Snapp	2	7/15/2013	8:00 AM	37.744733	-83.63115	2	1409200103-56	ROGERS	1wh//606-668-7133//JM	EKPC - LickingValley	0	1	32251	WANDA THOMPSON	"1370 GLENCARIN RD, ROGERS, KY."	606-668-7133		2127731
T1	3373 MT FORAKER DR	33014931-2	Chad Snapp	3	7/15/2013	12:00 PM	37.978223	-84.461853	2	1084270001	LEXINGTON	7-9//7-15 12pm-3//1WH//304-550-1362//JJB	EKPC - Blue Grass	0	1	33014931	CASTANEDA-GOMEZ ROCIO	"3373 MT FORAKER DR, LEXINGTON, KY. "	304-550-1362		1977208
T1	1452 RICHMOND RD HOUSE	93008043-2	Chad Snapp	4	7/15/2013	12:00 PM	37.570189	-84.29296	2	1091810002	Berea	7-9//7-15 12-3pm//1AC//256-479-9565//JJB	EKPC - Blue Grass	1	0	93008043	PATRICIA WILSON	"1452 RICHMOND RD HOUSE, Berea, KY. "	256-479-9565		1979885
T1	438 EASTLAND ACRES	101231-2	Chad Snapp	5	7/15/2013	12:00 PM	37.606486	-84.560128	2	0051442002-27	LANCASTER	Customer called to REsched apt/859-339-9335/Call prior to arriving 1WH 1AC	EKPC - InterCounty	1	1	101231	REX SPEAKE	"438 EASTLAND ACRES, LANCASTER, KY. "	859-339-9335		2082747
T1	750 OLD KENTUCKY 68	28795-2	Chad Snapp	6	7/15/2013	3:00 PM	37.544741	-85.30323	2	0003081701-27	LEBANON	reschedule--07/05--07/15--270-692--6671--call before arrival--1a/c-1w/h--bg	EKPC - InterCounty	0	0	28795	DAVID SALLEE	"750 OLD KENTUCKY 68, LEBANON, KY. "	270-692-6671		2112129
T1	1009 WOODLAWN DR	37219566-2	Cliff Land	1	7/15/2013	8:00 AM	38.083262	-84.918435	2	17594001	LAWRENCEBURG	Appt set 7/9 7/15 8pm-12pm 1AC Best Contact 304-444-6906 POS	EKPC - Blue Grass	1	0	37219566	TANIA L PETRY	"1009 WOODLAWN DR, LAWRENCEBURG, KY."	304-444-6906	304-444-6906	1941736
T1	1200 LEMON NORTHCUTT	151768-2	Cliff Land	2	7/15/2013	12:00 PM	38.720458	-84.571528	2	0426056001-37	Dry Ridge	7/8 apt set for 7/15 12-3 1 a/c 1 wh best contact number 606-666-2691 TG	EKPC - Owen	0	0	151768	DAN GRIFFITH	"1200 LEMON NORTHCUTT, Dry Ridge, KY.  "	606-666-2691	606-272-0463	2223932
T1	1225 SHERMAN NEWTON RD 	110323-2	Cliff Land	3	7/15/2013	12:00 PM	38.742854	-84.580106	2	0076085001-37	CRITTENDEN	PRIORITY 1 a/c 1 w/h please call before arrival 859-428-2156	EKPC - Owen	1	1	110323	ARDITH N CHANCE	"1225 SHERMAN NEWTON RD , CRITTENDEN, KY."	859-428-2156		2225611
T1	592 NEWMAN RD	140582-2	Cliff Land	4	7/15/2013	12:00 PM	38.727992	-84.266894	2	0082891002-37	FALMOUTH	7/5 apt set for 7/15 12-3 1 a/c 1 wh please call before arrival 859-654-1239 tg	EKPC - Owen	0	0	140582	NEAL AND SANDY REED	"592 NEWMAN RD, FALMOUTH, KY. "	859-654-1239	859-750-2512	2191297
T1	268 CONVICT RD	91156669-2	Cliff Land	5	7/15/2013	12:00 PM	38.202514	-83.995023	2	1091121001	SHARPSBURG	APPT SET 7/9--7/15 12PM-3PM 1WH--BEST CONTACT 859-533-2290 POS	EKPC - Blue Grass	0	1	91156669	MICHAEL G KRAHWINKEL	"268 CONVICT RD, SHARPSBURG, KY. "	859-553-2290	270-929-6289	1968235
T1	1700 BRIDGEPORT BENSON	92684898-2	Cliff Land	6	7/15/2013	3:00 PM	38.181508	-84.959471	2	1070514001	FRANKFORT	APPT SET FOR 7/15 3-6--2 AC 1WH--Best contact number 501-804-6472--CJB	EKPC - Blue Grass	2	1	92684898	CARL GRAY	"1700 BRIDGEPORT BENSON, FRANKFORT, KY."	501-804-6472		1981523
T1	750 HWY 151 RD	37220145-2	Cliff Land	7	7/15/2013	3:00 PM	38.148994	-84.992432	2	1081885002	FRANKFORT	"Appt Set 7/8--7/9 1AC,1WH BEST CONTACT 502-219-1836 POS "	EKPC - Blue Grass	1	1	37220145	JESSICA TILLMAN	"750 HWY 151 RD, FRANKFORT, KY."	502-219-1836		1981566
T1	6870 VANTAGE CT	164881-2	James Griggs	1	7/15/2013	8:00 AM	39.000635	-84.691583	2	0043001001-37	FLORENCE	apt set 7/15 8-12pm//contact 859-283-0538 or 859-630-6762//ok to install//jdb	EKPC - Owen	0	0	164881	PATRICIA L AND SUCKOW	"6870 VANTAGE CT, FLORENCE, KY. "	859-283-0538	859-581-5800	2181842
T1	8637 TREELINE DR LOT 328	180449-2	James Griggs	2	7/15/2013	8:00 AM	38.980954	-84.701761	2	0122604001-37	FLORENCE	apt set 7/15 8-12pm//contact 859 371 4536//1ac//1wh//ok to install//jdb	EKPC - Owen	1	1	180449	LARRY AND TERRENE DILLON	"8637 TREELINE DR LOT 328, FLORENCE, KY. "	859-371-4536	859-371-4707	2204011
T1	8609 TREELINE DR	108016-2	James Griggs	3	7/15/2013	12:00 PM	38.981361	-84.701165	2	0122109001-37	FLORENCE	1 AC//0 WH// 859-640-8422 or 859-640-8509// customer will not be home but it is ok to install withou	EKPC - Owen	1	0	108016	SUZANNE AND ROBERT FRANCIS	"8609 TREELINE DR, FLORENCE, KY. "	859-640-8422	859-640-8509	2204692
T1	6127 PAR FOUR CT LOT 125 	116082-2	James Griggs	4	7/15/2013	12:00 PM	39.003931	-84.682956	2	0106681001-37	FLORENCE	PRIORITY/PLEASE BE THERE BEFORE 2PM 7/15//8593720330 CALL BEFORE ARRIVAL	EKPC - Owen	1	1	116082	SLAVKO AND SLAVICA RADAKOVIC	"6127 PAR FOUR CT LOT 125 , FLORENCE, KY. "	859-802-4458	8593720330	2199136
T1	7524 ROXBURY CT	151272-2	James Griggs	5	7/15/2013	12:00 PM	38.9432	-84.544414	2	0012417008-37	INDEPENDENCE	7/8 we rescheduled appt for 7/15 12-3pm 1ac ok to install if customer not home 859-653-5204 MR	EKPC - Owen	0	0	151272	Oaks Nicole	"7524 ROXBURY CT, INDEPENDENCE, KY. "	8596535204		2221193
T1	4293 RICHARDSON RD APT 1	109973-2	James Griggs	6	7/15/2013	3:00 PM	38.967458	-84.607468	2	0081952001-37	INDEPENDENCE	"Appt set for 7/15 3-6--2AC 1WH--Best contact number 859-282-8951--CJB (THIS ISN'T AN APT, BUT A SING"	EKPC - Owen	2	1	109973	TIMOTHY LEE AND JOY S TAYLOR	"4293 RICHARDSON RD APT 1, INDEPENDENCE, KY. "	859-282-8951		2191489
T1	2477 FERDINAND DR	132256-2	James Griggs	7	7/15/2013	3:00 PM	39.033673	-84.708079	2	0079480001-37	BURLINGTON		EKPC - Owen	1	0	132256	CHRISTI BAILEY	"2477 FERDINAND DR, BURLINGTON, KY. "	859-534-5131	859-341-5260	2190666
T1	6245 E TYGARTS RD	27906969-2	John Scott	1	7/15/2013	8:00 AM	38.660188	-82.90565	2	0067393001-61	GREENUP	6-20//1AC 1WH//hasnt heard of program//JJB	EKPC - Grayson	1	1	27906969	JOSH GRAYSON	"6245 E TYGARTS RD, GREENUP, KY. "	606-932-3263		2074582
T1	220 SWINT HOLW	27908131-2	John Scott	2	7/15/2013	8:00 AM	38.439964	-82.913061	2	0027881001-61	GREENUP	6/19 apt set for 7/15 8-12 1 wh 1 a/c please come as early as possible and call before arrival 606 4	EKPC - Grayson	0	0	27908131	ROBERT EUGENE SMITH	"220 SWINT HOLW, GREENUP, KY. "	606-473-5334	606-615-4395 	2067342
T1	1187 RATTLESNAKE FORK	81285875-2	John Scott	3	7/15/2013	8:00 AM	38.250668	-83.057235	2	0099141001-61	OLIVE HILL	6-26//1WH//Hasnt heard of program//JJB	EKPC - Grayson	0	1	81285875	GLENN BARKER	"1187 RATTLESNAKE FORK, OLIVE HILL, KY."	606-474-5869		2075365
T1	169 WARREN RD	82863057-2	John Scott	4	7/15/2013	12:00 PM	38.300632	-83.17533	2	0065413001-61	OLIVE HILL	RS per cust req JMC	EKPC - Grayson	0	0	82863057	DAVID W MAGGARD	"169 WARREN RD, OLIVE HILL, KY. "	606-738-9908		2073693
T1	5520 N. KY 706	82878079-2	John Scott	5	7/15/2013	12:00 PM	38.12777	-83.059017	2	0066039002-61	SANDY HOOK		EKPC - Grayson	0	1	82878079	LISA RATLIFF	"5520 N. KY 706, SANDY HOOK, KY. "	606-738-4017		2074086
T1	1370 WEAVER RIDGE	82586152-2	John Scott	6	7/15/2013	3:00 PM	38.163072	-83.347575	2	0100574001-61	MOREHEAD	1WH BEST CONTACT 606-784-4624 POS	EKPC - Grayson	0	1	82586152	BERRY KIDD	"1370 WEAVER RIDGE, MOREHEAD, KY. 40351"	606-784-4624		2075563
T1	465 WHITE ROAD	82879285-2	John Scott	7	7/15/2013	3:00 PM	38.230088	-83.269889	2	67669002	OLIVE HILL	THE CUSTOMER HAS 1 ELECTRIC 40 GALLON WATER HEATER...CB.	EKPC - Grayson	0	1	82879285	LESTER C WILLIAMS	"465 WHITE ROAD, OLIVE HILL, KY. "	606-286-9342		2076884
T1	135 SLATE POINT PL	49004680-2	John Scott	8	7/15/2013	3:00 PM	38.440262	-82.949509	2	0064756001-61	GREENUP	REscheduled for 7/15 3-6pm-please call before arrival-- 606-831-2420 -LR	EKPC - Grayson	2	1	49004680	RICHARD SLATE	"135 SLATE POINT PL, GREENUP, KY. "	606-831-2420		2073822
T1	409 ALSIP TRL	59473-2	Larry Moore	1	7/15/2013	8:00 AM	36.914585	-84.159722	2	0962125000-57	CORBIN		EKPC - Cumberland	1	1	59473	MARION THOMAS	"409 ALSIP TRL, CORBIN, KY. "	6065281208		2017672
T1	641 Old State Route 21 Rd	255304-2	Larry Moore	2	7/15/2013	8:00 AM	36.935487	-84.922439	2	0981155001-54	SARDINIA	606-340-0623 1AC/1WH APPT 7/15 8-12 JRC	EKPC - SouthKY	1	1	255304	JOHN SHAFFER	"641 Old State Route 21 Rd, Monticello, KY."	937-446-2089		2334113
T1	1909 E WILLIAMSBURG ST APT 3	214767-2	Larry Moore	3	7/15/2013	12:00 PM	36.724433	-84.451774	2	0994351001-54	WHITLEY CITY	set appt for 7/15 12pm to 3pm priority	EKPC - SouthKY	1	1	214767	PAUL ROBERT LINDER	"1909 E WILLIAMSBURG ST APT 3, WHITLEY CITY, KY. "	606-376-8575	606-310-5151	2338958
T1	145 ST PARK RD 	246840-2	Larry Moore	4	7/15/2013	12:00 PM	36.912937	-85.028611	2	0938795003-54	JAMESTOWN	"Appt set 7/8 7/15 12-3pm 1AC, 1WH BEST CONTACTS 270-343-3205, 270-566-1518 POS"	EKPC - SouthKY	0	0	246840	WILLIAM EVITTS	"145 ST PARK RD , JAMESTOWN, KY. "	812-949-8040	502-583-1080	2317764
T1	145 ST PARK RD	246842-2	Larry Moore	5	7/15/2013	3:00 PM	36.912937	-85.028611	2	0086224002-54	JAMESTOWN	"APPT SET 7/8 7/15 3-6PM 1AC, 1WH BEST CONTACT 270-343-3205, 270-566-1518 POS"	EKPC - SouthKY	1	1	246842	WAYNE DEHONEY	"145 ST PARK RD, JAMESTOWN, KY. "	812-949-8040		2306447
T1	79 NEIGHBORLY WAY 	212901-2	Larry Moore	6	7/15/2013	3:00 PM	37.052638	-84.631466	2	0981540001-54	SOMERSET	1 AC. Please contact 20 mins before arrival. Best Contact number is 606-679-3864. Ok to install if c	EKPC - SouthKY	1	0	212901	VELDA HINKLE	"79 NEIGHBORLY WAY , SOMERSET, KY. "	606-679-3864		2335301
T1	240 SKYVIEW BOTTOM	57229-2	Larry Moore	7	7/15/2013	3:00 PM	36.999628	-82.91146	2	3496604000-57	CUMBERLAND	7/8 appt set for 7/15 3-6pm 1ac 1wh 606-589-9227 MR 	EKPC - Cumberland	1	0	57229	ANGELA HOWARD	"240 SKYVIEW BOTTOM, CUMBERLAND, KY. "	(606)589-9227		2017618
T1	10867 HIGHWAY 421 N	65447-2	Lowell McCourt	1	7/15/2013	8:00 AM	38.710356	-85.376066	2	77778463001	MILTON	apt set 7/15 8-12pm//contact 502-268-3644 or 8125693536 //1ac//1wh//ok to install//jdb	EKPC - Shelby	1	1	65447	DENNY C JACKSON	"10867 HIGHWAY 421 N, MILTON, KY. "	502-268-3644	502-268-3322	2286944
T1	1608 DRENNON RD	55562-2	Lowell McCourt	2	7/15/2013	8:00 AM	38.445126	-85.154955	2	0989354001-30	NEW CASTLE	rescheduled/priority	EKPC - Shelby	1	1	55562	TOM PIGRAM	"1608 DRENNON RD, NEW CASTLE, KY. "	502-845-1611		2291283
T1	76 PLUMMER FORK	91722205-2	Lowell McCourt	3	7/15/2013	3:00 PM	37.494787	-82.882124	2	0059467001-58	HUEYSVILLE	6-26//1WH//hasnt heard of program//JJB	EKPC - Big Sandy	0	1	91722205	ROY DALE LOVE	"76 PLUMMER FORK, HUEYSVILLE, KY. "	606-358-4311	606-946-2297	1933552
T1	4804 HEMPSTEAD	91155348-2	Mannuel Walters	1	7/15/2013	8:00 AM	37.953132	-84.473386	2	1082382001	LEXINGTON	7-9//7-15 8am-12//2AC 1WH//317-523-7229//customer will be there in the morning from 8-10:30am//JJB	EKPC - Blue Grass	2	1	91155348	MICHAEL WOODWORTH	"4804 HEMPSTEAD, LEXINGTON, KY. "		317-523-7229	1964733
T1	100 STONEHAVEN	91937554-2	Mannuel Walters	2	7/15/2013	8:00 AM	37.898889	-84.560466	2	1079154001	NICHOLASVILLE	"Appt Set 7/8 7/15 2AC,1WH 8am-12pm Best Contact 206-335-2656"	EKPC - Blue Grass	2	1	91937554	PATRICIA PAYNE	"100 STONEHAVEN, NICHOLASVILLE, KY."	206-335-2656		1976187
T1	113 BASS POND GLEN	92685053-2	Mannuel Walters	3	7/15/2013	8:00 AM	37.893064	-84.578725	2	1087817001	NICHOLASVILLE	7-9//7-15 8am-12//1AC 1WH//330-840-4559//JJB	EKPC - Blue Grass	1	1	92685053	TODD PAYNE	"113 BASS POND GLEN, NICHOLASVILLE, KY. "		330-840-4559	1966072
T1	1118 KEENE PK	91931857-2	Mannuel Walters	4	7/15/2013	12:00 PM	37.89488	-84.578275	2	1089744001	NICHOLASVILLE	Appt Set 7/9 7/15 12-3pm 1AC BEST CONTACT 432-466-4261 OK TO COMPLETE AC IF MEMBER NOT PRESENT POS	EKPC - Blue Grass	0	0	91931857	OLETA ALLEN	"1118 KEENE PK, NICHOLASVILLE, KY. "	432-466-4261		1966871
T1	105 ARBY DR	91940320-2	Mannuel Walters	5	7/15/2013	12:00 PM	37.88679	-84.570349	2	0899150003-64	NICHOLASVILLE	apt set for 7/15 12-3 1 ac 2 wh contact numbers 859-885-5562 or 859-533-2566 TG	EKPC - Blue Grass	0	0	91940320	FRANK RICHARDSON	"105 ARBY DR, NICHOLASVILLE, KY. "	859-885-5562	859-533-2566	1942953
T1	123 COLONIAL DR	33015100-2	Mannuel Walters	6	7/15/2013	12:00 PM	37.930409	-84.674014	2	1094133001	VERSAILLES	Appt Set 7/9 7/15 12-3pm Best contact 270-272-2682 POS	EKPC - Blue Grass	2	2	33015100	MICHELLE L BRENNER	"123 COLONIAL DR, VERSAILLES, KY."	270-272-2682		1983557
T1	1617 MAGNA OAK DR	91156409-2	Mannuel Walters	7	7/15/2013	3:00 PM	37.972686	-84.461706	2	1088304001	LEXINGTON	Appt set for 7/15 8-12--1AC--Best contact number 270-427-8732--CJB	EKPC - Blue Grass	1	0	91156409	LAUREN BURGESS	"1617 MAGNA OAK DR, LEXINGTON, KY. "	270-427-8732		1978376
T1	725 STONEBRIAR WAY	93007692-2	Mannuel Walters	8	7/15/2013	3:00 PM	37.882389	-84.366298	2	1090342001	RICHMOND	7-9//7-15 12pm-3//1AC//352-361-4670//JJB	EKPC - Blue Grass	1	0	93007692	AUDRA M EBEL	"725 STONEBRIAR WAY, RICHMOND, KY."	352-361-4670	859-624-6230	1967201
T1	3316 TARR RIDGE RD 	66492-2	Raymond Wright	1	7/15/2013	8:00 AM	37.95093	-83.625789	2	1157310	FRENCHBURG	apt set for 7/15 8-12 1 ac no ok to install if customer not home best contact 606-768-3896 TG	EKPC - Clark	0	0	66492	ROBERT KLENK	"3316 TARR RIDGE RD , FRENCHBURG, KY."	606-768-3896		1997873
T1	139 HIGHWAY 1274	65335-2	Raymond Wright	2	7/15/2013	8:00 AM	37.986221	-83.527705	2	1052510	FRENCHBURG	1 ac-0 WH 606-768-2410 APPT SET 6/12 ag	EKPC - Clark	1	0	65335	RONALD MULLINS	"139 HIGHWAY 1274, FRENCHBURG, KY. "	606-768-2410		1996737
T1	2214 HIGHWAY 589	31369-2	Raymond Wright	3	7/15/2013	8:00 AM	37.939325	-83.080993	2	1104501001-56	WEST LIBERTY	6-26//1AC 1WH//hasnt heard of program//JJB	EKPC - LickingValley	1	1	31369	DENZIL CANTRELL	"2214 HIGHWAY 589, WEST LIBERTY, KY. "	606-522-4662		2126761
T1	4810 UPPER GILMORE RD	26790-2	Raymond Wright	4	7/15/2013	12:00 PM	37.721051	-83.365972	2	2102801001-56	CAMPTON	1 a/c 1 w/h- different trailer on same lot 606-662-4448	EKPC - LickingValley	0	0	26790	TIMOTHY FERGUSON	"4810 UPPER GILMORE RD, CAMPTON, KY. "	606-662-4448		2120598
T1	4810 UPPER GILMORE RD	30275-2	Raymond Wright	5	7/15/2013	12:00 PM	37.721051	-83.365972	2	2102802001-56	CAMPTON	1 w/h 1 a/c- 606-662-4448 	EKPC - LickingValley	1	1	30275	TIMOTHY FERGUSON	"4810 UPPER GILMORE RD, CAMPTON, KY. "	606-662-4448		2121060
T1	146 CONLEY BR	12113-2	Raymond Wright	6	7/15/2013	3:00 PM	37.957948	-83.164047	2	1103204200-56	WEST LIBERTY	"1AC,1WH BEST CONTACT 606-522-4253 POS"	EKPC - LickingValley	0	0	12113	JONATHON PERRY	"146 CONLEY BR, WEST LIBERTY, KY. 41472"	606-522-4253		2126537
T1	221 BROOKSIDE DR	70876-2	Raymond Wright	7	7/15/2013	3:00 PM	37.846787	-83.863314	2	2406918	STANTON	THE NEW CUSTOMER'S NAME IS FAREED SAGHIR. THIS 7/15 3-6--1AC--606-909-5949--773-573-4780--CJB	EKPC - Clark	1	1	70876	PREMCHAND SHANMVGAM	"221 BROOKSIDE DR, STANTON, KY. "	214-412-7026		2016953
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
																					
