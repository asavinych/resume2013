﻿namespace SSS.AI

open System

(*Прототип генетического алгоритма создавался со следующими условиями:
    1) возможно будет изменять тип хромосом;
    2) создавать свои операции скрещивания;
    3) создавать свои операции мутации;
    4) создавать свои операции для модификации всего списка особей

*)
[<Class>]
type public GA<'Individual>=class
    
    val private _crossover:list<'Individual*'Individual ->'Individual>
    
    val private _mutation:list<'Individual->'Individual>
    
    //функция вычисляет живучесть каждой особи (для сортировки)
    val private _viabilityIndividual:'Individual -> float

    //метод отбирает особи из отсортированного списка особей по функции _viabilityIndividual
    val private _gettingIndividual:list<'Individual> -> list<'Individual>

    val private _userOperation:list<list<'Individual>->list<'Individual>>

    public new(crossovers:list<'Individual*'Individual ->'Individual>, 
               mutation:list<'Individual->'Individual>,
               viabilityIndividual:'Individual -> float,
               gettingIndividual:list<'Individual> -> list<'Individual>,
               userOperation:list<list<'Individual>->list<'Individual>>)=
        {
            _crossover=crossovers
            _mutation=mutation
            _viabilityIndividual=viabilityIndividual
            _gettingIndividual=gettingIndividual
            _userOperation=userOperation
        }

    member public this.Start(firstGeneration:list<'Individual>, countGeneration:int)=
        this.Start(firstGeneration, countGeneration, "amcr")
    
    member public this.Start(firstGeneration:list<'Individual>)=
        this.Start(firstGeneration, 3, "amcr")

    member public this.Start(firstGeneration:list<'Individual>, countGeneration:int, pattern:string)=
        let allIndividualsInEnvirement=ref firstGeneration

        let rec loop(indexGeneration:int)=
            
            if (indexGeneration>0) then
                let newList=ref ((!allIndividualsInEnvirement))

                //эта локальная функция выполняет операции над всем особями в "среде"
                let userOperation=(newList:=(!newList)@(List.fold (fun accomulator operation-> accomulator@operation(!newList)) [] this._userOperation))

                //эта локальная функция собирает список из списков особей над которыми проведены все пользовательские мутации
                let mutation=(newList:=(!newList)@(List.fold (fun accomulator mutatio-> accomulator@((!newList)|>List.map mutatio))
                                                                                        [] this._mutation))
                
                let crossing=(
                    let rec loop2(accomulator:list<'Individual>,
                                    currentIndividuals:list<'Individual>,
                                        crossOperation:'Individual*'Individual -> 'Individual)=
                        match(currentIndividuals) with
                            | x::y::tail -> loop2(crossOperation(x,y)::accomulator, tail, crossOperation)
                            | tail -> accomulator@tail

                    newList:=(!newList)@(List.fold (fun accomulator crossOperation -> accomulator@loop2([], (!newList), crossOperation))
                                                   [] this._crossover)
                    )
                
                let rate= (newList:= (!newList) |> List.sortBy this._viabilityIndividual)

                let tournament = (newList:=this._gettingIndividual((!newList)))
                
                let rec loopStr(pattern:string)=
                    if (pattern.Length>0) then
                        match (pattern.Chars(0)) with
                        | 'a' -> userOperation
                        | 'm' -> mutation
                        | 'c' -> crossing
                        | 'r' -> rate
                        | 't' -> tournament
                        | _ -> ()
                        loopStr(pattern.Remove(0,1))
                loopStr(pattern)
                allIndividualsInEnvirement:=(!newList)
                loop(indexGeneration-1)
            else
              ()

        loop(countGeneration)
        (!allIndividualsInEnvirement)
end

