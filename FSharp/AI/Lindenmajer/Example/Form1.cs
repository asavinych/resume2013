﻿using SSS.AI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            /*//Fractals(StepByStep(3, "F+F+F+F", "F-F+F+FFF-F-F+F", ""), 2, 0, 90, 500, 400, ref mm1);
             * Fractals(StepByStep(4, "F+F+F+F", "F-b+F+FFF-F-b+F", "bbb"), 1, 0, 90,300, 400, ref mm1);
             * Fractals(StepByStep(3, "F-F-F-F", "F+b-FF+F+FF+Fb+FF-b+FF-F-FF-Fb-FFF", "bbbbbb"), 1, 0, 90, 300, 150, ref mm1);
             * Fractals(StepByStep(5, "F", "F[-F]F[+F]F", ""), 2, 90, 25.71F, 300, 500, ref mm1);
             * Fractals(StepByStep(4, "F", "+F-F-[-F+F+]+[+F-F-F]", ""), 10, 90, 22.5F, 300, 500, ref mm1);
             * Fractals(StepByStep(3, "F[+F+F][-F-F][++F][--F]F", "FF[--F][-F][F][+F][++F]", ""), 15, 90, 11.25F, 300, 500, ref mm1);
             *Fractals(StepByStep(3, "[F]+[F]+[F]+[F]+[F]+[F]", "F[--F][+FF]FF[-F][+F]FF", ""), 2, 0, 60, 300, 300, ref mm1); 
            */
            Graphics mm1 = CreateGraphics();
            mm1.Clear(Color.Blue);
            formalSystemLindenmayer linden = new formalSystemLindenmayer("[F]+[F]+[F]+[F]+[F]+[F]");
            Dictionary<char, Func<IEnumerable<char>>> dictionary = new Dictionary<char, Func<IEnumerable<char>>>();
            dictionary.Add('+', () => new char[] { '+' });
            dictionary.Add('-', () => new char[] { '-' });
            dictionary.Add('[', () => new char[] { '[' });
            dictionary.Add(']', () => new char[] { ']' });
            dictionary.Add('F', () => "F[++F][-FF]FF[+F][-F]FF".ToArray());
            dictionary.Add('b', () => new char[] { 'b' }); 
            var a = linden.create(3, dictionary);
            linden.drawLines(a, 2, 0, 60, 300, 300, (x0, y0, x1, y1) =>
            {
                Pen P = new Pen(Brushes.Yellow);
                mm1.DrawLine(P, (float)x0, (float)y0, (float)x1, (float)y1);
            });
        }
    }
}
