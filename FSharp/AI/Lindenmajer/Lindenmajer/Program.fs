﻿namespace SSS.AI

open System
open System.Collections.Generic;


[<Class>]
type public formalSystemLindenmayer=class
    
    val private _axiom:seq<char>

    public new(axiom:string)=
        {
            _axiom=axiom
        }

    member public this.create(steps:int, funcs:Dictionary<char, Func<seq<char>>>)=
        let word=this._axiom
        let next:seq<char> ref=ref word

        let rec loop(l:int)=
            if (l>0) then
                next:= (!next) |> Seq.map 
                                     (fun x -> if (funcs.ContainsKey(x)) then funcs.Item(x).Invoke()
                                               else seq([char(0)]) )|> Seq.concat
                loop(l-1)
            else
                ()

        loop(steps)
        !next

    member public this.drawLines(word:seq<char>, k:double, a:double, q:double, x0:double, y0:double,  
                                    drawLine:Action<double, double, double, double>)=

        let st:Stack<double> ref =ref (new Stack<double>());
        let _a=ref ((a * Math.PI) / (double(180)));
        let x:double ref =ref (double(x0));
        let y:double ref= ref (double(y0));
        let _q=ref ((float)(q*Math.PI) /(double(180)));
        word |> Seq.iter ( fun ch -> 
                                match (ch) with
                                | 'F' -> drawLine.Invoke(!x,!y,!x - k * Math.Cos((!_a)),  !y - k * Math.Sin((!_a)))
                                         x:=!x - k * Math.Cos((!_a))
                                         y:=!y - k * Math.Sin((!_a))
                                | '+' -> _a:=!_a+(!_q)
                                | '-' -> _a:=!_a-(!_q)
                                | '[' -> (!st).Push(!x)
                                         (!st).Push(!y)
                                         (!st).Push(!_a)
                                | ']' -> _a:=(!st).Pop()
                                         y:=(!st).Pop()
                                         x:=(!st).Pop()
                                | 'b' -> x:=!x - k * Math.Cos((!_a));
                                         y:=!y - k * Math.Sin((!_a));
                                | _ -> ()
                                     )
    
end