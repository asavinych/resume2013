﻿open System
open System.IO
open System.Collections.Generic


let acvion (path:string)=
    ()

let action1 (paTh:string)=
    ()

let mutable arrayExamphe=[|[|0; 1; 1; 1; 0|]
                           [|0; 0; 1; 0; 0|]
                           [|0; 0; 0; 1; 1|]
                !          [|0; 0; 0; 0; 0|]
 `                        `[|0; 0; 0; 0; 1|]
                        |]

let rec searchDepth<'T> (elements:IEnumerable<'T>, actionGetNextElements:'T -> IEnumerable<'t>, actionPredInPath:'T -> unit)=
     let elementsUse=Seq.toList(elements)
     elementsUse |> List.iter (fun element -> 
                                          //выполняем действия перед погружением
                                          actionPredInPath(element) 
                                          let nextElements=actionGetNextElements(element)
                                          if (Seq.length(nextElements)>0) then 
                                            //погружение
                                            searchDepth(nextElements, actionGetNextElements, actionPredInPath) 
                                          else 
                                            ()
                                          ) 
let nextElements (x:int, y:int) =
       let line=Array.get arrayExample (x)
       let res=[ for i in [y.. line.Length-1] do
                     if ((Array.get (Array.get arrayExample (x)) i)>0) then
                        yield (x, i) ] 
       (res:>IEnumerable<(int*int)>)

let nextElements1 (_:int, y:int) =
       let line=Array.get arrayExample (y)
      
       let res=[ for i=y to line.Length-1 do
                     if ((Array.get (Array.get arrayExample (y)) i)=1) then
                        yield (y, i) ]
       (res:>IEnumerable<(int*int)>)
 
let mutable dt=DateTime.Now
Console.WriteLine("{0}  {1}", dt.Minute, dt.Second) 
searchDepth(nextElements(0, 0), nextElements1, fun(i, j) -> Array.set (Array.get arrayExample i) j 2) 
dt<-DateTime.Now
Console.WriteLine("{0}  {1}", dt.Minute, dt.Second)

for i in [0..4] do
    for j in [0..4] do
        Console.Write(Array.get (Array.get arrayExample i) j)
    Console.WriteLine()


let nextElements2 (str:string)=
    Directory.GetDirectories(str):>IEnumerable<string>

searchDepth([ @"C:\temp" ], nextElements2, fun path -> Console.WriteLine(path)) 
ignore(Console.ReadLine())