﻿namespace SSS.DataParsing

open System
open System.IO
open System.Text
open System.Data
open System.Data.SqlClient

[<Class>]
type public ParsingPostFile=class
    
    val private _pathIn:string
    val private _pathOut:string
    val private _conStr:string

    public new (pathIn:string, pathOut:string, conStr:string)=
        {
        _pathIn=pathIn
        _pathOut=pathOut
        _conStr=conStr 
        }


    member private this.load(_Barcode:string)=
          let strCon1=new SqlConnection(this._conStr)
      
          let loop(strCom, strCon:SqlConnection)=
            strCon.Open()
            let mutable sqlCom=new SqlCommand(strCom, strCon)
            sqlCom.CommandTimeout<-Int32.MaxValue
            let aR=sqlCom.ExecuteReader()
            let mutable res=""
            if (aR.Read()) then 
                res<-aR.Item(0).ToString()
            else
                ()
            strCon.Close()
            res

          loop(String.Format("SELECT b.N18 FROM fn1774
                join fn212 b on fn1774.n1_1 = b.n1
                where fn1774.Barcode='{0}'
                ORDER BY fn1774.S692 DESC", _Barcode),
                            strCon1)

    member private this.GetFilesList(str:string, strOut:string)=
        let files=Array.toList(Directory.GetFiles(str))

        let txtFiles=List.sort (List.filter (fun (x) -> Path.GetExtension(x).IndexOf("txt")>=0) files)

        let iniFiles=List.sort( List.filter (fun (x) -> Path.GetExtension(x).IndexOf("ini")>=0) files)

        let dictionaryFiles=List.zip txtFiles iniFiles

        List.iter (fun (x:string, y:string) -> 
                        let paymentSum=ref 0
                        let MassSum=ref 0
                        let MassRateSum=ref 0
                        let InsrRateSum=ref 0
                        let AirRateSum=ref 0
                        let ValueSum=ref 0
                        let sr=new StreamReader(x, Encoding.GetEncoding("cp866"))
                        let sr1=new StreamReader(y, Encoding.GetEncoding("cp866"))

                        let DataMail=Array.toList(sr.ReadToEnd().Split([|'\n'; '\r'|], StringSplitOptions.RemoveEmptyEntries))

                        let DataIniFile= Array.toList(sr1.ReadToEnd().Split([|'\n'; '\r'|], StringSplitOptions.RemoveEmptyEntries))
                    
                        let mutable ListNum=(List.filter( fun (z:string) -> z.IndexOf("ListNum")>=0) DataIniFile).Head.Replace("ListNum=", "")

                        while (ListNum.Length>=3) do ListNum<-ListNum.Remove(0,1)

                        let forDataIni=["Inn"; "SendCtg"; "SendDate"; "ListNum"; "MailType"; "[Main]"; "MailCtg"; "PostMark"; "MailRank"; "[Summary]"; "MailCount"; "ValueSum="]
                    
                        let mutable newDataIniFile=List.filter( fun (x1:string) -> forDataIni |> List.exists (fun (lise) -> x1.IndexOf(lise)>=0)) DataIniFile 

                        let mutable sendDate=(List.find(fun (x:string) -> x.IndexOf("SendDate=")=0) newDataIniFile).Remove(0, "SendDate=201".Length)

                        let mutable newNameTxt=sendDate+ListNum+".txt"

                        let mutable newNameIni=sendDate+ListNum+"h.ini"

                        if ((File.Exists(strOut+newNameTxt))) then
                            let dir=DateTime.Now.Minute.ToString("D2")+DateTime.Now.Hour.ToString("D2")+DateTime.Now.Second.ToString("D2")
                            ignore(Directory.CreateDirectory(strOut+dir))
                            newNameTxt<-dir+"\\"+newNameTxt
                            newNameIni<-dir+"\\"+newNameIni
                    
                        let sw1=new StreamWriter(strOut+newNameTxt, false, Encoding.GetEncoding("cp866"))
                        let sw2=new StreamWriter(strOut+newNameIni, false, Encoding.GetEncoding("cp866"))

                    
                        
                        
                        let mutable FirstLine=false
                        sw1.WriteLine("Barcode|Mass|MassRate|Payment|Value|InsrRate|AirRate|IndexTo|Region|Area|City|Street|Addressee|Comment|MailDirect|AlterDeliv|TelAddress")
                        DataMail.Tail |> List.iter
                            ( fun (x) -> 
                                let a=Array.toList(x.Split([|'|'|], StringSplitOptions.None))
                                match(Array.toList(x.Split([|'|'|], StringSplitOptions.None))) with
                                    | _Barcode::_Mass::_MassRate::_Payment::_Value
                                        ::_InsrRate::_AirRate::_IndexTo::_Region::_Area
                                        ::_City::_Street::_Comment::_MailDirect::_TelAddress::_ ->
                                                                                                let conv (_str:string) = Convert.ToInt32(_str)
                                                                                                MassSum:=(!MassSum)+conv(_Mass)
                                                                                                MassRateSum:=(!MassRateSum)+conv(_MassRate)
                                                                                                paymentSum:=(!paymentSum)+conv(_Payment)
                                                                                                ValueSum:=(!ValueSum)+conv(_Value)
                                                                                                InsrRateSum:=(!InsrRateSum)+ conv(_InsrRate)
                                                                                                AirRateSum:=(!AirRateSum)+ conv(_AirRate)
                                                                                                sw1.WriteLine("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}",
                                                                                                                _Barcode,
                                                                                                                (_Mass),
                                                                                                                (_MassRate),
                                                                                                                (_Payment),
                                                                                                                (_Value),
                                                                                                                (_InsrRate),
                                                                                                                (_AirRate),
                                                                                                                (if (["163061"; "163031"; "163054"] |>List.exists (fun x -> x=_IndexTo)) then "163000" else _IndexTo),
                                                                                                                _Region, _Area, _City, _Street, this.load(_Barcode),
                                                                                                                _Comment, _MailDirect, 0, _TelAddress)
                                                                                                sw1.Flush()                                          
                                    |    _ -> ()
                            )
                        newDataIniFile |> List.iter (fun (x) ->  sw2.WriteLine(x))
                        sw2.Flush()
                        sw2.WriteLine("PaymentSum="+(!paymentSum).ToString())
                        sw2.WriteLine("MassSum="+(!MassSum).ToString())
                        sw2.WriteLine("MassRateSum="+(!MassRateSum).ToString())
                        sw2.WriteLine("InsrRateSum="+(!InsrRateSum).ToString())
                        sw2.WriteLine("AirRateSum="+(!AirRateSum).ToString())
                        sw2.Flush()
                        sw1.Close()
                        sw2.Close()
                    ) dictionaryFiles 
        ()
    
    member public this.Start()=
        let thr=new Threading.Thread(new Threading.ThreadStart(fun () -> this.GetFilesList(this._pathIn, this._pathOut)
                                                                         ignore(Windows.Forms.MessageBox.Show("Обработка завершена"))))
        thr.Start()

end