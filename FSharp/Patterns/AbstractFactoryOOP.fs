﻿open System

[<Interface>]
type AbstractProductA=interface
    abstract member ShowMessage: unit -> unit
end

[<Interface>]
type AbstractProductB=interface
    abstract member ShowMessage: unit -> unit
end


[<AbstractClass>]
type AbstractFactory=class
    new()={}
    abstract member CreateProductA:AbstractProductA
    abstract member CreateProductB:AbstractProductB    
end

[<Class>]
type ProductA1=class
    new() ={}
    interface AbstractProductA with
        member this.ShowMessage()=
            Console.WriteLine "Product A1"
end

[<Class>]
type ProductB1=class
    new() ={}
    interface AbstractProductB with
        member this.ShowMessage()=
            Console.WriteLine "Product B1"
end

[<Class>]
type ConcreteFactory1=class
    new()={}
    
    inherit AbstractFactory

    override this.CreateProductA:AbstractProductA=
        (new ProductA1()):>AbstractProductA
    override this.CreateProductB:AbstractProductB=
        (new ProductB1()):>AbstractProductB
end

[<Class>]
type ProductA2=class
    new()={}

    interface AbstractProductA with
        member this.ShowMessage()=
            Console.WriteLine "Product A2"

end

[<Class>]
type ProductB2=class
    new()={}
    interface AbstractProductB with
        member this.ShowMessage()=
            Console.WriteLine "Product B2"
end

[<Class>]
type ConcreteFactory2=class
    new()={}
    inherit AbstractFactory 
    override this.CreateProductA:AbstractProductA =
        (new ProductA2()):>AbstractProductA
    override this.CreateProductB:AbstractProductB=
        (new ProductB2()):>AbstractProductB
end

[<EntryPoint>]
let main argv = 
    
    let cf1=new ConcreteFactory1();
    cf1.CreateProductA.ShowMessage();
    cf1.CreateProductB.ShowMessage();

    let cf2=new ConcreteFactory2();
    cf2.CreateProductA.ShowMessage();
    cf2.CreateProductB.ShowMessage();

    printfn "%A" argv
    0 // return an integer exit code
