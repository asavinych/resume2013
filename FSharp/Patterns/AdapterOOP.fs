﻿open System

[<AbstractClass>]
type Target=class
    new() = {}
    abstract member Request:unit -> unit
    default this.Request()=
       Console.WriteLine "Default method"
end

[<Class>]
type Adaptee=class
    new() = {}
    member this.SpecificRequest()=
        Console.WriteLine "Specific Request"
end

[<Class>]
type Adapter(adaptee:Adaptee)=class
     
    inherit Target()
    
    override this.Request()=
            adaptee.SpecificRequest()
end

[<EntryPoint>]
let main argv = 
    let adapter=new Adapter(new Adaptee())
    adapter.Request()
    printfn "%A" argv
    0 // return an integer exit code
