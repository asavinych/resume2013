﻿open System

type Implementor=interface
    abstract member Operation:unit -> unit
end

type Abstraction(__implementor:Implementor)=class
    
    let mutable _implementor=__implementor

    member this.implementor with get() = _implementor and set impl = _implementor<-impl

    abstract member Operation:unit -> unit
    default this.Operation()=
        this.implementor.Operation();
end

type ConcreteImpl=class
    new ()={}
    interface Implementor with
        override this.Operation()=
            Console.WriteLine "Concrete Implementer A"
end

type ConcreteImpl2=class
    new ()={}
    interface Implementor with
        override this.Operation()=
            Console.WriteLine "Concrete Implementer B"
end


[<EntryPoint>]
let main argv = 
    let ab=new Abstraction((new ConcreteImpl()):>Implementor)
    ab.Operation();
    ab.implementor<-(new ConcreteImpl2()):>Implementor
    ab.Operation();
    ignore(Console.ReadLine())
    printfn "%A" argv
    0 // return an integer exit code
