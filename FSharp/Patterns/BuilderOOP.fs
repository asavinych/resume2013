﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.
namespace Example

open System
open System.Windows.Forms

[<Measure>] type kg

[<Class>]
type Product=class
    
    val mutable parts:list<string>
    new() = { parts=[]}
    member this.getK(i:float<kg>):float<kg> =
        10.0<kg>

    member this.gteFunc(functionMy:int->unit):unit=
        functionMy 12
end

[<Interface>]
type IBuilder=interface
    abstract member BuildPartA  : unit -> unit
    abstract member BuildPartB  : unit -> unit
    abstract member GetResult   : Product with get
    abstract member ShowResult  : unit -> unit
end

[<Class>]
type ConcreteBuilder1(product:Product)=class
    interface IBuilder with

        member x.BuildPartA()=
            product.parts<-"PartA"::product.parts
        member x.BuildPartB()=
            product.parts<-"PartB"::product.parts
        member x.GetResult=
            product
        member x.ShowResult()=
           let res = List.fold (fun x state -> x+" "+state) ""  product.parts
           Console.WriteLine(res);
end

[<Class>]
type ConcreteBuilder2(product:Product)=class
    interface IBuilder with

        member this.BuildPartA()=
            product.parts<-"PartX"::product.parts
        member this.BuildPartB()=
            product.parts<-"PartY"::product.parts
        member this.GetResult =
            product
        member this.ShowResult()=
           let res = List.fold (fun x state -> x+" "+state) ""  product.parts
           ignore(MessageBox.Show(res))
end

[<Class>]
type Director=class
    new() = {}
    member x.Construct(builder: IBuilder):unit=
        builder.BuildPartA()
        builder.BuildPartB() 
end



//[<EntryPoint>]
//let main argv = 
//    let director:Director=new Director()
//    let b1:IBuilder=(new ConcreteBuilder1(new Product())):>IBuilder
//    let b2:IBuilder=(new ConcreteBuilder2(new Product())):>IBuilder
//    director.Construct(b1)
//    director.Construct(b2)
//    
//    let p1=b1.ShowResult
//    let p2=b2.ShowResult
//
//    p1()
//
//    p2()
//    
//    0 // return an integer exit code
