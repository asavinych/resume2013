﻿open System

[<AbstractClass>]
type Component(__name:string)=class
    let mutable name=__name

    abstract member Display: unit -> unit
end

type Composite(__name:string)=class
   inherit Component(__name)
   
   let mutable children:list<Component>=[]   

   override this.Display()=
        children |> List.iter (fun x -> x.Display())

   member this.Add(com:Component):unit=
        children<-com::children
   member this.Remove(com:Component):unit=
        children <- children |> List.filter (fun x -> x<>com)

    
end

type Leaf(__name:string)=class
    inherit Component(__name)
    override this.Display():unit=
        Console.WriteLine ("Leaf"+__name)
end

[<EntryPoint>]
let main argv = 
    let com=new Composite("Ss")
    com.Add(new Leaf("second"))
    com.Add(new Leaf("first"))
    com.Display()
    ignore(Console.ReadLine())
    printfn "%A" argv
    0 // return an integer exit code
