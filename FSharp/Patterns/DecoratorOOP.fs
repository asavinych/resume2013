﻿open System

[<AbstractClass>]
type Component=class
    new() ={}
    abstract member Operation:unit -> unit
end

[<Class>]
type Decorator(__component:Component)=class
    inherit Component()
    let mutable componentMy:Component=__component

    override this.Operation():unit=
       componentMy.Operation()
end

[<Class>]
type DecoratorB(__component:Component)=class
    inherit Decorator(__component)

    override this.Operation()=
        base.Operation()
        this.NewOperation()

    member this.NewOperation()=
        Console.WriteLine "Yes"
end

[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    0 // return an integer exit code
