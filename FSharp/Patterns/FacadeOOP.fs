﻿open System

type SystemA=class
    new()= {}
    member this.Operation1()=
        Console.WriteLine "Operation 1 System A"
end

type SystemB=class
    new()={}
    member this.Operation2()=
        Console.WriteLine "Operation 2 System B"
end

type SystemC=class
    new() = {}
    member this.Operation3()=
        Console.WriteLine "Operation 3 System C"
end

[<Class>]
type Facade()=class
    
    static let aS:SystemA=new SystemA()
    static let bS:SystemB=new SystemB()
    static let cS:SystemC=new SystemC()
    
    static member OperationForAll()=
        aS.Operation1()
        bS.Operation2()
        cS.Operation3()
end

[<EntryPoint>]
let main argv = 
    Facade.OperationForAll()
    printfn "%A" argv
    0 // return an integer exit code
