﻿open System

[<AbstractClass>]
type Product=class
    new() ={}
    abstract member GetAction:unit -> unit
    default this.GetAction() =  Console.WriteLine "Default Action";
end

[<Class>]
type ProductA=class
    new() ={}
    inherit Product
    override this.GetAction()=
        Console.WriteLine "Action Product A"
end

[<Class>]
type ProductB=class
    new() ={}
    inherit Product
    override this.GetAction()=
        Console.WriteLine "Action Product B"
end

[<AbstractClass>]
type Creator=class
    new()={}
    abstract member GetProduct:unit -> Product
    default this.GetProduct():Product=
        (new ProductA()):>Product
end

[<Class>]
type CreatorA=class
    new()={}
    inherit Creator
    override this.GetProduct()=
        (new ProductA()):>Product
end

[<Class>]
type CreatorB=class
    new()={}
    inherit Creator
    override this.GetProduct()=
        (new ProductB()):>Product
end

[<EntryPoint>]
let main argv = 
    [new CreatorA():>Creator; new CreatorB():>Creator] |> Seq.iter (fun (x) -> x.GetProduct().GetAction())
    ignore(Console.ReadLine())
    0