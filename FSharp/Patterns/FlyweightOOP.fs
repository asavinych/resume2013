﻿open System

[<AbstractClass>]
type Flyweight(_first:int, _second:int)=class
    let mutable first:int=_first
    let mutable second:int = _second

    abstract member Display:unit -> unit
end

type Flyweight1(_first:int, _second:int)=class
    inherit Flyweight(_first, _second)

    override this.Display()=
        Console.WriteLine "1"
end

type Flyweight2(_first:int, _second:int)=class
    inherit Flyweight(_first, _second)

    override this.Display()=
        Console.WriteLine "2"
end
[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    0 // return an integer exit code
