﻿open System

type ContextMy()=class
end

[<AbstractClass>]
type AbstractExpression()=class
    abstract Interpret: ContextMy -> unit
end

type TerminalExpression()=class
    inherit AbstractExpression()

    override this.Interpret(context:ContextMy)=
        Console.WriteLine "Hi"

end

type NonterminalExpression()=class
    inherit AbstractExpression()

    override this.Interpret(context:ContextMy)=
        Console.WriteLine "Hi"

end

[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    0 // return an integer exit code
