﻿open System

[<AbstractClass>]
type Iterator()=class
    abstract First: unit -> Object
    abstract Next:unit -> Object
    abstract IsDone: unit -> bool
    abstract CurrentItem: unit -> Object
end

[<AbstractClass>]
type Aggregate()=class
    abstract CreateIterator: unit -> Iterator 
    abstract Count: unit -> int
    abstract get: int -> Object
end

type ConcreteIterator(_aggregator:Aggregate)=class
    inherit Iterator()
    let aggregate=_aggregator
    let mutable current=0
    

    override this.First()=
        aggregate.get(0)

    override this.Next()=
        aggregate.get(current+1)
    
    override this.CurrentItem()=
        aggregate.get(current)

    override this.IsDone()=
        current>=aggregate.Count()
end

[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    0 // return an integer exit code
