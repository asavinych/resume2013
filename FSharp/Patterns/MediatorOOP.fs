﻿open System

[<AbstractClass>]
type Mediator()=class
    abstract Send: String -> Object -> unit
end

type Colleague(_mediator:Mediator)=class
    let mediator:Mediator=_mediator

end

type ConcreteColleague(_mediator:Mediator)=class
    inherit Colleague(_mediator)

    member this.Send()=
        _mediator.Send "Hi" this

end

[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    0 // return an integer exit code
