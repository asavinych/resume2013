﻿open System

[<AbstractClass>]
type Prototype(_id:string)=class
    
    let mutable id=_id

    member this.Id with get()= id and set value = id <-value
    abstract member Clone:unit -> Prototype
    abstract member Show:unit -> unit
    default this.Show()=
        Console.WriteLine ("This is "+this.Id)
end

[<Class>]
type ConcretePrototypeA(_id:string)=class
    inherit Prototype(_id)
    override this.Clone():Prototype=
        this :> Prototype    
end

[<Class>]
type ConcretePrototypeB(_id:string)=class
    inherit Prototype(_id)
    override this.Clone():Prototype=
        this :> Prototype    
end


[<EntryPoint>]
let main argv = 
    let a=new ConcretePrototypeA("A")
    let b=new ConcretePrototypeB("B")

    let c=[a.Clone(); b.Clone()]
    c |> Seq.iter (fun x -> x.Show())
    ignore(Console.ReadLine())
    printfn "%A" argv
    0 // return an integer exit code
