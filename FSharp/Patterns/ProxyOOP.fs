﻿open System


type IMath=interface
    abstract member AddMy: (int* int) -> int
    abstract member SubMy: (int*int) ->int
    abstract member Mul: (int*int) -> int
    abstract member Div:(int*int) -> int    
end

type Math()=class
    interface IMath with
        override this.AddMy(x) =
            let (f,s)=x 
            f+s
        override this.SubMy(x) =
            let (f,s)=x 
            f-s
        override this.Mul(x) =
            let (f,s)=x 
            f*s
        override this.Div(x) =
            let (f,s)=x 
            f/s

end

type MathProxy()=class
    let math:Math=new Math()

    interface IMath with
        override this.AddMy(x) =
            (math:>IMath).AddMy(x)

        override this.SubMy(x) =
            let (f,s)=x 
            f-s
        override this.Mul(x) =
            let (f,s)=x 
            f*s
        override this.Div(x) =
            let (f,s)=x 
            f/s

end


[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    0 // return an integer exit code
