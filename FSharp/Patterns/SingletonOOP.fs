﻿type A private () =class
    static let instance = A()
    static member Instance = instance
    member this.Action() = printfn "action"
end
    
[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    0 // return an integer exit code
