﻿open System;
open System.IO;
open System.Data;
open System.Data.SqlClient;
open Word


let printOperDay=
    let a=Console.ReadLine()
    let countDay:int= Int32.Parse(Console.ReadLine())%30
    
    let strCon=new SqlConnection("Data Source=I2901-APP003;Initial Catalog=Taxes01;Integrated Security=True")
    strCon.Open()
    
    let mutable strCommand=String.Format("SELECT DISTINCT TOP 30 
                                          a.Id, a.name, 
                                          Convert(datetime,
                                            (case when a.lastrundate > 0 then  substring(convert(varchar(10),a.lastrundate),7,2) + '.' + substring(convert(varchar(10),a.lastrundate),5,2) + '.'+substring(convert(varchar(10),a.lastrundate),1,4) else NULL end)) as dd,
                                            (case when a.lastruntime > 0 then substring(convert(varchar(10),(1000000+a.lastruntime)),2,2)+':'+substring(convert(varchar(10),(1000000+a.lastruntime)),4,2)+':'+substring(convert(varchar(10),(1000000+a.lastruntime)),6,2) else NULL end)
                                          FROM tpe_TP_Info a
                                          LEFT JOIN tpe_RuningTask c ON convert(varchar(15),a.id) = c.TaskId 
                                          WHERE ( a.TP_Id <= 32599) AND ((case when c.Status like '%sleep%' then 10 when 
                                          (a.TP_Error_Id=1 or (a.TP_Error_Id=2 and c.TaskId  is Null)) then 12 when 
                                          (c.TaskId is Null and a.enabled=0) then 7 when Not (c.TaskId is Null) then 14  else 10 end NOT IN (14,7))
                                          AND (a.name LIKE '%Операционный день%') AND (a.N273 LIKE '{0}%'))
                                          ORDER BY dd DESC,a.Id ", a)

    let mutable sqlCom=new SqlCommand(strCommand, strCon)

    sqlCom.CommandTimeout<-Int32.MaxValue
    let aR=sqlCom.ExecuteReader()

    let mutable rows:list<string*string*string*string>=[]

    while (aR.Read()) do
        rows <-rows @ [(aR.Item(0).ToString(), aR.Item(1).ToString(), aR.Item(2).ToString().ToString(), aR.Item(3).ToString().ToString())]
    
    strCon.Close()
    let rec loop(i:int, data:list<string*string*string*string>)=
        if (i<countDay) then
            let (id, name, dd, tt)=data.Head
            let dateOD=name.Split([|"Операционный день ОБУ"|], StringSplitOptions.RemoveEmptyEntries)
            let mutable strCommand2=String.Format("SELECT 
                                                    x.Description
                                                    FROM ErrorLog x WHERE ( x.ProcessId = {0}) AND ((x.Source LIKE 'Создание%'))  ORDER BY x.id ", id)
            let mutable sqlCom2=new SqlCommand(strCommand2, strCon)
            sqlCom2.CommandTimeout<-Int32.MaxValue
           
          
            
            //let mutable counter=1
            strCon.Open()
            let aR2=sqlCom2.ExecuteReader()
            let mutable newRows:list<string>=[]
            while (aR2.Read()) do
               newRows<-newRows@[aR2.Item(0).ToString()]
            
                             
            strCon.Close()
            
            let mutable wdApp:Word.ApplicationClass=new Word.ApplicationClass()
            wdApp.Visible<-false
            let mutable deflt=Type.Missing
            
            let doc=wdApp.Documents.Add(ref deflt,ref deflt ,ref deflt ,ref deflt)

            wdApp.Application.Selection.PageSetup.Orientation<-Word.WdOrientation.wdOrientPortrait

            let start:obj=0:>obj

            let rng:Word.Range=wdApp.ActiveDocument.Range(ref start, ref start)

            rng.Font.Name<-"Times New Roman"
            rng.Font.Size<-13.0f
            rng.Font.Bold<-1
            rng.Font.ColorIndex<-Word.WdColorIndex.wdBlack
            rng.InsertParagraphAfter()
            rng.SetRange(rng.End, rng.End)
            wdApp.ActiveDocument.Paragraphs.Item(1).Range.Font.Name<-"Times New Roman"
            wdApp.ActiveDocument.Paragraphs.Item(1).Range.Font.Size<-13.0f
            wdApp.ActiveDocument.Paragraphs.Item(1).Alignment<-Word.WdParagraphAlignment.wdAlignParagraphCenter
            wdApp.ActiveDocument.Paragraphs.Item(1).Range.InsertBefore("Создание налоговых обязательств")
            rng.InsertParagraphAfter()
            wdApp.ActiveDocument.Paragraphs.Item(2).Alignment<-Word.WdParagraphAlignment.wdAlignParagraphCenter
            wdApp.ActiveDocument.Paragraphs.Item(2).Range.Font.Name<-"Times New Roman"
            wdApp.ActiveDocument.Paragraphs.Item(2).Range.Font.Size<-13.0f
            wdApp.ActiveDocument.Paragraphs.Item(2).Range.InsertBefore("ТП Опердень ЮЛ и ФЛ на "+dateOD.GetValue(1).ToString())
            rng.InsertParagraphAfter()
            
            let tbl:Word.Table=wdApp.ActiveDocument.Tables.Add(wdApp.ActiveDocument.Paragraphs.Item(3).Range, newRows.Length, 2, ref  deflt, ref deflt)
            tbl.Columns.Item(1).Width<-40.0f
            tbl.Columns.Item(2).Width<-400.0f
            tbl.Range.Font.Name<-"Times New Roman"
            tbl.Range.Font.ColorIndex<-Word.WdColorIndex.wdBlack
            tbl.Borders.InsideLineStyle<-Word.WdLineStyle.wdLineStyleSingle
            tbl.Borders.OutsideLineStyle<-Word.WdLineStyle.wdLineStyleSingle
            tbl.Borders.InsideColor<-Word.WdColor.wdColorBlack
            tbl.Borders.OutsideColor<-Word.WdColor.wdColorBlack
            
            tbl.Cell(1,1).Range.Text<-"№ стр."
            tbl.Cell(1,2).Range.Text<-"Сообщение"

            newRows |> List.iteri (fun i x -> tbl.Cell(i+2, 1).Range.Text<-(i+1).ToString()
                                              tbl.Cell(i+2, 2).Range.Text<-x
                                        )
            
            //let sw = new StreamWriter(@"C:\temp\ПРИЛОЖЕНИЕ_ЮЛиФЛ "+dateOD.GetValue(1).ToString().Trim().Substring(0, 5)+".doc");
            let ss=Word.WdSaveOptions.wdDoNotSaveChanges:>obj
            let ss1=Word.WdSaveFormat.wdFormatDocument:>obj
            let dtOper=DateTime.Parse(dateOD.GetValue(1).ToString())
            let mutable folder= @"T:\!!! ФАЙЛЫ !!!\НО_Опердень\"
            let months=[|"январь"; "февраль"; "март"; "апрель"; "май"; "июнь"; "июль"; "август"; "сентябрь"; "октябрь"; "ноябрь";"декабрь" |]

            if (Directory.Exists(folder)) then
                folder<-folder+dtOper.Year.ToString()
                if  (not(Directory.Exists(folder))) then
                    folder<-Directory.CreateDirectory(folder+"\\"+dtOper.Month.ToString()+"_"+months.GetValue(dtOper.Month-1).ToString()+"\\"+dtOper.Day.ToString("D2")+"\\").FullName
                    
                else
                    folder<-folder+"\\"+dtOper.Month.ToString()+"_"+months.GetValue(dtOper.Month-1).ToString()
                    if (not(Directory.Exists(folder))) then
                         folder<- (Directory.CreateDirectory(folder+"\\"+dtOper.Day.ToString("D2")+"\\").FullName)
                         
                    else
                        folder<-folder+"\\"+dtOper.Day.ToString("D2")+"\\"
                        if (not(Directory.Exists(folder))) then
                                folder<- (Directory.CreateDirectory(folder).FullName)
                        else
                            ()
            else
                ()
              
            
            let mutable nameFile= folder + @"ПРИЛОЖЕНИЕ_ЮЛиФЛ " + (dateOD.GetValue(1).ToString().Trim().Substring(0, 5))+".doc"

            let rec modifyNamePath(indexFile:int, fileName:string):string*bool=
                if (File.Exists(fileName)) then
                    let ff:FileInfo=FileInfo(fileName)
                    let startProc=DateTime.Parse(dd)+TimeSpan.Parse(tt)
                    if (ff.CreationTime<startProc) then
                        let path=Path.GetDirectoryName(fileName)+"\\"
                        modifyNamePath(2, path + @"ПРИЛОЖЕНИЕ_ЮЛиФЛ " + (dateOD.GetValue(1).ToString().Trim().Substring(0, 5))+ "_"+indexFile.ToString()+ ".doc")
                    else
                        ("", false)
                else
                    (fileName, true)
            
            let (nameFile, saving)=modifyNamePath(1, nameFile.ToString())

            if (saving) then 
                wdApp.ActiveDocument.SaveAs(ref (nameFile:>obj), 
                                   ref deflt, ref deflt,ref deflt, ref deflt, ref deflt,ref deflt, ref deflt, ref deflt,ref deflt,ref deflt, ref deflt,ref deflt,ref deflt, ref deflt,ref deflt)
            else ()
            wdApp.ActiveDocument.Close(ref ss,ref ss1,ref deflt) 
            //sw.Close()
            loop(i+1, data.Tail)
        else
            ()

    loop(0, rows)

    
    ()