﻿open System
open System.Net
open System.IO
open VkNet
open VkNet.Utils

let path= @""
let path2= @""
let secreteAppId=711111L

let replaceStr(str:string)= 
    let seq = [ '>'; '?'; '|'; '\\'; '/'; ':'; '<'; '*'; '"' ] 
    let localReplace (strNew:string) (x:char) = strNew.Replace(x, '_')
    List.fold localReplace str seq

let copyToQuestion (str:string) = 
    let index= str.IndexOf('?')
    str.Substring(0, index)

let getFileName (item:Model.Attachments.Audio)= 
    path+replaceStr(item.Title)+".mp3"

let getNewFileName(item:Model.Attachments.Audio)= 
    path2+replaceStr(item.Artist)+"_"+replaceStr(item.Title)+".mp3"

let downloadMusicFile (funcFileName:Model.Attachments.Audio->string) (item:Model.Attachments.Audio) = 
    try
        let client=new WebClient()
        client.DownloadFile(copyToQuestion(item.Url.AbsoluteUri), 
                                 funcFileName(item))
    with
    | _ -> Console.WriteLine("Error!" + item.Title);
            
let authorizeAndLoad(idApp: int, username:string, pswd:string)=
    let vk=new VkApi()
    vk.Authorize(idApp, username, pswd, Enums.Filters.Settings.All)
    let audio=vk.Audio.Get(secreteAppId)
    let list=Array.ofSeq(audio) 
    
    let a=list |> Array.map (downloadMusicFile getFileName)
    Console.WriteLine("Its all")
    ignore(Console.ReadLine())
    () 

let authorizeAndLoad2(idApp: int, username:string, pswd:string)=
    let vk=new VkApi()
    vk.Authorize(idApp, username, pswd, Enums.Filters.Settings.All)
    let audio=vk.Audio.Get(secreteAppId)
    let list=Array.ofSeq(audio) 
    
    let a=list |> Array.map (downloadMusicFile getNewFileName)
    Console.WriteLine("Its all")
    ignore(Console.ReadLine())
    () 


let checkFiles(idApp: int, username:string, pswd:string)=
    let vk=new VkApi()
    vk.Authorize(idApp, username, pswd, Enums.Filters.Settings.All)
    let audio=vk.Audio.Get(secreteAppId)
    let list2=List.ofSeq(audio)

    let existInList (element:Model.Attachments.Audio) (list:list<Model.Attachments.Audio>) =
        list |> List.exists( fun x -> (replaceStr(x.Title)).GetHashCode() = replaceStr(element.Title).GetHashCode())
                                       
    let rec looper (element) (list) =
        let pathFile=getFileName(element)
        if (existInList element list) then
            if (File.Exists(pathFile)) then
                File.Delete(pathFile)
                downloadMusicFile getNewFileName element
        else
            if (not(File.Exists(pathFile))) then
                downloadMusicFile getNewFileName element
            else 
                ()
            ()
        if list.IsEmpty then ()
        else
            let next::tail=list
            looper next tail
    looper list2.Head list2.Tail

    Console.WriteLine(Directory.GetFiles(path).Length)
    Console.WriteLine("Its all")
    ignore(Console.ReadLine())
    ()

[<EntryPoint>]
let main argv =
    //authorize
    //authorizeAndLoad2
    0 // return an integer exit code
