﻿// Дополнительные сведения о F# см. на http://fsharp.net
// Дополнительную справку см. в проекте "Учебник по F#".
open System

let rec quicksort = function
                        | [] -> []
                        | h::t -> quicksort ([ for x in t do if x<=h then yield x]) 
                                  @ [h] @ quicksort ([ for x in t do if x>h then yield x]);;

[<EntryPoint>]
let main argv = 
    let rnd:Random=new Random()
    
    let max=1000000
    let a = List.init max (fun _ -> rnd.Next(max) )

    let dt=DateTime.Now;

    let b=quicksort a

    let dt1=DateTime.Now;
    let ts=dt1-dt;

    Console.WriteLine("{0} : {1} : {2} : {3}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);

    Console.ReadLine() |> ignore    
    0 // возвращение целочисленного кода выхода
