﻿open System


let rec insert(x:int, xs:list<int>):list<int>=
    match(xs) with
    | [] -> [x]
    | y::ys -> if (x<=y) then x::xs else y::insert(x, ys)

let rec iSort(xs:list<int>):list<int> =
    match(xs) with
    | [] -> []
    | y::ys -> insert(y, iSort(ys))

let insertionSort(x:list<int>):list<int>   = iSort(x)

let a=insertionSort([2;32;3;4;5;1;1;2;3;5;3])

for i in a do
    Console.WriteLine(i)

ignore(Console.ReadLine())