
//сортировка выбором
let rec selectSort(a:list<int>)=
    match (a) with
    | [] -> []
    | _::tail ->  let minElement=List.min(a)
                  (a |> List.filter (fun x -> x=minElement))@selectSort( a |> List.filter (fun x -> x<>minElement) )