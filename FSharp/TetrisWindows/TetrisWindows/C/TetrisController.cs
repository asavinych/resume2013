﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using TetrisWindows.ModelTetris;
using System.Diagnostics;

namespace TetrisWindows.C
{
    public class TetrisController
    {
        public Graphics graphic
        {
            get;
            set;
        }

        public int game_width
        {
            get;
            set;
        }

        public int game_height
        {
            get;
            set;
        }

        public ModelTetris.ModelTetris mt;

        public TetrisController(Graphics gr, int width,int height, int size)
        {
            graphic = gr;
            game_height = height;
            game_width = width;
            Board.FillData();
            mt = new ModelTetris.ModelTetris(height);
            mt.InitGame();
        }

        public void DrawPiece(int pX, int pY, int pPiece, int pRotation)
        {
            int mPixelsX = mt.GetXPosInPixels(pX);
            int mPixelsY = mt.GetYPosInPixels(pY);

            Brush color=Brushes.AliceBlue;

            for (int i = 0; i < Board.piece_blocks; i++)
            {
                for (int j = 0; j < Board.piece_blocks; j++)
                {
                    int block=mt.GetBlockTypeFrom(pPiece, pRotation, j, i);
                    switch (block)
                    {
                        case 1: color = Brushes.Green;
                            break;
                        case 2: color = Brushes.Yellow;
                            break;
                        default: break;
                    }

                    if (block != 0)
                    {
                        graphic.FillRectangle(color, mPixelsX + i * Board.block_size, mPixelsY + j * Board.block_size, Board.block_size - 1, Board.block_size - 1);
                    }
                }
            }
        }

        public void DrawBoard()
        {
            int mX1 = Board.board_position - (Board.block_size * (Board.board_width / 2)) - 1;
            int mX2 = Board.board_position + (Board.block_size * (Board.board_width / 2));
            int mY = mt.ScreenHeight - (Board.block_size * Board.board_height);

            graphic.FillRectangle(Brushes.Yellow, mX1 - Board.board_line_width, mY, (Board.board_line_width), (mt.ScreenHeight - (mY + 1)));
            graphic.FillRectangle(Brushes.Yellow, mX2, mY, Board.board_line_width, (mt.ScreenHeight - 1)-mY);

            mX1 += 1;
            for (int i = 0; i < Board.board_width; i++)
            {
                for (int j = 0; j < Board.board_height; j++)
                {
                    // Check if the block is filled, if so, draw it
                    if (!mt.IsFreeBlock(i, j))
                        graphic.FillRectangle(Brushes.Red, mX1 + i * Board.block_size,
                                                mY + j * Board.block_size,
                                                Board.block_size - 1,
                                                Board.block_size - 1);
                }
            }
        }

        public void DrawScene()
        {
            DrawBoard();
            DrawPiece(mt.mPosX, mt.mPosY, mt.mPeice, mt.mRotation);
            DrawPiece(mt.mNextPosX, mt.mNextPosY, mt.mNextPiece, mt.mNextRotation);                        
        }

        public void Tact(object sender, EventArgs e)
        {
            lock (mt)
            {
                graphic.Clear(Color.Blue);

                DrawScene();

                if (mt.IsPossibleMovement(mt.mPosX, mt.mPosY + 1, mt.mPeice, mt.mRotation))
                {
                    mt.mPosY++;
                }
                else
                {

                    mt.StorePiece(mt.mPosX, mt.mPosY, mt.mPeice, mt.mRotation);

                    mt.DeletePossibleLines();

                    if (mt.IsGameOver())
                    {
                        (sender as Timer).Stop();
                        MessageBox.Show("Game Over!");
                    }

                    mt.CreateNewPiece();
                }
            }
        }

        public void KeyPress(object sender, KeyEventArgs e)
        {
            lock (mt)
            {
                switch (e.KeyCode)
                {
                    case (Keys.Right):
                        if (mt.IsPossibleMovement(mt.mPosX + 1, mt.mPosY, mt.mPeice, mt.mRotation))
                            mt.mPosX++;
                        break;
                    case (Keys.Left):
                        if (mt.IsPossibleMovement(mt.mPosX - 1, mt.mPosY, mt.mPeice, mt.mRotation))
                            mt.mPosX--;
                        break;
                    case (Keys.Down):
                        if (mt.IsPossibleMovement(mt.mPosX, mt.mPosY + 1, mt.mPeice, mt.mRotation))
                            mt.mPosY++;
                        break;
                    case (Keys.X):
                        // Check collision from up to down
                        while (mt.IsPossibleMovement(mt.mPosX, mt.mPosY, mt.mPeice, mt.mRotation)) { mt.mPosY++; }

                        mt.StorePiece(mt.mPosX, mt.mPosY - 1, mt.mPeice, mt.mRotation);

                        mt.DeletePossibleLines();

                        if (mt.IsGameOver())
                        {
                            dynamic a = (sender as Form);
                            a.timer1.Stop();
                            MessageBox.Show("Game Over!");
                        }

                        mt.CreateNewPiece();

                        break;
                    case (Keys.Z):
                        if (mt.IsPossibleMovement(mt.mPosX, mt.mPosY, mt.mPeice, (mt.mRotation + 1) % 4))
                            mt.mRotation = (mt.mRotation + 1) % 4;
                        break;
                }
            }
        }
    }
}
