﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TetrisWindows.C;

namespace TetrisWindows
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            TetrisView fm = new TetrisView();
            TetrisController tc = new TetrisController(fm.CreateGraphics(), 220, 320, 10);

            fm.KeyDown +=new KeyEventHandler(tc.KeyPress);
            fm.timer1.Tick += new EventHandler(tc.Tact);
            fm.timer1.Enabled = true;
            fm.timer1.Start();

            Application.Run(fm);
            
        }
    }
}
