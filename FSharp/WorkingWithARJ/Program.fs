﻿namespace SSS.SendLP

open System
open System.IO
open System.Diagnostics
open System.Text
open System.Security


[<Class>]
type public ParsingPostFile(filenamearchive:string, pathtosend:string)=class
    
    let fileNameArchive= filenamearchive
    let pathToSend= if (pathtosend.Chars(pathtosend.Length-1)='\\') then pathtosend else pathtosend+ @"\"
 
    member public this.calcCRC32(str:string)=
      
        let mutable strHash=String.Empty
        let mutable strRes:list<string>=[]

        let files=Directory.GetFiles(str, "*.*", SearchOption.AllDirectories) 

        for j in files do
            let crc32=new CRC32()
            let fileStr=new FileStream(j, FileMode.Open)

            for i in crc32.ComputeHash(fileStr) do
                strHash<-i.ToString("x2").ToLower()+strHash
                   
            strRes <- strRes@[(strHash+" "+ j)] 
            strHash<-String.Empty
            fileStr.Close();
            fileStr.Dispose();

        List.toArray(strRes)

    member public this.replaceNameDir(str:string)=
        let mutable result=String.Empty

        if (str.IndexOf('.')>(-1)) then
            //let mutable name=Path.GetDirectoryName(str)
            Directory.Move(pathToSend+str,pathToSend+str.Replace(".", "").Replace(" ", ""))
            result<-str.Replace(".", "").Replace(" ", "")
        else
            result<-str
    
        let dirAll=Directory.GetDirectories(pathToSend+result, "*.*", SearchOption.AllDirectories)

        for i in dirAll do
            if (i.IndexOfAny([|' '; '('; ')'|])>(-1)) then
                let strNew=i.Replace(" ", "_").Replace("(", "").Replace(")", "")
                Directory.Move(i,strNew)

        result
    
    member public this.archiveFolder(str:string, outIfns:string, inIfns:string)=
        let p=new Process()
        p.StartInfo.FileName<-fileNameArchive
        let a=DateTime.Now
        let allDir=Directory.GetDirectories(pathToSend+str, "*.*", SearchOption.AllDirectories)

        let mutable partStr=String.Empty

        if (allDir.Length=0) then
            partStr<-"\""+str+"\""
        else
            partStr<-" ... "+ (List.fold (fun acc el -> acc+" \""+el+"\"") "" 
                                            (Array.toList(allDir))).Replace(pathToSend, "")
        p.StartInfo.Arguments<-" A \""+outIfns+"_"+inIfns+
                                String.Format("_{0:D2}.{1:D2}.{2:D2}_{3:D2}.{4:D2}.arj\" ", 
                                    a.Day, a.Month, a.Year, a.Hour, a.Minute)+partStr

        Debug.WriteLine(p.StartInfo.Arguments)

        p.StartInfo.UseShellExecute<-false
        p.StartInfo.WorkingDirectory<-pathToSend
        ignore(p.Start())
        ()
        (outIfns+"_"+inIfns+String.Format("_{0:D2}.{1:D2}.{2:D2}_{3:D2}.{4:D2}.arj", 
                                    a.Day, a.Month, a.Year, a.Hour, a.Minute))

end