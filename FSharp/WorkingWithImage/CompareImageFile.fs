﻿open System.IO
open System

let compareFiles (str:string)=
    let files=Directory.GetFiles(str, "*.jpg", SearchOption.AllDirectories)
    let mutable finalList:list<string*string>=[]
    for i in files  do
        let crc32=new CRC32()
        let mutable strHash=String.Empty
        let fileStr=new FileStream(i, FileMode.Open)

        for j in crc32.ComputeHash(fileStr) do
            strHash<-j.ToString("x2").ToLower()+strHash
        
        finalList<-(i, strHash)::finalList
        
    let newDisList=Seq.toList(finalList |> Seq.distinctBy (fun (_,y) -> y))

    for j in newDisList do
        let (path, crc)=j
        File.Copy(path, @"C:\temp\KLP\"+Path.GetFileNameWithoutExtension(path)+crc+Path.GetExtension(path))

    ()

compareFiles(@"C:\temp\СБМ-Групп")