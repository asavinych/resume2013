﻿open System.IO;
open System.Drawing;

//Изменение размеров изображения

//let dir= @"C:\temp\ex"
//
//let files=
//    Directory.GetFiles(dir, "*.png") |> 
//        Array.map (fun file -> let bmp=Bitmap.FromFile(file)
//                               let resized=new Bitmap(350, 100)
//                               let gr=Graphics.FromImage(resized)
//                               let dst=Rectangle(0, 0, 350, 100)
//                               let src=Rectangle(220,244, 350, 100)
//                               gr.DrawImage(bmp, dst, src, GraphicsUnit.Pixel)
//                               (resized,file)) |> 
//        Array.iter( fun (bmp, file) -> ))
//              
//
//files

let convert(from:string, toFile:string, dst:Rectangle, resized:Bitmap, src:Rectangle) =
    let bmp=Bitmap.FromFile(from)
    let mutable gr=Graphics.FromImage(resized)
    gr.DrawImage(bmp, dst, src, GraphicsUnit.Pixel)
    resized.Save(toFile) 
    

//let mutable x=0
//for i in [0..22] do
//    convert(@"E:\sprite.png", @"C:\temp\pp"+i.ToString()+".png", Rectangle(0, 0, 32, 32), new Bitmap(32,32), Rectangle(x, 99, 32, 32))
//    x<-x+34

let convertAll()=
    let files=Directory.GetFiles(@"E:\Учеба\M", "*.png")
    files |> Array.iter ( fun x -> let im:Image=Image.FromFile(x)
                                   convert(x,@"E:\Учеба\1\24\"+Path.GetFileName(x) , Rectangle(0, 0, 24,24), new Bitmap(24,24), Rectangle(0, 0, im.Width,im.Height))
                                   convert(x,@"E:\Учеба\1\48\"+Path.GetFileName(x) , Rectangle(0, 0, 48,48), new Bitmap(48,48), Rectangle(0, 0, im.Width,im.Height))
                                   )
//convert(@"E:\sprite.png", @"C:\temp\eq1.png", Rectangle(0, 0, 32, 32), new Bitmap(32,32), Rectangle(98, 3, 32, 32))
//convert(@"E:\sprite.png", @"C:\temp\eq2.png", Rectangle(0, 0, 32, 32), new Bitmap(32,32), Rectangle(298, 3, 32, 32))
//convert(@"E:\sprite.png", @"C:\temp\eq3.png", Rectangle(0, 0, 32, 32), new Bitmap(32,32), Rectangle(338, 3, 32, 32))
//convert(@"E:\sprite.png", @"C:\temp\eq11.png", Rectangle(0, 0, 24, 24), new Bitmap(24,24), Rectangle(98, 3, 32, 32))
//convert(@"E:\sprite.png", @"C:\temp\eq21.png", Rectangle(0, 0, 24, 24), new Bitmap(24,24), Rectangle(298, 3, 32, 32))
//convert(@"E:\sprite.png", @"C:\temp\eq31.png", Rectangle(0, 0, 24, 24), new Bitmap(24,24), Rectangle(338, 3, 32, 32))
//convert(@"E:\sprite.png", @"C:\temp\eq12.png", Rectangle(0, 0, 48, 48), new Bitmap(48,48), Rectangle(98, 3, 32, 32))
//convert(@"E:\sprite.png", @"C:\temp\eq22.png", Rectangle(0, 0, 48, 48), new Bitmap(48,48), Rectangle(298, 3, 32, 32))
//convert(@"E:\sprite.png", @"C:\temp\eq32.png", Rectangle(0, 0, 48, 48), new Bitmap(48,48), Rectangle(338, 3, 32, 32))
////convert(@"E:\sprite.png", @"C:\temp\button_work_play.png", Rectangle(0, 0, 43, 46), new Bitmap(43,46), Rectangle(470, 276, 43, 46))
convertAll()