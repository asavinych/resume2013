package com.example.AlarmSync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA.
 * User: Dimbo
 * Date: 21.08.13
 * Time: 12:09
 * To change this template use File | Settings | File Templates.
 */
public class AlarmMy extends Service {


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        AppSam syn = (AppSam) getApplication();

        syn.synchronizator.sync();

        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
