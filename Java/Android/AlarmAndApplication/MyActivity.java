package com.example.AlarmSync;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Calendar;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Calendar cal = Calendar.getInstance();

        cal.add(Calendar.SECOND, 10);

        Intent intent = new Intent(MyActivity.this, AlarmMy.class);
        PendingIntent pintent = PendingIntent.getService(MyActivity.this, 0, intent,
                0);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                20 * 1000, pintent);
        Button btn = (Button) findViewById(R.id.button);

        AppSam ss = (AppSam) getApplication();

        ss.synchronizator.sync();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // startService(new Intent(getBaseContext(), AlarmMy.class));
            }
        });


    }


}
