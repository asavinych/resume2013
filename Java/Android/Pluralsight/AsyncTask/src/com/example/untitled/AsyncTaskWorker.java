package com.example.untitled;

import android.location.Location;
import android.os.AsyncTask;
import android.widget.TextView;

public class AsyncTaskWorker extends AsyncTask<TextView, String, Boolean> {

    TextView _textView;

    @Override
    protected Boolean doInBackground(TextView... textViews) {

        if (textViews.length > 0) {
            _textView = textViews[0];

            Worker worker = new Worker(_textView.getContext());

            publishProgress("Starting");

            Location location = worker.getLocation();
            publishProgress("Retrieved Location");

            String address = worker.reverseGeocode(location);
            publishProgress("Retrieved Address");

            worker.save(location, address, "ResponsiveUX.out");
            publishProgress("Done");

            return true;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if (aBoolean.booleanValue()) {
            _textView.setText("Done");
        } else {
            _textView.setText("Error");
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        _textView.setText(values[0]);
    }
}
