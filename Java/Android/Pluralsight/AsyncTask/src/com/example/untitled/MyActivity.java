package com.example.untitled;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.view.*;
import android.widget.*;

public class MyActivity extends Activity {


    TextView _defualtTextView;
    AsyncTaskWorker _asyncTaskWorker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        _defualtTextView = (TextView) findViewById(R.id.my_text);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuDoWork:
                doWork();
                break;
            case R.id.menuQuit:
                finish();
                break;
            default:
                super.onOptionsItemSelected(item);
                break;
        }

        return true;
    }

    public void doWork() {
        _asyncTaskWorker = new AsyncTaskWorker();

        _asyncTaskWorker.execute(_defualtTextView);
    }


}
