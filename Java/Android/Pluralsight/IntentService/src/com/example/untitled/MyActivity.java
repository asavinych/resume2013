package com.example.untitled;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.*;
import android.widget.*;

public class MyActivity extends Activity {


    TextView _defualtTextView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        /*
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()   // or .detectAll() for all detectable problems
                .penaltyLog()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .penaltyDeath()
                .build());
          */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        _defualtTextView = (TextView) findViewById(R.id.my_text);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuDoWork:
                doWork();
                break;
            case R.id.menuQuit:
                finish();
                break;
            default:
                super.onOptionsItemSelected(item);
                break;
        }

        return true;
    }

    public void doWork() {
        LogHelper.ProcessAndThreadId("MyActivity.doWork");

        Intent serviceIntent = new Intent(this, OnDemandWorker.class);
        serviceIntent.putExtra("fileName", "AlternateFile.out");
        startService(serviceIntent);


        Toast toast = Toast.makeText(this, "Work has started", Toast.LENGTH_LONG);
        toast.show();
    }


}
