package com.example.untitled;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;

/**
 * Created with IntelliJ IDEA.
 * User: SFP
 * Date: 17.06.13
 * Time: 0:13
 */
public class OnDemandWorker extends IntentService {

    public OnDemandWorker() {
        super("OnDemandWorker");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Worker worker = new Worker(this);

        String fileName = intent.getStringExtra("fileName");
        if (fileName == null) {
            fileName = "ResponsiveUx.out";
        }

        Location location = worker.getLocation();

        String address = worker.reverseGeocode(location);

        worker.save(location, address, fileName);
    }
}
