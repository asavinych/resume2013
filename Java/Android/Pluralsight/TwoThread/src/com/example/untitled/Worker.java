package com.example.untitled;

import android.content.*;
import android.location.*;
import android.os.Environment;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.*;
import java.text.*;
import java.util.*;

public class Worker {
    public final boolean _upsGpsToGetLocation = false;

    Context _context;

    public Worker(Context context) {
        _context = context;
    }

    public Location getLocation() {
        Location lastLocation = null;

        if (_upsGpsToGetLocation) {
            LocationManager locationManager = (LocationManager) _context.getSystemService(Context.LOCATION_SERVICE);
            lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }

        if (lastLocation == null) {
            lastLocation = createLocationManually();
        }

        simulateDelay();

        return lastLocation;
    }

    public String reverseGeocode(Location location) {
        String addressDescription = null;

        try {
            Geocoder geocoder = new Geocoder(_context);
            List<Address> addressList = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

            if (!addressList.isEmpty()) {
                Address firstAddress = addressList.get(0);
                StringBuilder addressBuilder = new StringBuilder();

                for (int idx = 0; idx <= firstAddress.getMaxAddressLineIndex(); idx++) {
                    if (idx != 0)
                        addressBuilder.append(", ");
                    addressBuilder.append(firstAddress.getAddressLine(idx));
                }

                addressDescription = addressBuilder.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        simulateDelay();

        return addressDescription;
    }

    public void save(Location location, String address, String fileName) {
        try {
            File targetDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            assureThatDirectoryExist(targetDir);

            File outFile = new File(targetDir, fileName);
            FileWriter fileWriter = new FileWriter(outFile, true);
            BufferedWriter writer = new BufferedWriter(fileWriter);

            String outLine = String.format("%s:%f/%f", DateFormat.getDateTimeInstance().format(location.getTime()));

            writer.write(outLine);
            writer.write(address);

            writer.flush();
            writer.close();
            fileWriter.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        simulateDelay();
    }

    private Location createLocationManually() {
        Location lastLocation = new Location("fake");

        Date now = new Date();

        lastLocation.setTime(now.getTime());

        lastLocation.setLatitude(40.1);
        lastLocation.setLongitude(50.1);

        return lastLocation;
    }

    public String reverseGeoCodeWithWebService(Location location) {
        StringBuilder stringBuilder = new StringBuilder();
        String addressDescription = null;

        try {
            String serviceUrl =
                    String.format("http://maps.google.com/maps/api/geocode/xml?sensor=false&latlng=%f,%f", location.getLatitude(), location.getLongitude());
            HttpGet httpGet = new HttpGet(serviceUrl);
            HttpClient client = new DefaultHttpClient();

            HttpResponse response = client.execute(httpGet);

            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();

            InputStreamReader reader = new InputStreamReader(stream);

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();

            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(reader);

            boolean isAddressNode = false;
            int eventType = xpp.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    String tagName = xpp.getName();

                    if (tagName.equalsIgnoreCase("formatted_address")) {
                        isAddressNode = true;
                    }
                } else if (isAddressNode && eventType == XmlPullParser.TEXT) {
                    addressDescription = xpp.getText();
                    break;
                }
                eventType = xpp.next();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return addressDescription;
    }

    private void assureThatDirectoryExist(File directory) {
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

    private void simulateDelay() {
        try {
            Thread.sleep(3000);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
