﻿#pragma warning disable 10001

namespace ConvertFilePost
{
  public partial class MForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private  mutable  components : System.ComponentModel.IContainer = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">
    /// true if managed resources should be disposed; otherwise, false.
    /// </param>
    protected override Dispose(disposing : bool) :  void
    {
      when (disposing && components != null)
        components.Dispose();

      base.Dispose(disposing);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private InitializeComponent() :  void
    {
        this.textBox1 = System.Windows.Forms.TextBox();
        this.button1 = System.Windows.Forms.Button();
        this.textBox2 = System.Windows.Forms.TextBox();
        this.button2 = System.Windows.Forms.Button();
        this.button4 = System.Windows.Forms.Button();
        this.menuStrip1 = System.Windows.Forms.MenuStrip();
        this.aboutToolStripMenuItem = System.Windows.Forms.ToolStripMenuItem();
        this.label1 = System.Windows.Forms.Label();
        this.textBox4 = System.Windows.Forms.TextBox();
        this.label2 = System.Windows.Forms.Label();
        this.textBox5 = System.Windows.Forms.TextBox();
        this.label3 = System.Windows.Forms.Label();
        this.textBox6 = System.Windows.Forms.TextBox();
        this.button5 = System.Windows.Forms.Button();
        this.menuStrip1.SuspendLayout();
        this.SuspendLayout();
        // 
        // textBox1
        // 
        this.textBox1.Location = System.Drawing.Point(45,  60);
        this.textBox1.Name = "textBox1";
        this.textBox1.ReadOnly = true;
        this.textBox1.Size = System.Drawing.Size(375,  20);
        this.textBox1.TabIndex = 0;
        // 
        // button1
        // 
        this.button1.Location = System.Drawing.Point(85,  32);
        this.button1.Name = "button1";
        this.button1.Size = System.Drawing.Size(293,  22);
        this.button1.TabIndex = 1;
        this.button1.Text = "Выбрать директорию с  файлами новой версии";
        this.button1.UseVisualStyleBackColor = true;
        this.button1.Click += System.EventHandler(this.button1_Click);
        // 
        // textBox2
        // 
        this.textBox2.Location = System.Drawing.Point(45,  128);
        this.textBox2.Name = "textBox2";
        this.textBox2.ReadOnly = true;
        this.textBox2.Size = System.Drawing.Size(375,  20);
        this.textBox2.TabIndex = 3;
        // 
        // button2
        // 
        this.button2.Location = System.Drawing.Point(85,  100);
        this.button2.Name = "button2";
        this.button2.Size = System.Drawing.Size(293,  22);
        this.button2.TabIndex = 2;
        this.button2.Text = "Выбрать директорию для файлов \"старой версии\"";
        this.button2.UseVisualStyleBackColor = true;
        this.button2.Click += System.EventHandler(this.button2_Click);
        // 
        // button4
        // 
        this.button4.Location = System.Drawing.Point(85,  398);
        this.button4.Name = "button4";
        this.button4.Size = System.Drawing.Size(293,  22);
        this.button4.TabIndex = 6;
        this.button4.Text = "Обработать";
        this.button4.UseVisualStyleBackColor = true;
        this.button4.Click += System.EventHandler(this.button4_Click);
        // 
        // menuStrip1
        // 
        this.menuStrip1.Items.AddRange(array[
                    this.aboutToolStripMenuItem]);
        this.menuStrip1.Location = System.Drawing.Point(0,  0);
        this.menuStrip1.Name = "menuStrip1";
        this.menuStrip1.Size = System.Drawing.Size(470,  24);
        this.menuStrip1.TabIndex = 7;
        this.menuStrip1.Text = "menuStrip1";
        // 
        // aboutToolStripMenuItem
        // 
        this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
        this.aboutToolStripMenuItem.Size = System.Drawing.Size(83,  20);
        this.aboutToolStripMenuItem.Text = "О программе";
        this.aboutToolStripMenuItem.Click += System.EventHandler(this.aboutToolStripMenuItem_Click);
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Location = System.Drawing.Point(115,  155);
        this.label1.Name = "label1";
        this.label1.Size = System.Drawing.Size(202,  13);
        this.label1.TabIndex = 8;
        this.label1.Text = "имя сервера (например: )";
        // 
        // textBox4
        // 
        this.textBox4.Location = System.Drawing.Point(85,  171);
        this.textBox4.Name = "textBox4";
        this.textBox4.Size = System.Drawing.Size(293,  20);
        this.textBox4.TabIndex = 9;
        this.textBox4.Text = "";
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Location = System.Drawing.Point(135,  204);
        this.label2.Name = "label2";
        this.label2.Size = System.Drawing.Size(148,  13);
        this.label2.TabIndex = 10;
        this.label2.Text = "имя БД (например: taxes)";
        // 
        // textBox5
        // 
        this.textBox5.Location = System.Drawing.Point(85,  220);
        this.textBox5.Name = "textBox5";
        this.textBox5.Size = System.Drawing.Size(247,  20);
        this.textBox5.TabIndex = 11;
        this.textBox5.Text = "Taxes";
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Font = System.Drawing.Font("Microsoft Sans Serif",  14.25f,  System.Drawing.FontStyle.Bold,  System.Drawing.GraphicsUnit.Point,  (204 :> byte));
        this.label3.Location = System.Drawing.Point(12,  284);
        this.label3.Name = "label3";
        this.label3.Size = System.Drawing.Size(0,  24);
        this.label3.TabIndex = 12;
        // 
        // textBox6
        // 
        this.textBox6.Font = System.Drawing.Font("Microsoft Sans Serif",  12f,  System.Drawing.FontStyle.Bold,  System.Drawing.GraphicsUnit.Point,  (204 :> byte));
        this.textBox6.Location = System.Drawing.Point(12,  246);
        this.textBox6.Multiline = true;
        this.textBox6.Name = "textBox6";
        this.textBox6.ReadOnly = true;
        this.textBox6.Size = System.Drawing.Size(458,  107);
        this.textBox6.TabIndex = 13;
        this.textBox6.Text = "Нажимать кнопку \"Обработать\" один раз, \r\nпосле нажатия дождаться сообщения об \r\nокончании обработки.";
        this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // button5
        // 
        this.button5.Location = System.Drawing.Point(85,  359);
        this.button5.Name = "button5";
        this.button5.Size = System.Drawing.Size(284,  23);
        this.button5.TabIndex = 14;
        this.button5.Text = "Проверить соединение";
        this.button5.UseVisualStyleBackColor = true;
        this.button5.Click += System.EventHandler(this.button5_Click);
        // 
        // MForm
        // 
        this.AutoScaleDimensions = System.Drawing.SizeF(6f,  13f);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = System.Drawing.Size(470,  432);
        this.Controls.Add(this.button5);
        this.Controls.Add(this.textBox6);
        this.Controls.Add(this.label3);
        this.Controls.Add(this.textBox5);
        this.Controls.Add(this.label2);
        this.Controls.Add(this.textBox4);
        this.Controls.Add(this.label1);
        this.Controls.Add(this.button1);
        this.Controls.Add(this.textBox1);
        this.Controls.Add(this.button2);
        this.Controls.Add(this.textBox2);
        this.Controls.Add(this.button4);
        this.Controls.Add(this.menuStrip1);
        this.MainMenuStrip = this.menuStrip1;
        this.Name = "MForm";
        this.Text = "MainForm";
        this.menuStrip1.ResumeLayout(false);
        this.menuStrip1.PerformLayout();
        this.ResumeLayout(false);
        this.PerformLayout();
    }
}
