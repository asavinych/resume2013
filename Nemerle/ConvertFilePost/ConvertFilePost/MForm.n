﻿using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace ConvertFilePost
{
  /// <summary>
  /// Description of MForm.
  /// </summary>
  public partial class MForm : Form
  {
    public this()
    {
      InitializeComponent();
    }
    
    private mutable pathIn:string;
    
    private mutable pathOut:string;
    
    private mutable conStr:string;
    
    private mutable textBox1 : System.Windows.Forms.TextBox;
  
    private mutable button1 : System.Windows.Forms.Button;
  
    private mutable textBox2 : System.Windows.Forms.TextBox;
  
    private mutable button2 : System.Windows.Forms.Button;
    
    private mutable button3 : System.Windows.Forms.Button;
    
    private mutable textBox3 : System.Windows.Forms.TextBox;
    
    private mutable button4:System.Windows.Forms.Button;
    
    
    private button1_Click (sender : object,  e : System.EventArgs) : void
    {
        mutable a:FolderBrowserDialog=FolderBrowserDialog();
        
        when (a.ShowDialog()==DialogResult.OK)
        {
            pathIn=a.SelectedPath;
            textBox1.Text=pathIn;
        }
    }
    
    private button2_Click (sender : object,  e : System.EventArgs) : void
    {
        mutable a:FolderBrowserDialog=FolderBrowserDialog();
        
        when (a.ShowDialog()==DialogResult.OK)
        {
            pathOut=a.SelectedPath;
            textBox2.Text=pathOut;
        }
    }
    private button3_Click (sender : object,  e : System.EventArgs) : void
    {
      mutable sqlCon=OpenFileDialog();
      when (sqlCon.ShowDialog()==DialogResult.OK)
      {
         
        }
    }
    
    private button4_Click (sender : object,  e : System.EventArgs) : void
    {
        conStr=""; 
        
        mutable a:SSS.DataParsing.ParsingPostFile=SSS.DataParsing.ParsingPostFile(pathIn, pathOut+"\\", conStr);
        a.Start();
    }
  
    private mutable menuStrip1 : System.Windows.Forms.MenuStrip;
  
    private mutable aboutToolStripMenuItem : System.Windows.Forms.ToolStripMenuItem;
  
    private aboutToolStripMenuItem_Click (sender : object,  e : System.EventArgs) : void
    {
        def a=MainForm();
        _=a.ShowDialog();
        ;
    }
  
    private button5_Click (sender : object,  e : System.EventArgs) : void
    {
         
         conStr=""; 
         def b=SqlClient.SqlConnection(conStr);
         try
          {
             b.Open();
             when (b.State==ConnectionState.Open)
             {
                 _=MessageBox.Show("Прошло!");
             }
         }
         catch
         {
             | e => _=MessageBox.Show("Ошибка!");
         }
    }
  
    private mutable label1 : System.Windows.Forms.Label;
  
    private mutable textBox4 : System.Windows.Forms.TextBox;
  
    private mutable label2 : System.Windows.Forms.Label;
  
    private mutable textBox5 : System.Windows.Forms.TextBox;
  
    private mutable label3 : System.Windows.Forms.Label;
  
    private mutable textBox6 : System.Windows.Forms.TextBox;
  
    private mutable button5 : System.Windows.Forms.Button;
  }
}
