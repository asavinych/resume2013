﻿#pragma warning disable 10001

namespace ConvertFilePost
{
  public partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private  mutable  components : System.ComponentModel.IContainer = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">
    /// true if managed resources should be disposed; otherwise, false.
    /// </param>
    protected override Dispose(disposing : bool) :  void
    {
      when (disposing && components != null)
        components.Dispose();

      base.Dispose(disposing);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private InitializeComponent() :  void
    {
        mutable resources = System.ComponentModel.ComponentResourceManager(typeof(MainForm));
        this.pictureBox1 = System.Windows.Forms.PictureBox();
        this.richTextBox1 = System.Windows.Forms.RichTextBox();
        (this.pictureBox1 :> System.ComponentModel.ISupportInitialize).BeginInit();
        this.SuspendLayout();
        // 
        // pictureBox1
        // 
        this.pictureBox1.Image = (resources.GetObject("pictureBox1.Image") :> System.Drawing.Image);
        this.pictureBox1.Location = System.Drawing.Point(0,  3);
        this.pictureBox1.Name = "pictureBox1";
        this.pictureBox1.Size = System.Drawing.Size(194,  144);
        this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
        this.pictureBox1.TabIndex = 0;
        this.pictureBox1.TabStop = false;
        // 
        // richTextBox1
        // 
        this.richTextBox1.Location = System.Drawing.Point(195,  3);
        this.richTextBox1.Name = "richTextBox1";
        this.richTextBox1.ReadOnly = true;
        this.richTextBox1.Size = System.Drawing.Size(365,  144);
        this.richTextBox1.TabIndex = 1;
        this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
        // 
        // MainForm
        // 
        this.AutoScaleDimensions = System.Drawing.SizeF(6f,  13f);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = System.Drawing.Size(566,  149);
        this.Controls.Add(this.richTextBox1);
        this.Controls.Add(this.pictureBox1);
        this.Name = "MainForm";
        this.Text = "About";
        (this.pictureBox1 :> System.ComponentModel.ISupportInitialize).EndInit();
        this.ResumeLayout(false);
        this.PerformLayout();
    }
  }
}
