//#pragma warning disable 10001
namespace ExampleWorkWithEmail
{
  public partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private  mutable  components : System.ComponentModel.IContainer = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">
    /// true if managed resources should be disposed; otherwise, false.
    /// </param>
    protected override Dispose(disposing : bool) :  void
    {
      when (disposing && components != null)
        components.Dispose();

      base.Dispose(disposing);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private InitializeComponent() :  void
    {
        this.button1 = System.Windows.Forms.Button();
        this.comboBox1 = System.Windows.Forms.ComboBox();
        this.label1 = System.Windows.Forms.Label();
        this.textBox1 = System.Windows.Forms.TextBox();
        this.label2 = System.Windows.Forms.Label();
        this.textBox2 = System.Windows.Forms.TextBox();
        this.checkBox1 = System.Windows.Forms.CheckBox();
        this.richTextBox1 = System.Windows.Forms.RichTextBox();
        this.label3 = System.Windows.Forms.Label();
        this.textBox3 = System.Windows.Forms.TextBox();
        this.button2 = System.Windows.Forms.Button();
        this.button3 = System.Windows.Forms.Button();
        this.textBox4 = System.Windows.Forms.TextBox();
        this.label4 = System.Windows.Forms.Label();
        this.textBox5 = System.Windows.Forms.TextBox();
        this.label5 = System.Windows.Forms.Label();
        this.textBox6 = System.Windows.Forms.TextBox();
        this.label6 = System.Windows.Forms.Label();
        this.textBox7 = System.Windows.Forms.TextBox();
        this.listBox1 = System.Windows.Forms.ListBox();
        this.button4 = System.Windows.Forms.Button();
        this.label7 = System.Windows.Forms.Label();
        this.SuspendLayout();
        // 
        // button1
        // 
        this.button1.Location = System.Drawing.Point(15,  359);
        this.button1.Name = "button1";
        this.button1.Size = System.Drawing.Size(75,  23);
        this.button1.TabIndex = 0;
        this.button1.Text = "Отправить";
        this.button1.UseVisualStyleBackColor = true;
        this.button1.Click += System.EventHandler(this.button1_Click);
        // 
        // comboBox1
        // 
        this.comboBox1.FormattingEnabled = true;
        this.comboBox1.Items.AddRange(array[
                    "Send", 
                    "Recieve"] :> array[object]);
        this.comboBox1.Location = System.Drawing.Point(51,  12);
        this.comboBox1.Name = "comboBox1";
        this.comboBox1.Size = System.Drawing.Size(150,  21);
        this.comboBox1.TabIndex = 1;
        this.comboBox1.SelectedIndexChanged += System.EventHandler(this.comboBox1_SelectedIndexChanged);
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Location = System.Drawing.Point(12,  48);
        this.label1.Name = "label1";
        this.label1.Size = System.Drawing.Size(33,  13);
        this.label1.TabIndex = 2;
        this.label1.Text = "Login";
        // 
        // textBox1
        // 
        this.textBox1.Location = System.Drawing.Point(51,  45);
        this.textBox1.Name = "textBox1";
        this.textBox1.Size = System.Drawing.Size(150,  20);
        this.textBox1.TabIndex = 3;
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Location = System.Drawing.Point(207,  48);
        this.label2.Name = "label2";
        this.label2.Size = System.Drawing.Size(53,  13);
        this.label2.TabIndex = 4;
        this.label2.Text = "Password";
        // 
        // textBox2
        // 
        this.textBox2.Location = System.Drawing.Point(266,  45);
        this.textBox2.Name = "textBox2";
        this.textBox2.Size = System.Drawing.Size(143,  20);
        this.textBox2.TabIndex = 5;
        // 
        // checkBox1
        // 
        this.checkBox1.AutoSize = true;
        this.checkBox1.Location = System.Drawing.Point(430,  48);
        this.checkBox1.Name = "checkBox1";
        this.checkBox1.Size = System.Drawing.Size(46,  17);
        this.checkBox1.TabIndex = 6;
        this.checkBox1.Text = "SSL";
        this.checkBox1.UseVisualStyleBackColor = true;
        // 
        // richTextBox1
        // 
        this.richTextBox1.Location = System.Drawing.Point(15,  138);
        this.richTextBox1.Name = "richTextBox1";
        this.richTextBox1.Size = System.Drawing.Size(518,  163);
        this.richTextBox1.TabIndex = 7;
        this.richTextBox1.Text = "";
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Location = System.Drawing.Point(12,  84);
        this.label3.Name = "label3";
        this.label3.Size = System.Drawing.Size(20,  13);
        this.label3.TabIndex = 8;
        this.label3.Text = "To";
        // 
        // textBox3
        // 
        this.textBox3.Location = System.Drawing.Point(51,  84);
        this.textBox3.Name = "textBox3";
        this.textBox3.Size = System.Drawing.Size(150,  20);
        this.textBox3.TabIndex = 9;
        // 
        // button2
        // 
        this.button2.Location = System.Drawing.Point(12,  307);
        this.button2.Name = "button2";
        this.button2.Size = System.Drawing.Size(98,  23);
        this.button2.TabIndex = 10;
        this.button2.Text = "Add File";
        this.button2.UseVisualStyleBackColor = true;
        this.button2.Click += System.EventHandler(this.button2_Click);
        // 
        // button3
        // 
        this.button3.Location = System.Drawing.Point(116,  307);
        this.button3.Name = "button3";
        this.button3.Size = System.Drawing.Size(98,  23);
        this.button3.TabIndex = 12;
        this.button3.Text = "Remove file";
        this.button3.UseVisualStyleBackColor = true;
        // 
        // textBox4
        // 
        this.textBox4.Location = System.Drawing.Point(220,  309);
        this.textBox4.Name = "textBox4";
        this.textBox4.ReadOnly = true;
        this.textBox4.Size = System.Drawing.Size(319,  20);
        this.textBox4.TabIndex = 13;
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Location = System.Drawing.Point(217,  87);
        this.label4.Name = "label4";
        this.label4.Size = System.Drawing.Size(43,  13);
        this.label4.TabIndex = 14;
        this.label4.Text = "Subject";
        // 
        // textBox5
        // 
        this.textBox5.Location = System.Drawing.Point(266,  84);
        this.textBox5.Name = "textBox5";
        this.textBox5.Size = System.Drawing.Size(267,  20);
        this.textBox5.TabIndex = 15;
        // 
        // label5
        // 
        this.label5.AutoSize = true;
        this.label5.Location = System.Drawing.Point(207,  12);
        this.label5.Name = "label5";
        this.label5.Size = System.Drawing.Size(29,  13);
        this.label5.TabIndex = 17;
        this.label5.Text = "Host";
        // 
        // textBox6
        // 
        this.textBox6.Location = System.Drawing.Point(266,  12);
        this.textBox6.Name = "textBox6";
        this.textBox6.Size = System.Drawing.Size(143,  20);
        this.textBox6.TabIndex = 18;
        this.textBox6.Text = "smtp.gmail.com";
        // 
        // label6
        // 
        this.label6.AutoSize = true;
        this.label6.Location = System.Drawing.Point(427,  15);
        this.label6.Name = "label6";
        this.label6.Size = System.Drawing.Size(26,  13);
        this.label6.TabIndex = 19;
        this.label6.Text = "Port";
        // 
        // textBox7
        // 
        this.textBox7.Location = System.Drawing.Point(462,  12);
        this.textBox7.Name = "textBox7";
        this.textBox7.Size = System.Drawing.Size(71,  20);
        this.textBox7.TabIndex = 20;
        this.textBox7.Text = "25";
        // 
        // listBox1
        // 
        this.listBox1.FormattingEnabled = true;
        this.listBox1.Location = System.Drawing.Point(12,  84);
        this.listBox1.Name = "listBox1";
        this.listBox1.Size = System.Drawing.Size(521,  264);
        this.listBox1.TabIndex = 21;
        this.listBox1.Visible = false;
        // 
        // button4
        // 
        this.button4.Location = System.Drawing.Point(108,  359);
        this.button4.Name = "button4";
        this.button4.Size = System.Drawing.Size(87,  23);
        this.button4.TabIndex = 22;
        this.button4.Text = "Просмотреть";
        this.button4.UseVisualStyleBackColor = true;
        this.button4.Click += System.EventHandler(this.button4_Click);
        // 
        // label7
        // 
        this.label7.AutoSize = true;
        this.label7.Location = System.Drawing.Point(12,  15);
        this.label7.Name = "label7";
        this.label7.Size = System.Drawing.Size(34,  13);
        this.label7.TabIndex = 23;
        this.label7.Text = "Mode";
        // 
        // MainForm
        // 
        this.AutoScaleDimensions = System.Drawing.SizeF(6f,  13f);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = System.Drawing.Size(551,  403);
        this.Controls.Add(this.label7);
        this.Controls.Add(this.button4);
        this.Controls.Add(this.listBox1);
        this.Controls.Add(this.textBox7);
        this.Controls.Add(this.label6);
        this.Controls.Add(this.textBox6);
        this.Controls.Add(this.label5);
        this.Controls.Add(this.textBox5);
        this.Controls.Add(this.label4);
        this.Controls.Add(this.textBox4);
        this.Controls.Add(this.button3);
        this.Controls.Add(this.button2);
        this.Controls.Add(this.textBox3);
        this.Controls.Add(this.label3);
        this.Controls.Add(this.richTextBox1);
        this.Controls.Add(this.checkBox1);
        this.Controls.Add(this.textBox2);
        this.Controls.Add(this.label2);
        this.Controls.Add(this.textBox1);
        this.Controls.Add(this.label1);
        this.Controls.Add(this.comboBox1);
        this.Controls.Add(this.button1);
        this.Name = "MainForm";
        this.Text = "MainForm";
        this.ResumeLayout(false);
        this.PerformLayout();
    }
  }
}
