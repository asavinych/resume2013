﻿using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExampleWorkWithEmail
{
  /// <summary>
  /// Description of MainForm.
  /// </summary>
  public partial class MainForm : Form
  {
    
    public this()
    {
      InitializeComponent();
      this.comboBox1.SelectedIndex=0;
    }
  
    //в зависимости от состояния формы либо отправляет сообщение, либо принимает
    private mutable button1 : System.Windows.Forms.Button;
    
    //переключатель состояний формы
    private mutable comboBox1 : System.Windows.Forms.ComboBox;
  
    private mutable label1 : System.Windows.Forms.Label;
        
    private mutable textBox1 : System.Windows.Forms.TextBox;
  
    private mutable label2 : System.Windows.Forms.Label;
  
    private mutable textBox2 : System.Windows.Forms.TextBox;
  
    private mutable checkBox1 : System.Windows.Forms.CheckBox;
  
    private mutable richTextBox1 : System.Windows.Forms.RichTextBox;
  
    private mutable label3 : System.Windows.Forms.Label;
  
    private mutable textBox3 : System.Windows.Forms.TextBox;
  
    private mutable button2 : System.Windows.Forms.Button;
  
    private mutable button3 : System.Windows.Forms.Button;
    
    private mutable textBox4 : System.Windows.Forms.TextBox;
    
    private mutable label4 : System.Windows.Forms.Label;
  
    private mutable textBox5 : System.Windows.Forms.TextBox;
  
    private mutable label5 : System.Windows.Forms.Label;
  
    private mutable textBox6 : System.Windows.Forms.TextBox;
  
    private mutable label6 : System.Windows.Forms.Label;
  
    private mutable textBox7 : System.Windows.Forms.TextBox;
  
    private mutable listBox1 : System.Windows.Forms.ListBox;
    
    private mutable button4 : System.Windows.Forms.Button;
    
    private mutable label7 : System.Windows.Forms.Label;
    
    private button2_Click (_ : object,  _ : System.EventArgs) : void
    {
        //выбор файла
        def df=OpenFileDialog();
        
        when (df.ShowDialog()==DialogResult.OK)
            textBox4.Text=df.FileName;
    }
  
    private button3_Click (_ : object,  _ : System.EventArgs) : void
    {
        textBox4.Text="";
    }
  
    private button1_Click (_ : object,  _ : System.EventArgs) : void
    {
        match(comboBox1.SelectedIndex)
        {
            | 0 => SendEmail();
            | 1 => Recievemails();
        }
        
    }
  
    private comboBox1_SelectedIndexChanged (_ : object,  _ : System.EventArgs) : void
    {
        listBox1.Items.Clear();
        match(comboBox1.SelectedIndex) 
        {
            | 0 => listBox1.Visible=false;
                   button1.Text="Отправить";
                   button4.Visible=false;
            | 1 => listBox1.Visible=true;
                   button1.Text="Обновить";
                   button4.Visible=true;
                   
        }
    }
  
    private button4_Click (_ : object,  _ : System.EventArgs) : void
    {
           GetMail(arrIndex[listBox1.SelectedIndex]);
    }
  
    
  }
}
