﻿#pragma warning disable 10001

namespace ExampleWorkWithEmail
{
  public partial class Message
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private mutable components : System.ComponentModel.IContainer = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override Dispose(disposing : bool) : void
    {
      when (disposing && components != null)
        components.Dispose();

      base.Dispose(disposing);
    }

    #region

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private InitializeComponent() : void
    {
        this.richTextBox1 = System.Windows.Forms.RichTextBox();
        this.SuspendLayout();
        // 
        // richTextBox1
        // 
        this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
        this.richTextBox1.Location = System.Drawing.Point(0,  0);
        this.richTextBox1.Name = "richTextBox1";
        this.richTextBox1.Size = System.Drawing.Size(500,  467);
        this.richTextBox1.TabIndex = 0;
        this.richTextBox1.Text = "";
        // 
        // Message
        // 
        this.AutoScaleDimensions = System.Drawing.SizeF(6f,  13f);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = System.Drawing.Size(500,  467);
        this.Controls.Add(this.richTextBox1);
        this.Name = "Message";
        this.Text = "Message";
        this.ResumeLayout(false);
    }

    #endregion
  }
}
