﻿using System;
using System.Text;
using System.Net.Sockets;
using System.Net.Security;
using System.IO;

namespace ExampleWorkWithEmail
{
    public class POP
    {
        private mutable mail:TcpClient;
        private mutable sslStream:SslStream;
        private mutable networkStream:NetworkStream;
        private mutable sr:StreamReader;
        private mutable sw:StreamWriter;

        public Connect(hostName:string, port:int, ssl:bool):string
        {
            try
            {
                mail=TcpClient(hostName, port);
                
                mutable a:Stream;
                
                match(ssl) 
                {
                    |true =>  sslStream=SslStream(mail.GetStream());
                              sslStream.AuthenticateAsClient(hostName);
                              a=sslStream;
                    | _ => networkStream=mail.GetStream();
                           a=networkStream;
                }
                
                sr=StreamReader(a, Encoding.ASCII);
                sw=StreamWriter(a, Encoding.ASCII);
                sr.ReadLine();    
            }
            catch
            {
                | s => s.Message;
            }
        }
        
        public SendCommand(com:string, f:StreamReader -> string):string
        {
            try
            {
                mutable buffer:array[byte] = Encoding.ASCII.GetBytes((com + "\r\n").ToCharArray());
                sw.BaseStream.Write(buffer, 0, buffer.Length);
                sw.Flush();
               
                f(sr);
            }
            catch
            {
             | d => d.Message;   
            }
        }
    }
}
