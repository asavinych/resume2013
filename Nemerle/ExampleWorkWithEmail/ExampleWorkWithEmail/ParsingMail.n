﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.IO;


namespace ExampleWorkWithEmail
{
  /// <summary>
  /// Description of ParsingMail.
  /// </summary>
  public class ParsingMail
  {
          
      //дата создания сообщения
      public mutable origDate:string;
      
      public mutable from:string;
      
      public mutable sender:string;
               
      //адреса тех, кто получает данное сообщение
      public mutable to:string;
      
      //адреса тех, кто получил данное сообщение
      public mutable cc:string;
      
      //адреса тех, кто должен получить сообщение, но их адреса необходимо скрыть
      public mutable bcc:string;
      
      public mutable subject:string;
      
      public mutable body:string;
      
      public mutable attachment:Stream;
      
      
      private convertSubject(str:string):string
      {
          def a=Attachment.CreateAttachmentFromString("", str);
          
          a.Name
      }
      
      
      
      //public parsingMail(str:StreamReader):list<string>
      //{
          
      //    mutable li:list<string>
      //    mutable line:string;
          
      //    def loop():void
      //    {
      //        when (!str.EndOfStream)
      //        {
      //              match(str.ReadLine())
      //              {
      //                  | x when x.IndexOf(" ")==0 => 
      //              }
      //        }
      //    }
          
          
      //}
      
      
      
      public GetSubjectFromMail(str:string):string
      {
          mutable a:list[string]=str.Split(array['\r', '\n'], StringSplitOptions.RemoveEmptyEntries).ToListRev();
          match(a.Find( x => x.IndexOf("Subject")==0))
          {
              | d when d.IsSome =>  def strNew=d.Value.Remove(0, 8).Trim();
                                    convertSubject(strNew);
              | _ => ""
          }
          
      }
  }
}
