#Consider the sequence of numbers a(i), i = 0, 1, 2, …, which satisfies the following requirements:
#a(0)=0
#a(1)=1
#a(2*i)
#a(2*i+1)=a(i)+a(i+1)
#for every i = 1, 2, 3, … .
#Write a program which for a given value of n finds the largest number among the numbers a(0), a(1), …, a(n).
#You are given several test cases (not more than 10). Each test case is a line containing an integer n (1 ≤ n ≤ 99 999). The last line of input contains 0.
#For every n in the input write the corresponding maximum value found.
#
#

$newHash={
     0 => 0,
     1 => 1,
     2 => 1
    }

def simpleCalc(n)
    return $newHash[n]  if  not $newHash[n].nil?
    if n%2==0  
        $newHash[n/2]=simpleCalc(n/2)
        $newHash[n]=$newHash[n/2]
	else 
        i=(n-1)/2
        $newHash[i]=simpleCalc(i)
        $newHash[i+1]=simpleCalc(i+1)
        $newHash[n]=$newHash[i]+$newHash[i+1]
	end
    $newHash[n]
end

def selectMax(n)
   max=0
   1.upto(n) { |x|
      res=simpleCalc(x)
	  if res>max
		max=res
	  end
	 }
   max
end

tokens = []
while string = gets do
   if string.to_i!=0
     puts selectMax(string.to_i)
   end
end
