import forcomp._

object test {
	class Poly (terms0:Map[Int, Double]){
		val terms=terms0 withDefaultValue 0.0
		def + (other:Poly) = new Poly (terms ++ (other.terms map adjust))
		
		def adjust(term:(Int,Double)):(Int, Double)={
			val (exp, coeff)=term
			exp -> (coeff + terms(exp))
		}
		
		override def toString =
			(for ((exp, coeff) <- terms.toList.sorted.reverse) yield coeff + "x^"+exp) mkString " + "
						
	}
	
	val p1=new Poly(Map (1 -> 2.0, 3 -> 4.0, 5 -> 6.2))
                                                  //> p1  : test.Poly = 6.2x^5 + 4.0x^3 + 2.0x^1
	p1.terms(9)                               //> res0: Double = 0.0
}