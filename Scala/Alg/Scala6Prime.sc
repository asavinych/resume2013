
object test {
	
 	
 	/*
 		Given a positive integer n, find all pairs of positive
 		integers i and j, with 1<=j<i<n such that i+j is prime.
 		For example, if n=7, the sought pairs are:
 		 i | 2 3 4 4 5 6 6
 		 j | 1 2 1 3 2 1 5
 		-------------------
 		i+j| 3 5 5 7 7 7 11
 	 	*/
 	
 	def isPrime(n:Int)=(2 until n) forall (n % _ !=0)
                                                  //> isPrime: (n: Int)Boolean
 	
 	def primePairs(n: Integer)=
 		((1 until n) flatMap ( i =>
 					(1 until i) map ( j => (i,j)))) filter (pair =>
 						isPrime(pair._1+pair._2))
                                                  //> primePairs: (n: Integer)scala.collection.immutable.IndexedSeq[(Int, Int)]

	primePairs(7)                             //> res0: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,1), (3,2
                                                  //| ), (4,1), (4,3), (5,2), (6,1), (6,5))
  
  def primePairsFor(n:Integer)=
  	for{
  		i <- 1 until n
  		j <- 1 until i
  		if isPrime(i+j)
  	} yield (i,j)                             //> primePairsFor: (n: Integer)scala.collection.immutable.IndexedSeq[(Int, Int)]
                                                  //| 
  	
  primePairsFor(7)                                //> res1: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,1), (3,2
                                                  //| ), (4,1), (4,3), (5,2), (6,1), (6,5))
  
  
}