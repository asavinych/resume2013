    --//count nulls and not nulls
	SELECT  sum(case when d.DispatchedAppointmentId is null then 1 else 0 end) count_nulls,
			count(d.DispatchedAppointmentId) count_not_nulls 
	FROM Appointments as a
	LEFT OUTER JOIN  DispatchedAppointments as d on d.AppointmentId=a.AppointmentId
	WHERE (a.Date=@Date or d.Date=@Date) and  a.StatusId=15