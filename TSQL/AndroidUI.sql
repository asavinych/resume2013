USE [AndreyTest]
GO
/****** Object:  Table [dbo].[AndroidButtons]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AndroidButtons](
	[ButtonId] [int] IDENTITY(1,1) NOT NULL,
	[TypeButton] [varchar](50) NOT NULL,
	[NameButton] [varchar](50) NULL,
	[Message] [nvarchar](max) NULL,
	[IsSync] [bit] NOT NULL,
	[IsTracking] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidButtons] PRIMARY KEY CLUSTERED 
(
	[ButtonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AndroidElementTypes]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AndroidElementTypes](
	[TypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidElementTypes] PRIMARY KEY CLUSTERED 
(
	[TypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AndroidFieldFormat]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AndroidFieldFormat](
	[FieldFormatId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidFieldFormat] PRIMARY KEY CLUSTERED 
(
	[FieldFormatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AndroidFields]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AndroidFields](
	[FieldId] [int] IDENTITY(1,1) NOT NULL,
	[FieldFormatId] [int] NOT NULL,
	[FieldTypeId] [int] NOT NULL,
	[DataSourceId] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidFields] PRIMARY KEY CLUSTERED 
(
	[FieldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AndroidFieldsDataSource]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AndroidFieldsDataSource](
	[DataSourceId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[ServerTableName] [varchar](100) NOT NULL,
	[ServerFieldName] [varchar](100) NOT NULL,
	[DeviceTableName] [varchar](100) NOT NULL,
	[DeviceFieldName] [varchar](100) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidFieldsDataSource] PRIMARY KEY CLUSTERED 
(
	[DataSourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AndroidFieldTypes]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AndroidFieldTypes](
	[FieldTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidFieldTypes] PRIMARY KEY CLUSTERED 
(
	[FieldTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AndroidGroup]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AndroidGroup](
	[GroupId] [int] IDENTITY(1,1) NOT NULL,
	[GroupTypeId] [int] NOT NULL,
	[DataSourceId] [int] NOT NULL,
	[SetOptionsId] [int] NULL,
	[Name] [varchar](100) NULL,
	[IsRequired] [bit] NOT NULL,
	[DefaultValue] [varchar](100) NULL,
	[SortOrder] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidGroup] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AndroidGroup_Button]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AndroidGroup_Button](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupId] [int] NOT NULL,
	[ButtonId] [int] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[Message] [nvarchar](max) NULL,
	[IsSync] [bit] NOT NULL,
	[IsTracking] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidGroup_Button] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AndroidGroupTypes]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AndroidGroupTypes](
	[GroupTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidGroupTypes] PRIMARY KEY CLUSTERED 
(
	[GroupTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AndroidNavigation]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AndroidNavigation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ElementId] [int] NOT NULL,
	[TypeElement] [int] NOT NULL,
	[ParentElementId] [int] NOT NULL,
	[TypeParentElementId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidNavigation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AndroidOptions]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AndroidOptions](
	[OptionId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidOptions] PRIMARY KEY CLUSTERED 
(
	[OptionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AndroidScreen_Group]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AndroidScreen_Group](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidScreen_Group] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AndroidScreens]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AndroidScreens](
	[ScreenId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](100) NULL,
	[IsDeleted] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidScreens] PRIMARY KEY CLUSTERED 
(
	[ScreenId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AndroidSetOption_Option]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AndroidSetOption_Option](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SetOptionsId] [int] NOT NULL,
	[OptionId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidSetOption_Option] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AndroidSetOptions]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AndroidSetOptions](
	[SetOptionsId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidSetOptions] PRIMARY KEY CLUSTERED 
(
	[SetOptionsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AndroidWorkflow_Screen]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AndroidWorkflow_Screen](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkflowId] [int] NOT NULL,
	[ScreenId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidWorkflow_Screen] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AndroidWorkflows]    Script Date: 4/1/2014 3:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AndroidWorkflows](
	[WorkflowId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[IsDeleted] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AndroidWorkflows] PRIMARY KEY CLUSTERED 
(
	[WorkflowId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AndroidButtons] ADD  CONSTRAINT [DF_AndroidButtons_IsSync]  DEFAULT ((0)) FOR [IsSync]
GO
ALTER TABLE [dbo].[AndroidButtons] ADD  CONSTRAINT [DF_AndroidButtons_IsTracking]  DEFAULT ((0)) FOR [IsTracking]
GO
ALTER TABLE [dbo].[AndroidButtons] ADD  CONSTRAINT [DF_AndroidButtons_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidButtons] ADD  CONSTRAINT [DF_AndroidButtons_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidButtons] ADD  CONSTRAINT [DF_AndroidButtons_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidElementTypes] ADD  CONSTRAINT [DF_AndroidElementTypes_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidElementTypes] ADD  CONSTRAINT [DF_AndroidElementTypes_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidElementTypes] ADD  CONSTRAINT [DF_AndroidElementTypes_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidFieldFormat] ADD  CONSTRAINT [DF_AndroidFieldFormat_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidFieldFormat] ADD  CONSTRAINT [DF_AndroidFieldFormat_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidFieldFormat] ADD  CONSTRAINT [DF_AndroidFieldFormat_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidFields] ADD  CONSTRAINT [DF_AndroidFields_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidFields] ADD  CONSTRAINT [DF_AndroidFields_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidFields] ADD  CONSTRAINT [DF_AndroidFields_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidFieldsDataSource] ADD  CONSTRAINT [DF_AndroidFieldsDataSource_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidFieldsDataSource] ADD  CONSTRAINT [DF_AndroidFieldsDataSource_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidFieldsDataSource] ADD  CONSTRAINT [DF_AndroidFieldsDataSource_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidFieldTypes] ADD  CONSTRAINT [DF_AndroidFieldTypes_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidFieldTypes] ADD  CONSTRAINT [DF_AndroidFieldTypes_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidFieldTypes] ADD  CONSTRAINT [DF_AndroidFieldTypes_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidGroup] ADD  CONSTRAINT [DF_AndroidGroup_IsRequired]  DEFAULT ((0)) FOR [IsRequired]
GO
ALTER TABLE [dbo].[AndroidGroup] ADD  CONSTRAINT [DF_AndroidGroup_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
GO
ALTER TABLE [dbo].[AndroidGroup] ADD  CONSTRAINT [DF_AndroidGroup_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidGroup] ADD  CONSTRAINT [DF_AndroidGroup_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidGroup] ADD  CONSTRAINT [DF_AndroidGroup_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidGroup_Button] ADD  CONSTRAINT [DF_AndroidGroup_Button_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
GO
ALTER TABLE [dbo].[AndroidGroup_Button] ADD  CONSTRAINT [DF_AndroidGroup_Button_IsSync]  DEFAULT ((0)) FOR [IsSync]
GO
ALTER TABLE [dbo].[AndroidGroup_Button] ADD  CONSTRAINT [DF_AndroidGroup_Button_IsTracking]  DEFAULT ((0)) FOR [IsTracking]
GO
ALTER TABLE [dbo].[AndroidGroup_Button] ADD  CONSTRAINT [DF_AndroidGroup_Button_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidGroup_Button] ADD  CONSTRAINT [DF_AndroidGroup_Button_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidGroup_Button] ADD  CONSTRAINT [DF_AndroidGroup_Button_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidGroupTypes] ADD  CONSTRAINT [DF_AndroidGroupTypes_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidGroupTypes] ADD  CONSTRAINT [DF_AndroidGroupTypes_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidGroupTypes] ADD  CONSTRAINT [DF_AndroidGroupTypes_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidNavigation] ADD  CONSTRAINT [DF_AndroidNavigation_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidNavigation] ADD  CONSTRAINT [DF_AndroidNavigation_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidNavigation] ADD  CONSTRAINT [DF_AndroidNavigation_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidOptions] ADD  CONSTRAINT [DF_AndroidOptions_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidOptions] ADD  CONSTRAINT [DF_AndroidOptions_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidOptions] ADD  CONSTRAINT [DF_AndroidOptions_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidScreen_Group] ADD  CONSTRAINT [DF_AndroidScreen_Group_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidScreen_Group] ADD  CONSTRAINT [DF_AndroidScreen_Group_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidScreen_Group] ADD  CONSTRAINT [DF_AndroidScreen_Group_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidScreens] ADD  CONSTRAINT [DF_AndroidScreens_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidScreens] ADD  CONSTRAINT [DF_AndroidScreens_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
GO
ALTER TABLE [dbo].[AndroidScreens] ADD  CONSTRAINT [DF_AndroidScreens_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidScreens] ADD  CONSTRAINT [DF_AndroidScreens_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidSetOption_Option] ADD  CONSTRAINT [DF_AndroidSetOption_Option_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidSetOption_Option] ADD  CONSTRAINT [DF_AndroidSetOption_Option_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidSetOption_Option] ADD  CONSTRAINT [DF_AndroidSetOption_Option_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidSetOptions] ADD  CONSTRAINT [DF_AndroidSetOptions_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidSetOptions] ADD  CONSTRAINT [DF_AndroidSetOptions_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidSetOptions] ADD  CONSTRAINT [DF_AndroidSetOptions_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidWorkflow_Screen] ADD  CONSTRAINT [DF_AndroidWorkflow_Screen_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidWorkflow_Screen] ADD  CONSTRAINT [DF_AndroidWorkflow_Screen_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidWorkflow_Screen] ADD  CONSTRAINT [DF_AndroidWorkflow_Screen_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidWorkflows] ADD  CONSTRAINT [DF_AndroidWorkflows_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AndroidWorkflows] ADD  CONSTRAINT [DF_AndroidWorkflows_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[AndroidWorkflows] ADD  CONSTRAINT [DF_AndroidWorkflows_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[AndroidFields]  WITH CHECK ADD  CONSTRAINT [FK_AndroidFields_AndroidFieldFormat] FOREIGN KEY([FieldFormatId])
REFERENCES [dbo].[AndroidFieldFormat] ([FieldFormatId])
GO
ALTER TABLE [dbo].[AndroidFields] CHECK CONSTRAINT [FK_AndroidFields_AndroidFieldFormat]
GO
ALTER TABLE [dbo].[AndroidFields]  WITH CHECK ADD  CONSTRAINT [FK_AndroidFields_AndroidFieldsDataSource] FOREIGN KEY([DataSourceId])
REFERENCES [dbo].[AndroidFieldsDataSource] ([DataSourceId])
GO
ALTER TABLE [dbo].[AndroidFields] CHECK CONSTRAINT [FK_AndroidFields_AndroidFieldsDataSource]
GO
ALTER TABLE [dbo].[AndroidFields]  WITH CHECK ADD  CONSTRAINT [FK_AndroidFields_AndroidFieldTypes] FOREIGN KEY([FieldTypeId])
REFERENCES [dbo].[AndroidFieldTypes] ([FieldTypeId])
GO
ALTER TABLE [dbo].[AndroidFields] CHECK CONSTRAINT [FK_AndroidFields_AndroidFieldTypes]
GO
ALTER TABLE [dbo].[AndroidGroup]  WITH CHECK ADD  CONSTRAINT [FK_AndroidGroup_AndroidGroupTypes] FOREIGN KEY([GroupTypeId])
REFERENCES [dbo].[AndroidGroupTypes] ([GroupTypeId])
GO
ALTER TABLE [dbo].[AndroidGroup] CHECK CONSTRAINT [FK_AndroidGroup_AndroidGroupTypes]
GO
ALTER TABLE [dbo].[AndroidGroup]  WITH CHECK ADD  CONSTRAINT [FK_AndroidGroup_AndroidSetOptions] FOREIGN KEY([SetOptionsId])
REFERENCES [dbo].[AndroidSetOptions] ([SetOptionsId])
GO
ALTER TABLE [dbo].[AndroidGroup] CHECK CONSTRAINT [FK_AndroidGroup_AndroidSetOptions]
GO
ALTER TABLE [dbo].[AndroidGroup_Button]  WITH CHECK ADD  CONSTRAINT [FK_AndroidGroup_Button_AndroidButtons] FOREIGN KEY([ButtonId])
REFERENCES [dbo].[AndroidButtons] ([ButtonId])
GO
ALTER TABLE [dbo].[AndroidGroup_Button] CHECK CONSTRAINT [FK_AndroidGroup_Button_AndroidButtons]
GO
ALTER TABLE [dbo].[AndroidGroup_Button]  WITH CHECK ADD  CONSTRAINT [FK_AndroidGroup_Button_AndroidGroup] FOREIGN KEY([GroupId])
REFERENCES [dbo].[AndroidGroup] ([GroupId])
GO
ALTER TABLE [dbo].[AndroidGroup_Button] CHECK CONSTRAINT [FK_AndroidGroup_Button_AndroidGroup]
GO
ALTER TABLE [dbo].[AndroidScreen_Group]  WITH CHECK ADD  CONSTRAINT [FK_AndroidScreen_Group_AndroidGroup] FOREIGN KEY([GroupId])
REFERENCES [dbo].[AndroidGroup] ([GroupId])
GO
ALTER TABLE [dbo].[AndroidScreen_Group] CHECK CONSTRAINT [FK_AndroidScreen_Group_AndroidGroup]
GO
ALTER TABLE [dbo].[AndroidScreen_Group]  WITH CHECK ADD  CONSTRAINT [FK_AndroidScreen_Group_AndroidScreens] FOREIGN KEY([ScreenId])
REFERENCES [dbo].[AndroidScreens] ([ScreenId])
GO
ALTER TABLE [dbo].[AndroidScreen_Group] CHECK CONSTRAINT [FK_AndroidScreen_Group_AndroidScreens]
GO
ALTER TABLE [dbo].[AndroidSetOption_Option]  WITH CHECK ADD  CONSTRAINT [FK_AndroidSetOption_Option_AndroidOptions] FOREIGN KEY([OptionId])
REFERENCES [dbo].[AndroidOptions] ([OptionId])
GO
ALTER TABLE [dbo].[AndroidSetOption_Option] CHECK CONSTRAINT [FK_AndroidSetOption_Option_AndroidOptions]
GO
ALTER TABLE [dbo].[AndroidSetOption_Option]  WITH CHECK ADD  CONSTRAINT [FK_AndroidSetOption_Option_AndroidSetOptions] FOREIGN KEY([SetOptionsId])
REFERENCES [dbo].[AndroidSetOptions] ([SetOptionsId])
GO
ALTER TABLE [dbo].[AndroidSetOption_Option] CHECK CONSTRAINT [FK_AndroidSetOption_Option_AndroidSetOptions]
GO
ALTER TABLE [dbo].[AndroidWorkflow_Screen]  WITH CHECK ADD  CONSTRAINT [FK_AndroidWorkflow_Screen_AndroidScreens] FOREIGN KEY([ScreenId])
REFERENCES [dbo].[AndroidScreens] ([ScreenId])
GO
ALTER TABLE [dbo].[AndroidWorkflow_Screen] CHECK CONSTRAINT [FK_AndroidWorkflow_Screen_AndroidScreens]
GO
ALTER TABLE [dbo].[AndroidWorkflow_Screen]  WITH CHECK ADD  CONSTRAINT [FK_AndroidWorkflow_Screen_AndroidWorkflows] FOREIGN KEY([WorkflowId])
REFERENCES [dbo].[AndroidWorkflows] ([WorkflowId])
GO
ALTER TABLE [dbo].[AndroidWorkflow_Screen] CHECK CONSTRAINT [FK_AndroidWorkflow_Screen_AndroidWorkflows]
GO
