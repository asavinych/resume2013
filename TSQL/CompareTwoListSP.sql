--find find not exists sp in db from one DB (DAS_InvSoCal)
select * 
  from [DAS_InvSoCal].information_schema.routines as one
 where one.routine_type like 'PROCEDURE'
 and  (one.ROUTINE_NAME not in (select two.ROUTINE_NAME 
  from [DAS_10_05].information_schema.routines as two
 where two.routine_type like 'PROCEDURE'))