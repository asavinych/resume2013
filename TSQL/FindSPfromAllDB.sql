DROP TABLE #Databases

CREATE TABLE #Databases (ID INTEGER IDENTITY (0,1), DbName NVARCHAR(128))

INSERT INTO #Databases(DbName)
SELECT name FROM sys.databases

DECLARE @CurrentID INTEGER = 0,
        @MaxID     INTEGER = (SELECT MAX(ID) FROM #Databases)

DECLARE @DbName NVARCHAR(128) = (SELECT DbName FROM #Databases WHERE ID = @CurrentID),
        @SqlCommand   VARCHAR(MAX)



WHILE (@CurrentID <= @MaxID)
BEGIN
    SET @SqlCommand = 'SELECT name,ROUTINES.SPECIFIC_SCHEMA,ROUTINES.SPECIFIC_NAME 
                       FROM sys.databases AS DatabaseNames
                       INNER JOIN ' + @DbName + '.INFORMATION_SCHEMA.ROUTINES AS ROUTINES
                               ON ROUTINES.SPECIFIC_CATALOG = DatabaseNames.name
                       WHERE ROUTINES.ROUTINE_TYPE = ''PROCEDURE'' 
                        AND ROUTINES.SPECIFIC_NAME IN (''App_Office_OfficeSelectByUserId'',''App_Office_OfficeSelectByOrganizationId'')'

   EXEC (@SqlCommand)

   SET @CurrentID = @CurrentID + 1
   SELECT @DbName = DbName 
   FROM #Databases 
   WHERE ID = @CurrentID

END