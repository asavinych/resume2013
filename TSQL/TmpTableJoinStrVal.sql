create table #EXITABLE ([AccountNum] [int]
                        , [ClientName] [nvarchar](50)
                        , [SellerName] [nvarchar](50)
                        , [ProfSysName] [nvarchar](50))

insert into #EXITABLE	 (AccountNum, ClientName, SellerName, ProfSysName)
SELECT 
	Task.AccountNum,
	Client.Name AS ClientName,
	Seller.Name AS SellerName,
	ProfSys.Name AS ProfSysName
FROM Task
  inner join Project on Task.ID    = Project.idTask
  left  join ProfSys on ProfSys.ID = Project.idProfSys
  left  join Client  on Client.ID  = Task.idClient
  left  join Client Seller  on Seller.ID = Task.idSeller
--join ST1.ProfSysName values, sample: Names | one, two, three
Select distinct ST2.AccountNum, ST2.ClientName, ST2.SellerName,
    substring(
        (
            Select ','+ST1.ProfSysName  AS [text()]
            From #EXITABLE ST1
            Where ST1.AccountNum = ST2.AccountNum
            ORDER BY ST1.AccountNum
            For XML PATH ('')
        ), 2, 1000) Names
From #EXITABLE ST2
group by ST2.AccountNum, ST2.ClientName, ST2.SellerName