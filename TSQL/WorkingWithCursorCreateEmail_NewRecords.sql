CREATE PROCEDURE [dbo].[CreateEmail_NewRecords] 
AS
BEGIN

	create table #StorageProceudreTmp(
[ForUpdateQuery] nvarchar(MAX),
[Query] nvarchar(MAX) null,
[TableName] nvarchar(200)  null,
[FieldIdName] nvarchar(200)  null,
[FirstFieldId] bigint  null,
[NotificationId] int  null,
[TemplateId] int not null
)

insert into #StorageProceudreTmp ([ForUpdateQuery], [Query], [TableName], [FieldIdName], [FirstFieldId], [NotificationId], [TemplateId])
select 
convert(nvarchar(max), Concat(' select ', '@LastId=MAX(', InvNot.FieldIdName, ') ', ' from ', InvNot.TableName,' where ',InvNot.[FieldIdName], '>', isnull(InvNotHis.FieldId, '0'))),
convert(nvarchar(max), Concat(' select @OutputXML=( select ', ' * from ', InvNot.TableName,' where ',InvNot.[FieldIdName], '>', isnull(InvNotHis.FieldId, '0'))),
convert(nvarchar(200), InvNot.TableName),
convert(nvarchar(200), InvNot.FieldIdName),
isnull(InvNotHis.FieldId, 0),
InvNot.[NotificationId],
InvNot.[TemplateId]
from InvNotification as InvNot
left join InvNotificationHistory as InvNotHis on (InvNot.NotificationId=InvNotHis.NotificationId)
where not (InvNot.TableName is null)
	and InvNot.AddNewRecord = 1

declare @ForUpdateQuery nvarchar(MAX)
declare @NotificationId int
declare @Query  nvarchar(MAX)
declare @TableName varchar(50)
declare @FieldIdName varchar(200)
declare @FirstFieldId int
declare @TemplateId int


declare coursor_storage_procedure cursor for
select [ForUpdateQuery], [Query], [TableName], [FieldIdName], [FirstFieldId], [NotificationId], [TemplateId]  from #StorageProceudreTmp

open coursor_storage_procedure

fetch next from coursor_storage_procedure
into @ForUpdateQuery, @Query, @TableName, @FieldIdName, @FirstFieldId, @NotificationId, @TemplateId 

while @@FETCH_STATUS=0
begin
	declare @LastIdOut bigint

	exec sp_executesql @ForUpdateQuery, N'@LastId bigint OUTPUT', @LastId= @LastIdOut OUTPUT
	
	if (@LastIdOut<>@FirstFieldId) 
		begin
			set @Query=Concat(@Query, N' and ', @FieldIdName, N'<=', isnull(@LastIdOut, @FirstFieldId), N' for xml path(''''))')

			declare @OutputXML1 nvarchar(MAX)
			declare @OutputXML nvarchar(MAX)

			exec sp_executesql @Query, N'@OutputXML varchar(MAX) OUTPUT', @OutputXML= @OutputXML1 OUTPUT  

			insert into [Emails].[dbo].[Queue] ([TemplateId], [ToEmail], [ToName], [FromEmail], [FromName], [Subject], [TextBody], [HtmlBody])
			select	[TemplateIdEmail] as TemplateId,
					[ToEmail],
					[ToName],
					[FromEmail],
					[FromName],
					[Subject],
					CONCAT([TextBody], ' ', @OutputXML1) as TextBody,
					[HtmlBody]
			from [dbo].[Templates] where [TemplateId]=@TemplateId
			
			if exists (select * from InvNotificationHistory where NotificationId=@NotificationId)
				begin
					update InvNotificationHistory set FieldId = isnull(@LastIdOut, FieldId) where NotificationId=@NotificationId
				end
			else
				begin
					insert into InvNotificationHistory ([NotificationId], [TableName], [FieldId])
					values (@NotificationId, @TableName,@LastIdOut) 
				end
		end
	fetch next from coursor_storage_procedure into @ForUpdateQuery, @Query, @TableName, @FieldIdName, @FirstFieldId, @NotificationId, @TemplateId 
end

close coursor_storage_procedure
deallocate coursor_storage_procedure

drop table #StorageProceudreTmp


END
