CREATE PROCEDURE [dbo].[WebService_AndroidSync_GetChangedWorkflowButtons] (
	@LastSyncDate DATETIME = NULL,
	@UserId int=NULL
) AS


SELECT HHScreenItem.[ScreenItem] as 'WorkflowButtonId'
      ,HHScreenItem.[ScreenId] as 'WorkflowScreenId'
      ,HHElement.[ElementTypeId]-13 as 'WorkflowButtonTypeId'
      ,HHElement.[Name] as 'Text'
      ,HHScreenItem.[SortOrder]
      ,null as 'NextWorkflowScreenId'
      ,HHScreenItem.[IsDeleted]
      ,HHScreenItem.[LastModifiedDate]
      ,HHScreenItem.[CreateDate]
      ,HHElement.[FieldDataSourceId] as 'WorkFlowFieldTemplateID'
      ,HHScreenItem.[ButtonMessage] as 'Message'
      ,HHScreenItem.[ButtonIsSync] as 'IsSync'
      ,HHScreenItem.[ButtonIsTracking] as 'IsTracking'
FROM HHScreenItem 
inner join HHElement on HHElement.ElementId=HHScreenItem.ElementId
WHERE
	 ([HHElement].[ElementType] like 'Button') AND
	ISNULL(HHScreenItem.IsDeleted, 0) = 0
	AND ISNULL(HHScreenItem.LastModifiedDate, HHScreenItem.CreateDate) >= ISNULL(@LastSyncDate, '1900/01/01')
	
SELECT GETDATE() AS LastSyncDate
GO
/****** Object:  StoredProcedure [dbo].[WebService_AndroidSync_GetChangedWorkflowFieldsNew]    Script Date: 01.07.2014 16:30:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
CREATE PROCEDURE [dbo].[WebService_AndroidSync_GetChangedWorkflowFieldsNew] (
	@LastSyncDate DATETIME = NULL,
	@UserId int=NULL
) AS

	create table #WorkflowFieldTemp([WorkflowFieldId] [int] identity(1,1) NOT NULL,
				[WorkflowScreenId] [int] NOT NULL,
				[WorkflowFieldTemplateId] [int] NOT NULL,
				[WorkflowFieldTypeId] [int] NOT NULL,
				[Name] [varchar](100) NULL,
				[IsRequired] [bit] NULL,
				[WorkflowFieldContentType] [int] NULL,
				[MinLength] [smallint] NULL,
				[MaxLength] [smallint] NULL,
				[WorkflowParentFieldId] [int] NULL,
				[WorkflowParentValueOptionId] [int] NULL,
				[DefaultValue] [varchar](100) NULL,
				[SortOrder] [int] NULL,
				[IsDeleted] [bit] NOT NULL,
				[LastModifiedDate] [datetime] NULL,
				[CreateDate] [datetime] NOT NULL,
				[WorkflowParentValueOptionIds] [varchar](100) NULL,
				[Id] [int] null)

		--2
		insert into #WorkflowFieldTemp	(
			   [WorkflowScreenId],[WorkflowFieldTemplateId],[WorkflowFieldTypeId],[Name],[IsRequired],[WorkflowFieldContentType],[MinLength]
			  ,[MaxLength],[WorkflowParentFieldId],[WorkflowParentValueOptionId],[DefaultValue],[SortOrder],[IsDeleted],[LastModifiedDate]
			  ,[CreateDate],[WorkflowParentValueOptionIds], [Id]
			  )

		select [HHScreenItem].[ScreenId], [HHScreenItem].[ElementId],[HHElement].[ElementTypeId], [HHElement].[Name], [HHScreenItem].[IsRequired],
				ISNULL([HHElement].[FieldFormatId]-1, 0), [HHScreenItem].[MinLength], [HHScreenItem].[MaxLength],
				[HHScreenItem].[ParentElementId], [HHScreenItem].[ParentElementItemId], [HHScreenItem].[DefaultValue], [HHScreenItem].[SortOrder],
				[HHScreenItem].[IsDeleted],[HHScreenItem].[LastModifiedDate]
			  ,[HHScreenItem].[CreateDate], null, null
		from [HHScreenItem]
		inner join [HHElement] on [HHScreenItem].[ElementId] = [HHElement].[ElementId]
		where ([HHElement].[ElementType] not like 'Button')
				and ISNULL([HHScreenItem].IsDeleted, 0) = 0
				and ISNULL([HHScreenItem].LastModifiedDate, [HHScreenItem].CreateDate) >= ISNULL(@LastSyncDate, '1900/01/01')
		union all
		select  
			-2 as 'ScreenId', [HHGroupItems].[ElementId],[HHElement].[ElementTypeId], [HHElement].[Name], [HHGroupItems].[IsRequired],
				ISNULL([HHElement].[FieldFormatId]-1, 0),[HHGroupItems].[MinLength], [HHGroupItems].[MaxLength],
				[HHGroupItems].[ParentElementId],[HHGroupItems].[ParentElementItemId], [HHGroupItems].[DefaultValue], [HHGroupItems].[SortOrder],
				[HHGroupItems].[IsDeleted],[HHGroupItems].[LastModifiedDate]
			  ,[HHGroupItems].[CreateDate], [HHGroupItems].[ParentElementItemId], [HHGroupItems].[GroupItem]
		from [HHGroupItems]
		inner join [HHElement] on [HHGroupItems].[ElementId] = [HHElement].[ElementId]
		where ([HHElement].[ElementType] not like 'Button')
				and ISNULL([HHGroupItems].IsDeleted, 0) = 0
				and ISNULL([HHGroupItems].LastModifiedDate, [HHGroupItems].CreateDate) >= ISNULL(@LastSyncDate, '1900/01/01')
		--3
		update #WorkflowFieldTemp 
		set [WorkflowScreenId] = (select top 1 [HHScreenItem].[ScreenId]
								  from [HHScreenItem], [HHGroupItems]
								  where [HHScreenItem].[ElementId]=#WorkflowFieldTemp.[WorkflowParentFieldId] or 
								  ([HHGroupItems].[ElementId]=#WorkflowFieldTemp.[WorkflowParentFieldId] and [HHScreenItem].[ElementId]=[HHGroupItems].[ParentElementId]))

		where #WorkflowFieldTemp.[WorkflowScreenId] =-2
		
		--4
		create table #OptionsTemp(
				[FirstId] [int] null,
				[FirstItemId] [int] null,
				[SecondId] [int] null,
				[SecondItemId] [int] null,
				[ElementId] [int] not null,
				[GroupItemId] [int] not null
		)

		insert into #OptionsTemp(
				[FirstId],
				[FirstItemId],
				[SecondId],
				[SecondItemId],
				[ElementId],
				[GroupItemId]
		)
		select [ParentElementId], [ParentElementItemId], null, null, [ElementId], [GroupItem]
		from [HHGroupItems]

		update #OptionsTemp
		set SecondId = [ParentElementId], SecondItemId = [ParentElementItemId]
		from [HHGroupItems]
		where #OptionsTemp.[FirstId]=[HHGroupItems].[ElementId]


		update #WorkflowFieldTemp 
		set #WorkflowFieldTemp.[WorkflowParentValueOptionId] = ISNULL(#OptionsTemp.[SecondItemId], #OptionsTemp.[FirstItemId]),
			#WorkflowFieldTemp.[WorkflowParentValueOptionIds] = case 
																	when not (#OptionsTemp.[SecondItemId] is null) then 
																		#OptionsTemp.[FirstItemId]
																	else
																		null
																end
		from #OptionsTemp 
		where #WorkflowFieldTemp.[Id]=#OptionsTemp.[GroupItemId]

	

		select * from #WorkflowFieldTemp 
		
		drop table #WorkflowFieldTemp
		drop table #OptionsTemp

		SELECT GETDATE() AS LastSyncDate

		

		

GO
/****** Object:  StoredProcedure [dbo].[WebService_AndroidSync_GetChangedWorkflowFieldTemplates]    Script Date: 01.07.2014 16:30:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================

CREATE PROCEDURE [dbo].[WebService_AndroidSync_GetChangedWorkflowFieldTemplates] (
	@LastSyncDate DATETIME = NULL,
	@UserId int=NULL
) AS

SELECT HHElement.ElementId as 'WorkflowFieldTemplateId'
      ,HHElement.[Name]
      ,HHDataSource.[ServerTableName]
      ,HHDataSource.[ServerFieldName]
      ,HHDataSource.[DeviceTableName]
      ,HHDataSource.[DeviceFieldName]
      ,HHElement.[IsDeleted]
      ,HHElement.[LastModifiedDate]
      ,HHElement.[CreateDate]
FROM HHElement
inner join HHDataSource on HHElement.FieldDataSourceId=HHDataSource.FieldDataSourceId
WHERE
	([HHElement].[ElementType] not like 'Button')  AND
	ISNULL(HHElement.IsDeleted, 0) = 0
	AND ISNULL(HHElement.LastModifiedDate, HHElement.CreateDate) >= ISNULL(@LastSyncDate, '1900/01/01')
	
SELECT GETDATE() AS LastSyncDate

GO
/****** Object:  StoredProcedure [dbo].[WebService_AndroidSync_GetChangedWorkflowFieldValueOptions]    Script Date: 01.07.2014 16:30:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[WebService_AndroidSync_GetChangedWorkflowFieldValueOptions] (
	@LastSyncDate DATETIME = NULL,
	@UserId int=NULL
) AS
	   create table #WorkflowFieldTemp([WorkflowFieldId] [int] identity(1,1) NOT NULL,
				[WorkflowFieldTemplateId] [int] NOT NULL,
				[WorkflowFieldTypeId] [int] NOT NULL,
				[Id] [int] null)

		--2
		insert into #WorkflowFieldTemp	(
			  [WorkflowFieldTemplateId],[WorkflowFieldTypeId],[Id]
			  )
		select [HHScreenItem].[ElementId],[HHElement].[ElementTypeId], null
		from [HHScreenItem]
		inner join [HHElement] on [HHScreenItem].[ElementId] = [HHElement].[ElementId]
		where ([HHElement].[ElementType] not like 'Button')
				and ISNULL([HHScreenItem].IsDeleted, 0) = 0
				and ISNULL([HHScreenItem].LastModifiedDate, [HHScreenItem].CreateDate) >= ISNULL(@LastSyncDate, '1900/01/01')
		union all
		select  
			   [HHGroupItems].[ElementId],[HHElement].[ElementTypeId], [HHGroupItems].[GroupItem]
		from [HHGroupItems]
		inner join [HHElement] on [HHGroupItems].[ElementId] = [HHElement].[ElementId]
		where ([HHElement].[ElementType] not like 'Button')
				and ISNULL([HHGroupItems].IsDeleted, 0) = 0
				and ISNULL([HHGroupItems].LastModifiedDate, [HHGroupItems].CreateDate) >= ISNULL(@LastSyncDate, '1900/01/01')
		--3

       create table #WorkflowFieldOptionsTmp(
				[WorkflowFieldValueOptionId] [int] NOT NULL,
				[WorkflowFieldId] [int] NOT NULL,
				[Text] [nvarchar](100) NOT NULL,
				[Value] [nvarchar](100) NOT NULL,
				[LastModifiedDate] [datetime] NULL,
				[IsDeleted] [bit] NULL,
				[CreateDate] [datetime] NOT NULL,)
		--2
		insert into #WorkflowFieldOptionsTmp	(
			   [WorkflowFieldValueOptionId],[WorkflowFieldId], [Text], [Value], [LastModifiedDate], [IsDeleted], [CreateDate]
			  )
		select  HHDropDown.Id, #WorkflowFieldTemp.[WorkflowFieldId], HHLookupValues.[Text], HHLookupValues.[Text], 
				HHDropDown.LastModifiedDate, HHDropDown.IsDeleted, HHDropDown.CreateDate
		from #WorkflowFieldTemp
		inner join HHDropDown on #WorkflowFieldTemp.[WorkflowFieldTemplateId]=HHDropDown.ElementId
		inner join HHLookupValues on HHLookupValues.LookupId=HHDropDown.LookupValueId
		where ISNULL([HHDropDown].IsDeleted, 0) = 0
				and ISNULL([HHDropDown].LastModifiedDate, [HHDropDown].CreateDate) >= ISNULL(@LastSyncDate, '1900/01/01')
		
		select * from #WorkflowFieldOptionsTmp

		drop table #WorkflowFieldOptionsTmp
		drop table #WorkflowFieldTemp
	
		SELECT GETDATE() AS LastSyncDate
GO
/****** Object:  StoredProcedure [dbo].[WebService_AndroidSync_GetChangedWorkflows]    Script Date: 01.07.2014 16:30:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
CREATE PROCEDURE [dbo].[WebService_AndroidSync_GetChangedWorkflows] (
	@LastSyncDate DATETIME = NULL,
	@UserId int=NULL
) AS

SELECT HHWorkflow.*
FROM HHWorkflow   with (nolock)
WHERE
	ISNULL(IsDeleted, 0) = 0
	AND ISNULL(LastModifiedDate, CreateDate) >= ISNULL(@LastSyncDate, '1900/01/01')
	
SELECT GETDATE() AS LastSyncDate


GO
/****** Object:  StoredProcedure [dbo].[WebService_AndroidSync_GetChangedWorkflowScreens]    Script Date: 01.07.2014 16:30:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================

CREATE PROCEDURE [dbo].[WebService_AndroidSync_GetChangedWorkflowScreens] (
	@LastSyncDate DATETIME = NULL,
	@UserId int=NULL
) AS

select [HHScreen].[ScreenId] as 'WorkflowScreenId',
	   [HHWorkflowToScreen].[WorkflowId],
	   [HHScreen].[Title],
	   [HHScreen].[IsDeleted],
	   [HHScreen].[SortOrder],
	   [HHScreen].[LastModifiedDate],
	   [HHScreen].[CreateDate]
from [HHScreen]
INNER JOIN  [HHWorkflowToScreen] on [HHWorkflowToScreen].[ScreenId]=[HHScreen].[ScreenId]

SELECT GETDATE() AS LastSyncDate
